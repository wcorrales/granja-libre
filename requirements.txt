email_validator
# flask-googlemaps
pg-db
pytz
quart
quart-auth
quart-csrf
wtforms

# PGVEGA
cairosvg
graphviz
numpy
pydot
simpleeval
xhtml2pdf

# NOTEBOOKS
ipykernel
jupyter_client
lxml
nbconvert
matplotlib
pandas
# pivottablejs

# MESSAGING
pytelegrambotapi
# requests  # whatssap_bot
