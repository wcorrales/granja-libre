Granja Libre
============

Animals reproduction and production application.

It works with Postgresql, Python-Quart, JQuery and Bootstrap.

Home web site: [Granja Libre](https://granjalibre.com)


Install
-------

First clone repository and install packages
```bash
git clone https://gitlab.com/wcorrales/granja-libre.git
cd granja-libre
virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
deactivate
```

Second configure database
```bash
createuser -s root
createdb granjalibre

psql granjalibre -f app/breeding/models_main_tables.sql
psql granjalibre -f app/breeding/models_main_triggers.sql
psql granjalibre -v activity='1' -f app/breeding/models_breeding_tables.sql
psql granjalibre -v activity='1' -f app/breeding/models_breeding_triggers.sql
psql granjalibre -v activity='1' -f app/breeding/models_breeding_querys.sql
psql granjalibre -v activity='1' -f app/breeding/models_growing_triggers.sql
psql granjalibre -v activity='1' -f app/breeding/models_growing_querys.sql

psql granjalibre -v activity='1' -f app/notebooks/models.sql
psql granjalibre -v activity='1' -f app/pgvega/models.sql
psql granjalibre -v activity='1' -f app/utils/utils.sql
psql granjalibre -v activity='1' -f app/workers/models.sql
psql granjalibre -v activity='1' -f app/workers_chats/models.sql

psql granjalibre
  INSERT INTO farms VALUES(1, 'COOP FARM', 'coop', 'user@mail.com', MD5('pass'), 0);
  INSERT INTO activities VALUES(1, 1, 'pig', 'meet', 0, 'granjax', NULL, NULL, 'COOP FARM');
  SET search_path to activity_1;
  INSERT INTO workers VALUES(DEFAULT, 'x', '123', 'user@mail.com', MD5('pass'),
                             'delete', 'settings_roles', TRUE);
```


RUN
---

```bash
source venv/bin/activate
python run.py
# gunicorn --bind 127.0.0.1:9005 run:app
```


PRODUCTION
----------

```bash
nano /etc/systemd/system/granja.socket
  [Unit]
  Description = granja socket

  [Socket]
  ListenStream = /run/granja.sock

  [Install]
  WantedBy = sockets.target
```

```bash
nano /etc/systemd/system/granja.service
  [Unit]
  Description = granja daemon
  Requires = granja.socket
  After = network.target

  [Service]
  User = root
  Group = root
  WorkingDirectory = /srv/granja/
  Environment = "PATH=/srv/granja/venv/bin"
  ExecStart = /srv/granja/venv/bin/gunicorn -b unix:/run/granja.sock -w 2 -m 007 --error-logfile error.log run:app

  [Install]
  WantedBy=multi-user.target
```

```bash
systemctl daemon-reload
systemctl start granja.socket
systemctl status granja.socket
systemctl enable granja.socket

curl --unix-socket /run/granja.sock localhost

systemctl status granja.service
```

```bash
nano /etc/nginx/sites-available/granjalibre.com
  upstream granja {
    server unix:/run/granja.sock fail_timeout=0;
  }
  ...
    location /granja/ { proxy_pass http://granja; }
    location /static { alias /srv/granja/app/static; }

/etc/init.d/nginx restart
```


Contributing
------------

Granja-Libre is developed on [GitLab](https://gitlab.com/wcorrales/granja-libre). You are very welcome to
open [issues](https://gitlab.com/wcorrales/granja-libre/issues) or
propose [merge requests](https://gitlab.com/wcorrales/granja-libre/merge_requests).


Help
----

This README is the best place to start, after that try opening an
[issue](https://gitlab.com/wcorrales/granja-libre/issues).
