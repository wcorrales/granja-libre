from pytz import timezone


class AppConfig(object):
  DEBUG = True
  APP_URL = '/granja'
  DB_NAME = 'granjalibre'
  DB_USER = 'root'
  EMAIL = 'user@granjalibre.com'
  EMAIL_LOGIN = 'user2@granjalibre.com'
  EMAIL_PASSWORD = 'pass-xxx'
  EMAIL_SERVER = '111.111.111.111'                                                                 
  EMAIL_SERVER_PORT = '25'  # 587                                                                                      
  # GOOGLEMAPS_KEY = 'maps-key-xxx'                                                         
  # IMAGES_PATH = '/static/images'                                                                                     
  # THUMB_SIZE = 157, 87                                                                                               
  NODE_PATH = '/usr/bin/node'
  NODE_MODULES_PATH = '/usr/lib/node_modules/'
  QUART_AUTH_COOKIE_NAME = 'QUART_AUTH'
  QUART_AUTH_COOKIE_SECURE = False
  QUART_AUTH_SALT = 'quart auth salt'
  QUART_CSRF_TIME_LIMIT = None
  SECRET_KEY = 'key-xxx'
  RECAPTCHA_PUBLIC_KEY = 'rpub-key-xxx'
  RECAPTCHA_PRIVATE_KEY = 'rpriv-key-xxx'
  TIMEZONE = timezone('America/Costa_Rica')  # datetime.now(tz)                                                        
  BOT_COMMANDS = {'/reproduccion': {'description': 'resumen', 'notebook': True,
                                    'code':5, 'params': ["fecha_inicial", "fecha_final"]},
                  '/produccion': {'description': 'inventario', 'notebook': False,
                                  'code':'growing_1_produ_stock', 'params': []},
                  '/improductivas': {'description': 'inventario', 'notebook': False,
                                     'code':'breeding_1_repro_stock_unproductives', 'params': []},
                  '/servidas': {'description': 'inventario', 'notebook': False,
				        'code':'breeding_1_repro_stock_services', 'params': []},
                  '/productivas': {'description': 'inventario', 'notebook': False,
                                   'code':'breeding_1_repro_stock_productives', 'params': []},
                  '/camadas': {'description': 'inventario', 'notebook': False,
                               'code':'breeding_1_repro_stock_litters', 'params': []}}
  BOT_TOKEN = 'token-xxx'
  BOT_USERS_SCHEMAS_PATH = '/srv/granja/bot_users_schemas.pkl'  # touch bot_users_schemas.pkl                          
  BOT_WEBHOOK_HOST = 'https://granjalibre.com'
  BOT_WEBHOOK_PATH = '/granja_bot/'
