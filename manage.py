'''
from subprocess import call

from config import AppConfig


app_schema = 'public'
config = AppConfig()

# TABLES
for module in ('pgvega', 'notebooks', 'settings', 'breeding', 'growing'):  # , 'whatsapp'
  call("psql %s -v app_schema='%s' -f app/%s/models.sql" % (config.DB_NAME, app_schema, module), shell=True)


call("psql %s -v app_schema='%s' -f app/utils.sql" % (config.DB_NAME, app_schema), shell=True)


# EXAMPLE DATA
if config.DEBUG:
  from app.utils import db

  db = db()
  db.connect(config.DB_NAME, config.DB_USER, debug=config.DEBUG)

  db.execute("""
    INSERT INTO workers
    VALUES (DEFAULT, %s, %s, %s, %s, %s, %s, %s, TRUE, TRUE, FALSE, '{}', NULL, NOW(), NULL, NULL);""",
    ("Wagner Corrales M", "wagnerc4@gmail.com", "+506-8847-4554",
     "200 mts Norte de la Escuela de San Rafael Norte", 9.4123717, -83.7105836, "123"))

  db.commit()
'''

'''
createuser -s root
createdb granjalibre

psql granjalibre -f app/breeding/models_main_tables.sql
psql granjalibre -f app/breeding/models_main_triggers.sql
psql granjalibre -v activity='1' -f app/breeding/models_breeding_tables.sql
psql granjalibre -v activity='1' -f app/breeding/models_breeding_triggers.sql
psql granjalibre -v activity='1' -f app/breeding/models_breeding_querys.sql
psql granjalibre -v activity='1' -f app/breeding/models_growing_triggers.sql
psql granjalibre -v activity='1' -f app/breeding/models_growing_querys.sql

psql granjalibre -v activity='1' -f app/notebooks/models.sql
psql granjalibre -v activity='1' -f app/pgvega/models.sql
psql granjalibre -v activity='1' -f app/utils/utils.sql
psql granjalibre -v activity='1' -f app/workers/models.sql
psql granjalibre -v activity='1' -f app/workers_chats/models.sql

psql granjalibre
  INSERT INTO farms VALUES(1, 'COOP FARM', 'coop', 'user@mail.com', MD5('pass'), 0);
  INSERT INTO activities VALUES(1, 1, 'pig', 'meet', 0, 'granjax', NULL, NULL, 'COOP FARM');
  SET search_path to activity_1;
  INSERT INTO workers VALUES(DEFAULT, 'x', '123', 'user@mail.com', MD5('pass'),
                             'delete', 'settings_roles', TRUE);
'''

