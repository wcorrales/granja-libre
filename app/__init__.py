# from flask_googlemaps import GoogleMaps
from quart import Quart, flash, redirect, session, url_for
from quart_auth import AuthManager, Unauthorized
from quart_csrf import CSRFProtect

from config import AppConfig
from .utils.admin import User
from .utils.db import db
from .utils.websockets import websockets
from .utils.xcaptcha import XCaptcha

am = AuthManager()
cs = CSRFProtect()
db = db(AppConfig.DB_NAME, AppConfig.DB_USER, AppConfig.DEBUG)
ws = websockets()
xc = XCaptcha()


def create_app():
  app = Quart(__name__, static_folder='static')
  app.config.from_object(AppConfig)
  am.user_class = User
  am.init_app(app)
  cs.init_app(app)
  # GoogleMaps(app)
  xc.init_app(app)

  @app.errorhandler(Unauthorized)
  async def redirect_to_login(*_):
    activity_url = session['activity_url'] if 'activity_url' in session else 'granja'
    await flash("Por favor inicie sesión!")
    return redirect(url_for("workers.login", activity_url=activity_url))

  from .pgvega import pgvega
  app.register_blueprint(pgvega, url_prefix=app.config['APP_URL'])

  from .notebooks import notebooks
  app.register_blueprint(notebooks, url_prefix=app.config['APP_URL'])

  from .messaging import messaging
  app.register_blueprint(messaging)
  cs.exempt(messaging)

  from .breeding import breeding, get_breeding_menu
  app.register_blueprint(breeding, url_prefix=app.config['APP_URL'])

  from .growing import growing, get_growing_menu
  app.register_blueprint(growing, url_prefix=app.config['APP_URL'])

  from .workers import workers, get_settings_menu
  app.register_blueprint(workers)

  from .workers_chats import workers_chats
  app.register_blueprint(workers_chats, url_prefix=app.config['APP_URL'])

  app.menu = [get_breeding_menu, get_growing_menu, get_settings_menu]

  return app
