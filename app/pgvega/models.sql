CREATE SCHEMA :app_schema;
SET search_path TO :app_schema;

-- ************************************** --
------------------ TABLES ------------------
-- ************************************** --
CREATE TABLE querys (
  id SERIAL PRIMARY KEY NOT NULL,
  query TEXT NOT NULL,
  defs TEXT NOT NULL,
  code VARCHAR(50) UNIQUE NOT NULL,
  title VARCHAR(100) UNIQUE NOT NULL,
  CHECK (TRIM(query) <> '' AND query !~* '[^a-z0-9 _\]\[{}()"'';:,.&|?!~$%<>=/*+-\\^]+'),
  CHECK (TRIM(defs) <> '' AND defs !~* '[^a-z0-9 _\]\[{}()"'';:,.&|?!~#$%<>=/*+-\\^]+'),
  CHECK (TRIM(code) <> '' AND code !~* '[^a-z0-9_]+'),
  CHECK (TRIM(title) <> '' AND title !~* '[^a-z0-9 @#&()"'';:,.%<>=/*+-]+')
);

CREATE TABLE crons (
  id SERIAL PRIMARY KEY NOT NULL,
  cron VARCHAR(100) UNIQUE NOT NULL,
  CHECK (TRIM(cron) <> '' AND cron !~* '[^a-z0-9 /*,-]+')
);

-- INSERT INTO crons VALUES(DEFAULT, '*/5 5-17 * * 1-5');
-- INSERT INTO crons VALUES(DEFAULT, '0 6 * * 1-6');
-- INSERT INTO crons VALUES(DEFAULT, '0 12 * * 1-6');
-- INSERT INTO crons VALUES(DEFAULT, '0 5 1 * *');


CREATE TABLE alerts (
  id SERIAL PRIMARY KEY NOT NULL,
  cron_id INTEGER NOT NULL,
  query_id INTEGER NOT NULL,
  type VARCHAR(10) NOT NULL,
  CHECK (type in ('sms', 'calendar', 'notify')),
  FOREIGN KEY(cron_id) REFERENCES crons(id),
  FOREIGN KEY(query_id) REFERENCES querys(id)
);


CREATE TABLE alerts_targets (
  id SERIAL PRIMARY KEY NOT NULL,
  alert_id INTEGER NOT NULL,
  target VARCHAR(50) NOT NULL,
  CHECK (TRIM(target) <> '' AND target !~* '[^a-z0-9 &#;.+-]+'),
  FOREIGN KEY(alert_id) REFERENCES alerts(id) ON DELETE CASCADE
);


CREATE TABLE alerts_done (
  id INTEGER PRIMARY KEY NOT NULL,
  last_day DATE NOT NULL,
  FOREIGN KEY(id) REFERENCES alerts(id) ON DELETE CASCADE
);

-- ************************************* --
------------------ VIEWS ------------------
-- ************************************* --


-- ************************************** --
------------------ QUERYS ------------------
-- ************************************** --


-- **************************************** --
------------------ FUNCTIONS -----------------
-- **************************************** --


-- **************************************** --
------------------ TRIGGERS ------------------
-- **************************************** --
