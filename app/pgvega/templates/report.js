function set_table_head($table, head) {
  var filters = $.map(head, function(x){ return '<input type="text" size="4" />'; });
  $table.find("thead").html('<tr><th>' + head.join('</th><th>') + '</th></tr>' +
                            '<tr><th>' + filters.join('</th><th>') + '</th></tr>');
  if (!$table.closest('div').hasClass('modal-body')) {
    $table.stickyTableHeaders("destroy");
    $table.stickyTableHeaders({fixedOffset: $("header.navbar").height()});
    // $table.find('thead tr:first th').css({'position':'sticky', 'top':'50px', 'background-color':'white'});
  }
}

///////////////////////////// TABLE LOCAL /////////////////////////////
// SORT ARRAY
//var dp = /^(\d{2})[\/\- ](\d{2})[\/\- ](\d{4})/;
//a.match(dp) -> a.replace(dp,'$3$2$1')
//b.match(dp) -> b.replace(dp,'$3$2$1')
function cp(a,b){
  var c=a.match(/^\d{4}\-\d{2}\-\d{2}/)?a:parseFloat(a.replace(/\$|\,/g,'')) || a.toLowerCase(),
      d=b.match(/^\d{4}\-\d{2}\-\d{2}/)?b:parseFloat(b.replace(/\$|\,/g,'')) || b.toLowerCase();
  return (c>d)?1:(c<d)?-1:0;
}

function set_table_actions($table) {
  // SORT TABLE
  $table.find("thead").on("click", "tr:first > th", function() {
    if ($(this).html().indexOf('checkbox') === -1) {
      var body = $table.data("body");
      if($(this).hasClass("success") || $(this).hasClass("warning")) {
        body.reverse();
        $(this).toggleClass("success warning");
      } else {
        var index = $(this).index();
        body.sort(function(a,b) { return cp(stripHtml(String(a[index])),
                                            stripHtml(String(b[index]))); });
        $table.find("thead tr:first th").removeClass("success warning");
        $(this).addClass("success");
      }
      $table.data("body", body);
      var rows = body.slice(0);
      $.map($table.find("thead input[type='text']"), function(item, index){
        var i=0, value=$(item).val().toLowerCase();
        if (value) {
          while (i < rows.length) {
            if (stripHtml(String(rows[i][index])).toLowerCase().indexOf(value) > -1) i++;
            else rows.splice(i, 1);
          }
        }
      });
      set_table_body($table, {"body": rows}, false, false);
    }
  });

  // FILTER TABLE
  $table.find("thead").on("keyup", "tr:last > th > input", function() {
    var rows = $table.data("body").slice(0);
    $.map($table.find("thead input[type='text']"), function(item, index){
      var i=0, value=$(item).val().toLowerCase();
      if (value) {
        while (i < rows.length) {
          if (stripHtml(String(rows[i][index])).toLowerCase().indexOf(value) > -1) i++;
          else rows.splice(i, 1);
        }
      }
    });
    set_table_body($table, {"body": rows}, false, false);
  });

  // SELECT ALL
  $table.find("thead").on("click", "tr:first > th:eq(0) > input[type='checkbox']", function() {
    if ($(this).is(':checked')) $table.find("tbody tr").addClass('danger');
    else $table.find("tbody tr").removeClass('danger');
  });

  // SELECT ROW
  $table.find("tbody").on("click", "tr", function() {
    $(this).toggleClass("danger");
    var rows = $table.data("rows_selected"),
        row = $.map($(this).find("td"), function(td){
                var input = $(td).find("input");
                return input.length?input.val():$(td).text();
              });
    if ($(this).hasClass("danger")) rows[row[0]] = row;
    else delete rows[row[0]];
    $table.data("rows_selected", rows);
  });
/*
  var ids = $.map($("#table tbody tr.danger"), function(tr){
              return $(tr).find("td:first").text();
            });
  var rows = $.map($("#table tbody tr.danger"), function(tr){
               return [$.map($(tr).find("td"), function(td){ return $(td).text(); })];
             });
*/

  // XLS
  // TODO xls in tabs
  $table.find("caption").on("click", "button[name='xls']", function() {
    var rows=$.map($table.data("body").concat($table.data("last")), function(r){
               return [$.map(r, function(x){
                 var y = (typeof x === 'string')?stripHtml(x):String(x);
                 return (y.replace(/\d+(,\d+)*(\.\d+)?$/, '') == '' &&
                         y.replace(/,/g,'').length < 12)?
                          parseFloat(y.replace(/,/g,'')):y;
               })];
             });
    exportXLS($.map($table.find("thead tr:first th"), function(z){ return $(z).text(); }), rows);
  });
}

function set_table_body($table, tmp, start, paginate) {
  if (!paginate && !tmp["body"].length) {
    alert("sin resultados!");
    $table.find("tbody").empty();
  } else {
    if (!$table.data("head")) {
      $table.data("head", true);
      set_table_head($table, tmp["head"]);
      set_table_actions($table);
    }
    if (start) {
      $table.data("body", tmp["body"]);
      $table.data("foot", tmp["foot"]);
      $table.data("last", [tmp["foot"][0].slice(0)]);
      $table.data("rows_selected", {});
      $table.find("thead tr:first th").removeClass("success warning");
    }
    if (!paginate) {
      var foot=$table.data("foot"), values={}, last=[];
      for (var k in tmp["body"]) {
        for (var i=0; i<foot[1].length; ++i) {
          if (foot[1][i] != "" && tmp["body"][k][i] && tmp["body"][k][i] != "") {
            if (!values[i]) values[i] = [];
            if (typeof tmp["body"][k][i] === 'number') values[i].push(tmp["body"][k][i]);
            else values[i].push(Number(stripHtml(tmp["body"][k][i]).replace(/,/g, "")) || 0);
          }
        }
      }
      for (var i=0; i<foot[1].length; ++i) {
        if (!values[i]) values[i] = [0];
        if (foot[1][i] != "") {
          switch(foot[1][i]) {
            case "sum": last[i] = values[i].reduce(function(a,b){ return a + b; }); break;
            case "avg": last[i] = values[i].reduce(function(a,b){ return a + b; }) /
                                  values[i].length; break;
            case "min": last[i] = values[i].reduce(function(a,b){ return (a < b)?a:b; }); break;
            case "max": last[i] = values[i].reduce(function(a,b){ return (a > b)?a:b; }); break;
            case "count": last[i] = values[i].length; break;
          }
          last[i] = formatCurrency(last[i], (foot[1][i] == "count")?0:2);
        } else {
          last[i] = "";
        }
      }
      $table.data("rows", tmp["body"].slice(0));
      $table.data("last", [last]);
    }
    var rows = $table.data("rows"),
        last = $table.data("last"),
        begin = 0, end = rows.length,
        $ul = $table.find("caption ul.pagination"),
        keys_selected = Object.keys($table.data("rows_selected") || {});
    if ($ul.length) {
      var count = rows.length,
          lines = parseInt($table.find("select[name='lines']").val(), 10) || count,
          pages = Math.ceil(count / lines),
          page = Math.min($ul.pagination("getCurrentPage"), pages);
      begin = Math.max(page - 1, 0) * lines;
      end = begin + lines;
      $ul.pagination("updateItems", count);
      if ($ul.pagination("getPagesCount") != pages) {
        $ul.pagination("updateItemsOnPage", lines);
        $ul.pagination("selectPage", page);
      }
    }
    $table.find("tbody").html($.map(rows.slice(begin, end), function(row){
      var i = keys_selected.indexOf(stripHtml(String(row[0])));
      return '<tr'+ ((i > -1)?' class="danger"':'') +'><td>'+ row.join('</td><td>') +'</td></tr>';
    }).join(""));
    if (rows.length <= end) {
      $table.find("tfoot").html($.map(last, function(row){
        return '<tr><th>' + row.join('</th><th>') + '</th></tr>';
      }).join(""));
    } else {
      $table.find("tfoot").empty();
    }
  }
}

function set_table_local($table, all_lines) {
  var $caption = $table.find("caption");
  $caption.html('<div class="btn-group paginate" role="group" aria-label="...">' +
                '  <div class="btn-group" role="group">' +
                '    <p class="form-control-static"><strong>Lineas</strong></p>' +
                '  </div>' +
                '  <div class="btn-group" role="group">' +
                '    <select class="form-select" name="lines">' +
                '      <option'+(all_lines?"":" selected")+'>10</option>' +
                '      <option>50</option>' +
                '      <option>150</option>' +
                '      <option>450</option>' +
                '      <option value="all"'+(all_lines?" selected":"")+'>todo</option>'+
                '    </select>' +
                '  </div>' +
                '  <div class="btn-group" role="group">' +
                '    <ul class="pagination" style="margin:0;"></ul>' +
                '  </div>' +
                '  <div class="btn-group" role="group">' +
                '    <button class="btn btn-success" name="xls">xls</button>' +
                '  </div>' +
                '</div>');
  $caption.find("select[name='lines']").change(function(){
    set_table_body($table, {}, false, true);
  });
  $caption.find("ul.pagination").pagination({
    displayedPages: 3, edges: 1, prevText: "", nextText: "",
    onPageClick: function(p, e){ if (e) set_table_body($table, {}, false, true); }
  });
  $table.data("head", false);
}

function set_table(rs) {
  var $table = $('<table class="table table-striped">' +
                 '  <caption></caption>' +
                 '  <thead class="bg-white"></thead>' +
                 '  <tbody></tbody>' +
                 '  <tfoot></tfoot>' +
                 '</table>');
  set_table_local($table, rs["all_lines"]);
  set_table_body($table, rs, true, false);
  return $table;
}


///////////////////////////// TABLE AJAX /////////////////////////////
/*
function set_table_ajax_body($table) {
  var pd = new FormData(),
      $ul = $table.find("caption ul.pagination");
      lines = parseInt($table.find("select[name='lines']").val(), 10),
      page = $ul.pagination("getCurrentPage"),
      filter = $table.data("filter")?JSONfn.parse($table.data("filter")):{};
  pd.append("action", $table.data("action"));
  pd.append("sort_col", $table.data("sort_col"));
  pd.append("sort_dir", $table.data("sort_dir"));
  pd.append("filter", $table.data("filter"));
  pd.append("lines", lines);
  pd.append("page", page);
  request(db_url, pd, function(rs){
    if (!rs["body"].length){
      alert("sin resultados!");
      $table.find("tbody").empty();
    } else {
      $ul.pagination("updateItems", rs["count"]);
      if ($ul.pagination("getPagesCount") != Math.ceil(rs["count"] / lines)) {
        $ul.pagination("updateItemsOnPage", lines);
        $ul.pagination("selectPage", page);
      }
      if (!$table.data("head")) {
        set_table_head($table, rs["head"]);
        $table.find("thead tr:first th:eq(" + $table.data("sort_col") + ")")
              .addClass(($table.data("sort_dir").toLowerCase()=="asc")?"success":"warning");
        $table.data("head", true);
      }
      for (var k in filter) {
        var $input = $table.find("thead input:eq(" + k + ")");
        if ($input.val() != filter[k]) $input.val(filter[k]);
      }
      $table.find("tbody").html($.map(rs["body"], function(row){
        return '<tr><td>' + row.join('</td><td>') + '</td></tr>';
      }).join(""));
//      $table.find("tfoot").html('<tr><th>' + rs["foot"][0].join('</th><th>') + '</th></tr>');
    }
  });
}

function set_table_ajax($table, data) {
  var $caption = $table.find("caption");
  $caption.append('<div class="btn-group paginate" role="group" aria-label="...">' +
                  '  <div class="btn-group" role="group">' +
                  '    <p class="form-control-static"><strong>Lineas</strong></p>' +
                  '  </div>' +
                  '  <div class="btn-group" role="group">' +
                  '    <select class="form-select" name="lines">' +
                  '      <option>10</option>' +
                  '      <option>50</option>' +
                  '      <option>150</option>' +
                  '      <option>450</option>' +
                  '      <option value="all">todo</option>' +
                  '    </select>' +
                  '  </div>' +
                  '  <div class="btn-group" role="group">' +
                  '    <ul class="pagination" style="margin:0;"></ul>' +
                  '  </div>' +
                  '</div>');
  $caption.find("select[name='lines']").change(function(){
    set_table_ajax_body($table);
  });
  $caption.find("ul.pagination").pagination({
    displayedPages: 3, edges: 1, prevText: "", nextText: "",
    onPageClick: function(p, e){ if (e) set_table_ajax_body($table); }
  });
  $table.data("filter", JSONfn.stringify(data["filter"]));
  $table.data("sort_col", data["sort_col"]);
  $table.data("sort_dir", data["sort_dir"]);
  $table.data("head", false);
}

$(function() {
  // SORT TABLE AJAX
  $("table.resumen_ajax > thead").on("click", "tr:first > th", function() {
    var $table = $(this).parents("table"),
        direction = "";
    if($(this).hasClass("success") || $(this).hasClass("warning")) {
      $(this).toggleClass("success warning");
      direction = $(this).hasClass("success")?"ASC":"DESC";
    } else {
      $table.find("thead tr:first th").removeClass("success warning");
      $(this).addClass("success");
      direction = "ASC";
    }
    $table.data("sort_col", $(this).index());
    $table.data("sort_dir", direction);
    set_table_ajax_body($table);
  });

  // FILTER TABLE AJAX
  var resumen_timer=null;
  $("table.resumen_ajax > thead").on("keyup", "tr:last > th > input", function() {
    var $table = $(this).parents("table"),
        filter = {};
    window.clearTimeout(resumen_timer);
    resumen_timer = window.setTimeout(function() {
      $.map($table.find("thead input"), function(item, index){
        var value=$(item).val();
        if (value) filter[index] = value;
      });
      $table.data("filter", JSONfn.stringify(filter));
      set_table_ajax_body($table);
    }, 800);
  });

  // SELECT TABLE AJAX
  $("table.resumen_ajax tbody").on("click", "tr", function() {
    $(this).toggleClass("danger");
  });
});
*/

///////////////////////////// SET REPORT /////////////////////////////
function set_crosstable(rs) {
  var $table = $('<table class="table table-striped resumen">' +
                 '  <caption></caption>' +
                 '  <thead class="bg-white"></thead>' +
                 '  <tbody></tbody>' +
                 '  <tfoot></tfoot>' +
                 '</table>');
  $table.find("caption").html('<button class="btn btn-info" name="xls">xls</button>');
  $table.find("thead").html('<tr><th>' + rs["head"].join('</th><th>') + '</th></tr>');
  $table.stickyTableHeaders("destroy");
  $table.stickyTableHeaders({fixedOffset: $("header.navbar").height()});
  // $table.find("thead tr:first th").css({'position':'sticky', 'top':'50px', 'background-color':'white'});
  $table.find("tbody").html($.map(rs["body"], function(row){
    return '<tr><td>' + row.join('</td><td>') + '</td></tr>';
  }).join(""));
  // $table.find("tfoot").html('<tr><th>' + rs["foot"][0].join('</th><th>') + '</th></tr>');
  $table.data("head", rs["head"]);
  $table.data("body", rs["body"]);
  $table.find("caption").on("click", "button[name='xls']", function() {
    exportXLS($table.data("head"), $table.data("body"));
  });
  return $table;
}

function set_pivot(rs) {
  return rs['table'];
}

function set_graph(rs) {
  return '<embed type="image/svg+xml" src='+ rs["graph"] +' />';
}

function set_resumen(rs) {
  var $table = $('<table class="table table-striped">' +
                 '  <tbody></tbody>' +
                 '</table>');
  $table.find("tbody").html($.map(rs, function(row){
    return '<tr><td>' + row.join('</td><td>') + '</td></tr>';
  }).join(""));
  return $table;
}

function set_content($div, rs) {
  var $table = null, width = 0;
  for (var k in rs["content"]) {
    var data = rs["content"][k]["data"];
    if ("title" in rs["content"][k]) $div.append('<h3>'+rs["content"][k]["title"]+'</h3>');
    switch(rs["content"][k]["type"]) {
      case "table":
      //case "crosstable":
        data["all_lines"] = $div.data("all_lines");
        $div.append(set_table(data));
        width = 0;
      break;
      case "crosstable":
      case "crosstable_sum":
      case "crosstable_granja":
      case "crosstable_conta":
        $div.append(set_crosstable(data));
        width = 0;
      break;
      case "pivot":
      case "pandas_pivot":
        if (width == 0) {
          $table = $('<table><tr></tr></table>');
          $div.append($table);
        }
        $table.find("tr:eq(0)").append('<td>' + set_pivot(data) + '</td>');
        width = ((width + data["width"]) > 400)?0:(width + data["width"]);
      break;
      case "graph":
      case "graphviz":
        if (width == 0) {
          $table = $('<table><tr></tr></table>');
          $div.append($table);
        }
        $table.find("tr:eq(0)").append('<td>' + set_graph(data) + '</td>');
        width = ((width + data["width"]) > 400)?0:(width + data["width"]);
      break;
      case "resumen":
        $div.append(set_resumen(data));
        width = 0;
      break;
    }
  }
}

function set_buttons($buttons, rs) {
  $buttons.html('<h2 class="col-auto" style="display:inline;">' + rs['title'] + '</h2>&nbsp;' +
                '<input type="text" name="d1" class="col-auto form-control date" style="width:150px;" autocomplete="off" placeholder="desde . . ." />'+
                '<input type="text" name="d2" class="col-auto form-control date" style="width:150px;" autocomplete="off" placeholder="hasta . . ." />'+
                '<input type="text" name="v" class="col-auto form-control" style="width:200px;" autocomplete="off" placeholder="valor . . ." />&nbsp;' +
                '<select name="g1" class="col-auto form-select" style="width:125px;">' +
                '<option>year</option><option>month</option><option>week</option>' +
                '<option>worker</option><option>parity</option><option>race</option>' +
                '</select>&nbsp;' +
                '<select name="g2" class="col-auto form-select" style="width:125px;">' +
                '<option></option>' +
                '<option>year</option><option>month</option><option>week</option>' +
                '<option>worker</option><option>parity</option><option>race</option>' +
                '<option>animal</option>' +
                '</select>&nbsp;' +
                '<select name="c" class="col-auto form-select" style="width:100px;">' +
                '<option>moneda</option><option>crc</option><option>usd</option>' +
                '</select>&nbsp;' +
                '<button class="col-auto btn btn-success" data-pdf="0" name="ver">ver</button>&nbsp;' +
                '<button class="col-auto btn btn-primary" data-pdf="1" name="ver">pdf</button>');
  if (!rs["d1"]) $buttons.find("input[name='d1']").hide();
  if (!rs["d2"]) $buttons.find("input[name='d2']").hide();
  if (!rs["g1"]) $buttons.find("select[name='g1']").hide();
  if (!rs["g2"]) $buttons.find("select[name='g2']").hide();
  if (!rs["v"]) $buttons.find("input[name='v']").hide();
  if (!rs["c"]) $buttons.find("select[name='c']").hide();
}

/*
function set_buttons_click($buttons, url) {
  $buttons.find("button[name='ver']").click(function(){
    var pd = new FormData(),
        $div = $buttons.parent();
    pd.append("d1", $buttons.find("input[name='d1']").val());
    pd.append("d2", $buttons.find("input[name='d2']").val());
    pd.append("g1", $buttons.find("select[name='g1']").val());
    pd.append("g2", $buttons.find("select[name='g2']").val());
    pd.append("v", $buttons.find("input[name='v']").val());
    pd.append("c", $buttons.find("select[name='c']").val());
    request(url, pd, function(rs){
      $div.children().not($buttons).remove();
      set_content($div, rs);
    });
  });
}
*/

/////////////////////////// TABLE SUBQUERY ///////////////////////////
/*
function set_subquery($div, subquery, prev_subquery) {
  var pd = new FormData(),
      moneda = $div.find("div.buttons select[name='c']").val();
  pd.append("action", "select_query_saved");
  pd.append("code", $div.attr("id")+"_subquery" + (prev_subquery?"_subquery":""));
  pd.append("d1", $div.find("div.buttons input[name='d1']").val());
  pd.append("d2", $div.find("div.buttons input[name='d2']").val());
  pd.append("g1", $div.find("select[name='g1']").val());
  pd.append("g2", $div.find("select[name='g2']").val());
  pd.append("v", $div.find("div.buttons input[name='v']").val());
  pd.append("c", moneda);
  pd.append("subquery", subquery.escape());
  pd.append("prev_subquery", prev_subquery?prev_subquery.escape():null);
  request(db_url, pd, function(rs){
    var $modal = prev_subquery?$("#subquery2_modal"):$("#subquery_modal"),
        $body = $modal.find("div.modal-body");
    $modal.data("div_id", $div.attr("id"));
    $modal.data("subquery", subquery);
    $modal.find("div.modal-header h3").html(rs["title"] + ": "+
                                            (prev_subquery?prev_subquery + " - ":"") +
                                            subquery +
                                            ((moneda != 'moneda')?" - "+moneda:""));
    $body.data("all_lines", false);
    $body.empty();
    set_content($body, rs);
    $modal.modal("show");
  });
}

$(function() {
  $("div.tab-pane").on("click", "span.subquery", function() {
    set_subquery($(this).parents("div.tab-pane"), $(this).text(), null);
  });

  $("#subquery_modal").on("click", "span.subquery", function() {
    var $modal = $("#subquery_modal");
    set_subquery($("#"+$modal.data("div_id")), $(this).text(), $modal.data("subquery"));
  });

  // XLS
  $("div.modal div.modal-footer button.xls_subquery").click(function(){
    var $modal = $(this).parents("div.modal"),
        $table = $modal.find("div.modal-body table");
    exportXLS($.map($table.find("thead tr:first th"), function(n){ return $(n).text(); }),
              $table.data("body").concat($table.data("last")));
  });

  // PRINT
  $("div.modal div.modal-footer button.print_subquery").click(function(){
    var $modal = $(this).parents("div.modal"),
        $table0 = $modal.find("div.modal-body table:eq(0)").clone(),
        $table1 = $modal.find("div.modal-body table:eq(1)").clone();
    $("caption", $table0).remove();
    newTab({"title":"<h2>"+$modal.find("div.modal-header h3").html()+"</h2>",
            "content":'<table class="table table-striped">'+$table0.html()+$table1.html()+'</table>'});
  });
});
*/
