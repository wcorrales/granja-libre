from json import dumps, loads
from quart import make_response, render_template, request, session, url_for
from quart_auth import current_user, login_required

from app import db
from . import pgvega
from .report_utils import set_content, set_pdf, set_header_html, set_report_html
from ..utils.admin import check_admin, check_user
from ..utils.utils import JsonEncoder, escape


############################# REPORTS #############################
def get_reports_menu(code):
  # WHERE code ~ '^(forms|resumens|accounting|settings)((?!_subquery).)+$'
  return db.all("""SELECT code, title FROM querys WHERE code ~ %s AND code ~ '^(.+)((?!_subquery).)+$'
                   ORDER BY code;""", (code, ), as_dict=True)


def get_schema_titles():
  return {"schema": db.all("""SELECT c.relname AS table,
                                     (SELECT STRING_AGG(attname, ',') FROM pg_catalog.pg_attribute
                                      WHERE attrelid=c.oid AND attnum > 0 AND NOT attisdropped) AS columns
                              FROM pg_catalog.pg_class c
                              JOIN pg_catalog.pg_namespace n ON c.relnamespace=n.oid
                              WHERE c.relkind IN ('r', 'v') AND n.nspname=(SELECT CURRENT_SCHEMA)
                              ORDER BY c.relname;""", as_dict=True),
          "titles": db.all("""SELECT code, unescape(title) AS title, MAX(LENGTH(code)) OVER() AS l
                              FROM querys ORDER BY code;""", as_dict=True)}


def get_report_data(code, rq, pdf=False):
  content = []
  title, defs = tuple(db.one("SELECT title, defs FROM querys WHERE code=%s;", (code, )))
  defs = loads(defs.replace('\\\\', '\n'))
  codes = defs[0]['defs'] if len(defs) > 0 and 'type' in defs[0] and defs[0]['type'] == 'group' else [code]
  rows = db.all("SELECT query, defs, title FROM querys WHERE code IN %s ORDER BY code;", (tuple(codes), ))
  for row in rows:
    data = db.all(row[0].replace('\\\\', '\n'), rq)
    defs = loads(row[1].replace('\\\\', '\n'))
    if len(defs) < 1:
      cols = db.description()
      defs = [{"type":"table", "defs":[{"head":c} for c in cols]}]
    tmp = set_content(defs, data, pdf)
    if len(rows) > 1: tmp[0]['title'] = row[2]
    content = content + tmp
  rs = {"code": code, "title": title, "content": content}
  return rs if pdf else dumps(rs, cls=JsonEncoder)


@pgvega.route('/get_report/<code>', methods=['GET', 'POST'])
@login_required
@check_user
@db.wrapper
async def get_report(code):
  if request.method == 'GET':
    row = db.one("""
      SELECT title,
             query LIKE %s AS d1,
             query LIKE %s AS d2,
             query LIKE %s AS g1,
             query LIKE %s AS g2,
             query LIKE %s AS v,
             query LIKE %s AS c
      FROM querys WHERE code=%s;""",
      ('%%%%(d1)s%%', '%%%%(d2)s%%', '%%%%(g1)s%%', '%%%%(g2)s%%', '%%%%(v)s%%', '%%%%(c)s%%', code), as_dict=True)
    if not row:
      raise Exception('Error: reporte no ingresado!')
    return await render_template('templates/report.html', code=code, data=dumps(row, cls=JsonEncoder))
  else:
    rq = await request.form
    return get_report_data(code, rq, pdf=False)


@pgvega.route('/get_report_pdf/<code>', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_report_pdf(code):
  rq = await request.form
  try:
    activity = db.one("""SELECT unescape(activity) FROM public.activities
                         WHERE id=REPLACE((SELECT CURRENT_SCHEMA), 'activity_', '')::INT;""")[0]
    pdf = set_pdf(set_header_html('Granja Libre - ' + activity),
                  set_report_html(get_report_data(code, rq, pdf=True), rq['params']))
    rs = await make_response(pdf)
    rs.content_type = 'application/pdf'
    rs.headers["Content-Disposition"] = "filename=%s.pdf" % code
  except Exception as e:
    rs = await make_response("db error -> " + str(e), 500)
  return rs


#######################################################################
@pgvega.route('/admin-reports/build')
@login_required
@check_admin
@db.wrapper
async def reports():
  return await render_template('templates/reports.html', title="Reportes",
                               schema_titles=get_schema_titles())


@pgvega.route('/admin-reports/query_get_def', methods=['POST'])
@login_required
@check_admin
@db.wrapper
async def get_query_def():
  rq = await request.form
  return dumps(db.one("SELECT * FROM querys WHERE code=%s;", (rq['code'], )), cls=JsonEncoder)


@pgvega.route('/admin-reports/query_set', methods=['POST'])
@login_required
@check_admin
@db.wrapper
async def set_query():
  rq = await request.form
  if rq['action'] == 'insert':
    db.execute("INSERT INTO querys VALUES (DEFAULT, %s, %s, %s, %s);",
               (rq['query'], rq['defs'], rq['code'], escape(rq['title'])))
  elif rq['action'] == 'update':
    db.execute("UPDATE querys SET query=%s, defs=%s, title=%s WHERE code=%s;",
               (rq['query'], rq['defs'], escape(rq['title']), rq['code']))
  else:
    db.execute("DELETE FROM querys WHERE code=%s;", (rq['code'], ))
  db.commit()
  return dumps(get_schema_titles(), cls=JsonEncoder)



############################# ALERTS #############################
# def get_alerts_select(code):
#   return db.all("SELECT id, title FROM alerts WHERE code ~ %s;", (code, ), as_dict=True)


@pgvega.route('/admin-reports/alerts')
@login_required
@check_admin
@db.wrapper
async def alerts():
  return await render_template('templates/alerts.html', title="Alertas")


'''
def select_alerts_data(dbObj, rq):
  crons = dbObj.getRow("SELECT ARRAY_AGG(cron) FROM crons;")
  querys = dbObj.getRow("SELECT ARRAY_AGG(code) FROM querys WHERE code LIKE 'alerts_%';")
  return {"crons":crons[0] if crons else [],
          "querys":querys[0] if querys else [],
          #"phones":dbObj.getRows("""
          #           SELECT (SELECT persona FROM personas WHERE id=pt.persona_id) AS persona,
          #                  ARRAY_AGG(telefono || '_' || nota)
          #           FROM personas_telefonos pt GROUP BY persona_id ORDER BY persona;"""),
          "workers":dbObj.getRow("SELECT ARRAY_AGG(worker) FROM workers;")[0]}


def select_alerts(dbObj, rq):
  return dbObj.getRows("""SELECT id,
                                 (SELECT cron FROM crons WHERE id=a.cron_id),
                                 (SELECT code FROM querys WHERE id=a.query_id),
                                 (SELECT ARRAY_AGG(target) FROM alerts_targets
                                  WHERE alert_id=a.id)
                          FROM alerts a WHERE type=%s;""", (rq['type'], ))


def insert_alert(dbObj, rq):
  a_id = dbObj.getRow("""INSERT INTO alerts
                         VALUES(DEFAULT, (SELECT id FROM crons WHERE cron=%s),
                                (SELECT id FROM querys WHERE code=%s), %s)
                         RETURNING id;""", (rq['cron'], rq['query'], rq['type']))[0]
  items = loads(rq['items'])
  for item in items:
    dbObj.execute("INSERT INTO alerts_targets VALUES(DEFAULT, %s, %s);", (a_id, item))
  return select_alerts(dbObj, rq)


def delete_alert(dbObj, rq):
  dbObj.execute("DELETE FROM alerts WHERE id=%s;", (rq['id'], ))
  return select_alerts(dbObj, rq)


# TODO on close notify INSERT INTO alerts_done
'''

############################# CRONS #############################
#def insert_cron(dbObj, rq):
#  dbObj.execute("INSERT INTO crons VALUES(DEFAULT, %s);", (rq['cron'], ))
#  '''
#  from crontab import CronTab
#
#  cron = CronTab()
#  cron.new(command='/bin/speaker-test -t wav -f 1000 -l 1')
#  cron.write()
#
#  cron.remove_all()
#  for job in cron: print(job)
#  '''
#  return select_crons(dbObj, rq)
#
#
#def delete_cron(dbObj, rq):
#  dbObj.execute("DELETE FROM crons WHERE cron=%s;", (rq['cron'], ))
#  return select_crons(dbObj, rq)
#
#

'''
# from email.mime.application import MIMEApplication
# from email.mime.multipart import MIMEMultipart
# from email.mime.text import MIMEText
# from smtplib import SMTP

#def print_email(dbObj, rq):
#  email_oficina = dbObj.getRow("""
#     SELECT (SELECT email_oficina FROM personas WHERE id=documentos.persona_id) AS persona
#     FROM documentos WHERE id=%s""", (rq['a_id'], ))[0]
#  if not email_oficina:
#    raise Exception('Email oficina no esta definido!')
#  activity = dbObj.getRow("SELECT activity FROM public.activities WHERE id=%s;",
#                          (sub('activity_', '', rq['schema']), ))[0]
#  pdf_data = print_pdf(dbObj, rq)
#  msg = MIMEMultipart()
#  msg['Subject'] = '%s' % activity
#  msg['From'] = 'contabilidad@granjalibre.com'
#  msg['To'] = email_oficina
#  msg.attach(MIMEText("""
#***  No conteste a este mensaje ya que no recibirá ninguna respuesta.  ***
#Muchas Gracias.
#  """, 'plain'))
#  pdf = MIMEApplication(pdf_data, _subtype='pdf')
#  pdf.add_header('Content-Disposition', 'attachment', filename='fct.pdf')
#  msg.attach(pdf)
#  s = SMTP('localhost')
#  s.send_message(msg)
#  s.quit()
'''
