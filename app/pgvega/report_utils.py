from base64 import b64encode
from cairosvg import svg2png
from collections import defaultdict
from datetime import date, datetime, timedelta
from decimal import Decimal
from graphviz import Digraph
from io import StringIO, BytesIO
from json import loads, dumps
from math import floor
import numpy as np
from pandas import DataFrame, pivot_table
from pydot import graph_from_dot_data
from re import sub
from simpleeval import EvalWithCompoundTypes as SimpleEval
from subprocess import Popen, PIPE
from xhtml2pdf import pisa

from config import AppConfig
from ..utils.utils import round_half_up, JsonEncoder


def add_days(x, y):
  # x = x if isinstance(x, date) else datetime.strptime(x, "%Y-%m-%d")
  return x + timedelta(days=y)


def get_days(x):
  return floor(x/86400)


def now_ts():
  return int(datetime.now(AppConfig.TIMEZONE).strftime("%s"))


def sql_local(x):
  return datetime.strptime(x, "%Y-%m-%d").strftime("%d-%m-%Y")


# def sql_lote(x):
#   return datetime.strptime(x, "%Y-%m-%d").strftime("%j")

# def sql_ts(x):
#   return datetime.strptime(x, "%Y-%m-%d").strftime("%s")


def thous(x):
  return sub(r'(\d{3})(?=\d)', r'\1,', str(x)[::-1])[::-1]


def ts_hour(x):
  return datetime.fromtimestamp(x).strftime("%H:%M:%S")

def ts_sql(x):
  return datetime.fromtimestamp(x).strftime("%Y-%m-%d")


def ts_week(x):
  return int(datetime.fromtimestamp(x).strftime("%W"))


def set_value(fn, row):
  s = SimpleEval()
  s.names = {"r":row}
  s.functions = {"str":str, "int":int, "float":float, "round":round,
                 "abs":abs, "floor":floor, "sum":sum, "enumerate":enumerate,
                 "get_days":get_days, "add_days":add_days, "sql_local":sql_local,
                 "ts_sql":ts_sql, "ts_hour":ts_hour, "ts_week":ts_week,
                 "now_ts":now_ts}
  try:
    if isinstance(fn, list):
      return [s.eval(x) for x in fn]
    return s.eval(fn)
  except Exception as e:
    return fn


'''
from simpleeval import simple_eval

def safe_lambda(x, y):
  return lambda z: simple_eval(y, names={x:z})

list(filter(safe_lambda('x', 'x > 2'), [1, 2, 3, 4, 5, 6]))

f = simple_eval("sum(list(map(safe_lambda('x', 'x[1] if x[0] == 1 else 0'), r)))",
                names={"r":[[1, 2], [1, 3], [2, 4], [2, 5]]},
                functions={"sum":sum, "list":list, "map":map, "safe_lambda":safe_lambda})
'''


def set_class(c, v):
  if c == 'image':
    return '<img src="images/%s" />' % v
  elif not v or v == '':
    return '<span class="'+ c +'"></span>'
  else:
    return sub('([^,]+)', '<span class="'+ c +'">\\1</span>', str(v))


def set_format(v):
  if isinstance(v, int):
    return v
  elif isinstance(v, Decimal):  # or isinstance(v, float)
    return thous(round_half_up(v, 2))
  elif isinstance(v, str):
    return v.replace('\\\\', '<br />')
  else:
    return str(v) if v else '-'


def agg_fns(k, v):
  return thous(round_half_up({"count": lambda x: len(x),
                              "avg": lambda x: np.mean(x),
                              "sum": lambda x: np.sum(x),
                              "min": lambda x: np.min(x),
                              "max": lambda x: np.max(x),
                              "std": lambda x: np.std(x)}[k](v), 2))


############################# CONTENT #############################
'''
defs = [{"type":"table", "defs":[{"head":"col1", "foot":"sum", "class":"xxx", "value":"r[1]/100"},
                                 {"head":"col2", ...}]},
        {"type":"pivot", "defs":{"data":{}, "index":[], "values":[], "columns":[], "aggfunc":[]}},
        {"type":"graph", "defs":{"data":{}, "graph":{}}},
        {"type":"resumen", "defs":[]}]
'''
def set_table(defs, rows):
  body = []
  foot = [[], []]
  values = defaultdict(list)
  for row in rows:
    tmp = []
    for i, v in enumerate(defs):
      tmp.append(set_value(v['value'], row) if 'value' in v else row[i])
      if 'foot' in v and tmp[i]:
        values[i].append(tmp[i] if isinstance(tmp[i], str) else Decimal(tmp[i]))
      tmp[i] = set_class(v['class'], tmp[i]) if 'class' in v else set_format(tmp[i])
    body.append(tmp)
  if bool(values):
    for i, v in enumerate(defs):
      foot[0].append(agg_fns(v['foot'], values[i]) if 'foot' in v and i in values else '')
      foot[1].append(v['foot'] if 'foot' in v else '')
  return {"head": [d['head'] for d in defs], "body": body, "foot": foot}


def set_crosstable(defs, rows):
  groups = []
  values = {}
  body = []
  sub_groups = defs['sub_groups'] if 'sub_groups' in defs else 1
  for row in rows:
    if row[1] not in groups: groups.append(row[1])
    if row[0] not in values: values[row[0]] = {}
    values[row[0]][row[1]] = row[2]
  groups.sort(key=str.lower)
  # if sub_groups > 1:
  #   totals = [0] * len(groups) * sub_groups
  if len(groups) > 0:
    for k in sorted(values.keys(), key=str.lower):
      r = [values[k][g] if g in values[k] else '_' * sub_groups for g in groups]
      if sub_groups > 1:
        tmp = [x.split('_') for x in r]
        r = sum(map(list, zip(*tmp)), [])  # reduce(lambda x,y: x+y, map(list, zip(*tmp)))
        # for i in range(len(r)): totals[i] = totals[i] + float(r[i] or 0)
      body.append((k.split('_') if 'cols' in defs else [k]) + r)
    # if sub_groups > 1:
    #   totals = [thous(round(t, 2)) for t in totals]
    #   body.append(([''] * len(defs['cols']) if 'cols' in defs else ['']) + totals)
  return {"head": (defs['cols'] if 'cols' in defs else ['']) + groups * sub_groups if len(groups) > 0 else [],
          "body": body, "foot": [[], []]}


def set_crosstable_sum(defs, rows):
  groups = []
  values = {}
  body = []
  for row in rows:
    if row[1] not in groups: groups.append(row[1])
    if row[0] not in values: values[row[0]] = {}
    values[row[0]][row[1]] = row[2]
  groups.sort(key=str.lower)
  if len(groups) > 1:
    groups.append('total')
  totals = [0] * len(groups)
  if len(groups) > 0:
    for k in sorted(values.keys(), key=str.lower):
      r = []
      for i in range(len(groups)):
        if groups[i] == 'total':
          v = float(np.sum(r))
        else:
          v = values[k][groups[i]] if groups[i] in values[k] else 0
        totals[i] = totals[i] + v
        r.append(v)
      body.append((k.split('_') if 'cols' in defs else [k]) + r)
    body.append(([''] * len(defs['cols']) if 'cols' in defs else ['']) + totals)
  return {"head": (defs['cols'] if 'cols' in defs else ['']) + groups if len(groups) > 0 else [],
          "body": body, "foot": [[], []]}


def set_crosstable_granja(defs, rows):
  body = []
  groups = []
  values = {}
  values_order = []
  value_last = ''
  for row in rows:
    if row[1] and row[1] not in groups: groups.append(row[1])
    if row[0] not in values:
      values[row[0]] = {}
      values_order.append(row[0])
    values[row[0]][row[1] or 'total'] = row[2]
  if len(groups) > 1:
    groups.append('total')
  if len(groups) > 0:
    for k in values_order:
      first = [sub('_.+', '', k) if sub('_.+', '', k) != value_last else '',
              sub('.+_', '', k) if '_' in k else '']
      body.append(first + [values[k][g] if g in values[k] else '' for g in groups])
      value_last = sub('_.+', '', k)
  return {"head": ['', ''] + groups if len(groups) > 0 else [],
          "body": body, "foot": [[], []]}


def set_crosstable_conta(defs, rows):
  s = SimpleEval()
  s.names = {"c":[]}
  names = {}
  values = {}
  groups = []
  cols = []
  body = []
  for row in rows:
    key = int(sub('_.+$', '', row[0]))
    if key not in names:
      names[key] = sub('^\d+_', '', row[0])
      values[key] = {}
    values[key][row[1]] = row[2]
    if row[1] not in groups:
      groups.append(row[1])
      cols.append([])
  if len(groups) > 1:
    groups.append('total')
    cols.append([])
  if len(groups) > 0:
    for k in defs:
      if isinstance(k, int):
        r = []
        for i in range(len(groups)):
          if groups[i] == 'total':
            v = np.sum(r)
          else:
            v = values[k][groups[i]] if groups[i] in values[k] else 0
          r.append(v)
          cols[i].append(round_half_up(v, 2))
    j = 0
    for k in defs:
      if isinstance(k, int):
        r = [thous(cols[i][j]) for i in range(len(groups))]
        j = j + 1
        body.append([names[k]] + r)
      else:
        r = []
        for i in range(len(groups)):
          s.names['c'] = cols[i]
          r.append(thous(round_half_up(s.eval(k[1]), 2)) if k[1] != '' else '')
        body.append([k[0]] + r)
  return {"head": [''] + groups if len(groups) > 0 else [], "body": body, "foot": [[], []]}


def set_pivot(defs, rows):
  groups = {}
  key = defs['index']
  keys = defs['values']
  aggs = defs['aggfunc']
  data = defs["data"]
  for row in rows:
    values = {k: set_value(data[k], row) for k in data}
    if values[key] not in groups: groups[str(values[key])] = {k:[] for k in keys}
    for k in keys:
      if values[k]: groups[str(values[key])][k].append(values[k])
  table = '<tr><th>%s</th></tr>' % '</th><th>'.join([''] + keys)
  for group in sorted(groups):
    row = [group] + [agg_fns(aggs[k], groups[group][k]) for k in keys]
    table += '<tr><td>%s</td></tr>' % '</td><td>'.join(map(str, row))
  width = (1 + len(defs["values"])) * 100
  return {"width": width, "table": '<table class="table table-striped">%s</table>' % table}


def set_pandas_pivot(defs, rows):
  values = {}
  for row in rows:
    for k in defs["data"]:
      val = set_value(defs["data"][k], row)
      if isinstance(val, float) or isinstance(val, Decimal):
        val = round_half_up(val, 2)
      if k not in values:
        values[k] = []
      values[k].append(val)
  p = pivot_table(DataFrame(values, dtype=float), fill_value=0,
                  index=defs["index"], values=defs["values"], aggfunc=defs["aggfunc"],
                  columns=defs["columns"] if 'columns' in defs else [])
  width = (len(defs["index"]) + len(defs["values"])) * 100
  return {"width": width, "table": p.to_html().replace('<table border="1" class="dataframe">',
                                                       '<table class="table table-striped">')}


def set_graph(defs, rows):
  values = []
  for row in rows:
    tmp = {}
    for k in defs["data"]:
      val = set_value(defs["data"][k], row)
      if isinstance(val, date):
        val = str(val)
      if isinstance(val, float):
        val = Decimal(val)
      if isinstance(val, Decimal):
        val = round_half_up(val, 2)
      tmp[k] = val
    values.append(tmp)
  if 'vega-lite' in defs['graph']['$schema']:
    defs['graph']['data'] = {"values":values}
    p1 = Popen([AppConfig.NODE_PATH, AppConfig.NODE_MODULES_PATH+'vega-lite/bin/vl2vg'],
               stdin=PIPE, stdout=PIPE, stderr=PIPE)
    vg, err = p1.communicate(input=dumps(defs['graph'], cls=JsonEncoder).encode())
    if err:
      raise Exception('vl graph error!')
  else:
    defs['graph']['data'][0]["values"] = values
    vg = dumps(defs['graph'], cls=JsonEncoder).encode()
  p2 = Popen([AppConfig.NODE_PATH, AppConfig.NODE_MODULES_PATH+'vega-cli/bin/vg2svg'],
             stdin=PIPE, stdout=PIPE, stderr=PIPE)
  svg, err = p2.communicate(input=vg)
  if err:
    raise Exception('vg graph error!', err)
  if 'pdf' in defs:
    graph = 'data:image/png;base64,%s' % str(b64encode(svg2png(svg)))[2:-1]
  else:
    graph = 'data:image/svg+xml;base64,%s' % str(b64encode(svg))[2:-1]
  width = defs['graph']['width'] if 'width' in defs['graph'] else 200
  return {"graph": graph, "width": width}


def set_graphviz(defs, rows):
  dot = Digraph()
  dot.attr(rankdir='LR')
  for row in rows:
    tmp = {k: set_value(defs["data"][k], row) for k in defs["data"]}
    if tmp['parent']:
      dot.edge(str(tmp['id']), str(tmp['parent']), tmp['label'])
  graph = graph_from_dot_data(dot.source)
  svg = graph[0].create_svg()
  if 'pdf' in defs:
    graph = 'data:image/png;base64,%s' % str(b64encode(svg2png(svg)))[2:-1]
  else:
    graph = 'data:image/svg+xml;base64,%s' % str(b64encode(svg))[2:-1]
  width = defs['width'] if 'width' in defs else 200
  return {"graph": graph, "width": width}


def set_resumen(defs, rows):
  body = []
  for row in defs:
    tmp = []
    for i in range(len(row)):
      tmp.append(set_value(row[i], rows) if row[i] != '' else '')
      if isinstance(tmp[i], int):
        tmp[i] = thous(tmp[i])
      elif isinstance(tmp[i], Decimal):  # or isinstance(tmp[i], float)
        tmp[i] = thous(round_half_up(tmp[i], 2))
    body.append(tmp)
  return body


def set_content(defs, rows, pdf=False):
  if pdf:
    for d in defs:
      if 'type' in d and d['type'] == 'graph': d['defs']['pdf'] = True
  return [{"type":d['type'],
           "data":{"table": set_table,
                   "pivot": set_pivot,
                   "pandas_pivot": set_pandas_pivot,
                   "graph": set_graph,
                   "graphviz": set_graphviz,
                   "crosstable": set_crosstable,
                   "crosstable_sum": set_crosstable_sum,
                   "crosstable_granja": set_crosstable_granja,
                   "crosstable_conta": set_crosstable_conta,
                   "resumen": set_resumen}[d['type']](d['defs'], rows)} for d in defs]


############################# PDF #############################
def set_pdf(header, content):
  html = '<html><head>' + \
         '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />' + \
         '<style>/*.table {font-size:125%;}*/</style>' + \
         '<style>.table {border-collapse: collapse; border-style: hidden;}</style>' + \
         '<style>.table th {border-top:1px solid #ccc; border-bottom:1px solid #ccc;}</style>' + \
         '<style>.table th, .table td {text-align:right; vertical-align:top;}</style>' + \
         '<style>.table th:first-child, .table td:first-child {text-align:left}</style>' + \
         '</head><body>' + \
         header + \
         content + \
         "Fecha (hora): %s" % datetime.now(AppConfig.TIMEZONE).strftime("%Y-%m-%d (%H:%M:%S)") + \
         '</body></html>'
  # resultFile = open('/home/wagner/doc.pdf', 'w+b')
  # pisaStatus = pisa.CreatePDF(html, dest=resultFile)
  # resultFile.close()
  pdf = BytesIO()
  status = pisa.CreatePDF(StringIO(html), dest=pdf)
  if status.err:
    raise Exception('Error en impresion pdf!')
  return pdf.getvalue()


def set_header_html(header, logo='vaca.png'):
  return '<table><tr><td style="width:550px; vertical-align:middle;">' + \
         '<h1 style="font-size:300%">' + header + '</h1>' + \
         '</td><td style="width:150px;">' + \
         '<img src="app/static/logos/' + logo + '" />' + \
         '</td></tr></table>'


def set_report_html(data, params):
  html = f'<span style="font-size:200%;"><h2 style="display:inline;">{data["title"]}</h2>' + \
         f'&nbsp;{params}</span>'
  width = 0
  for c in data['content']:
    if 'title' in c: html += '<h3>%s</h3>' % c['title']
    d = c['data']
    if c['type'] in ('table', 'crosstable', 'crosstable_sum', 'crosstable_granja', 'crosstable_conta'):
      html += '</tr></table>' if width > 0 else ''
      html += '<table class="table table-striped">'
      html += '<thead><tr><th>%s</th></tr></thead>' % '</th><th>'.join(d['head'])
      html += '<tbody>'
      html += ''.join(['<tr><td>%s</td></tr>' % '</td><td>'.join(map(str, b)) for b in d['body']])
      html += '</tbody>'
      if len(d['foot']) > 0:
        html += '<tfoot><tr><th>%s</th></tr></tfoot>' % '</th><th>'.join(d['foot'][0])
      html += '</table>'
      width = 0
    elif c['type'] in ('pivot', 'pandas_pivot'):
      html += '<table><tr>' if width == 0 else ''
      html += '<td>%s</td>' % d['table']
      html += '</tr></table>' if (width + d['width']) > 400 else ''
      width = 0 if (width + d['width']) > 400 else width + d['width']
    elif c['type'] in ('graph', 'graphviz'):
      html += '<table><tr>' if width == 0 else ''
      html += '<td><img src="%s" /></td>' % d['graph']
      html += '</tr></table>' if (width + d['width']) > 400 else ''
      width = 0 if (width + d['width']) > 400 else width + d['width']
  html += '</table>'
  return html
