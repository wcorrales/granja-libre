from quart import Blueprint

breeding = Blueprint('breeding', __name__, template_folder='')

from . import views
from .views import get_breeding_menu
