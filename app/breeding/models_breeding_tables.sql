/*** ACTIVITIES ***/

-- psql granjalibre -v activity='1' -f db_activities.sql
-- \set activity '1'


SET search_path TO activity_:activity;


/**********************************************/
/***************** TABLES *********************/
/**********************************************/
-- ****************
-- intervals
-- ****************
CREATE TABLE variables (
  id INTEGER PRIMARY KEY,
  var VARCHAR(15) NOT NULL,
  val NUMERIC(4,1) NOT NULL,
  CHECK (val >= 0)
);

-- ****************
-- deaths
-- ****************
CREATE TABLE deaths (
  id SERIAL PRIMARY KEY NOT NULL,
  death VARCHAR(30) UNIQUE NOT NULL,
  type CHAR(5) NOT NULL,
  active SMALLINT NOT NULL,
  CHECK (TRIM(death) <> '' AND death !~* '[^a-z0-9 :.,/()#-]+'),
  CHECK (type IN ('adult', 'young')),
  CHECK (active IN (0, 1))
);


-- ****************
-- races
-- ****************
CREATE TABLE races (
  id SERIAL PRIMARY KEY NOT NULL,
  race VARCHAR(30) UNIQUE NOT NULL,
  active SMALLINT NOT NULL,
  CHECK (TRIM(race) <> '' AND race !~* '[^a-z0-9 :.,/()#-]+'),
  CHECK (active IN (0, 1))
);


-- ****************
-- animals
-- ****************
CREATE TABLE animals (
  id INTEGER PRIMARY KEY NOT NULL,
  race_id INTEGER NOT NULL,
  animal VARCHAR(30) UNIQUE NOT NULL,
  pedigree VARCHAR(30) UNIQUE NULL,
  litter VARCHAR(10) NOT NULL,
  birth_ts INTEGER NOT NULL,
  create_ts INTEGER NOT NULL,
  CHECK (TRIM(animal) <> '' AND animal !~* '[^a-z0-9 :.,/()#-]+'),
  CHECK (TRIM(pedigree) <> '' AND pedigree !~* '[^a-z0-9 :.,/()#-]+'),
  CHECK (litter ~* '^\d+[a-z]$'),
  FOREIGN KEY(id) REFERENCES public.animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id)
);


-- ****************
-- animals_eartags
-- ****************
CREATE TABLE animals_eartags (
  id INTEGER PRIMARY KEY NOT NULL,
  eartag VARCHAR(20) UNIQUE NOT NULL,
  CHECK (TRIM(eartag) <> '' AND eartag !~* '[^a-z0-9 :.,/()#-]+'),
  FOREIGN KEY(id) REFERENCES animals(id)
);


/******************* EVENTS *******************/
-- ****************
-- events
-- ****************
CREATE TABLE events (
  id SERIAL PRIMARY KEY NOT NULL,
  animal_id INTEGER NOT NULL,
  race_id INTEGER NOT NULL,
  worker_id INTEGER NOT NULL,
  ts INTEGER NOT NULL,
  parity INTEGER NOT NULL,
  CHECK (parity >= 0),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
);

CREATE UNIQUE INDEX events_idx ON events (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_entry_female
-- ****************
CREATE TABLE ev_entry_female (
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_entry_female_idx ON ev_entry_female (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_entry_male
-- ****************
CREATE TABLE ev_entry_male (
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_entry_male_idx ON ev_entry_male (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_entry_semen
-- ****************
CREATE TABLE ev_entry_semen (
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_entry_semen_idx ON ev_entry_semen (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_sale_semen
-- ****************
CREATE TABLE ev_sale_semen (
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_sale_semen_idx ON ev_sale_semen (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_sale
-- ****************
CREATE TABLE ev_sale (
  death_id INTEGER NOT NULL,
  -- destination INTEGER NULL,
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id),
  FOREIGN KEY(death_id) REFERENCES deaths(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_sale_idx ON ev_sale (
  animal_id ASC,
  id ASC
);


/***************** reproductive events *********************/
-- ****************
-- ev_heat
-- ****************
CREATE TABLE ev_heat (
  lordosis SMALLINT NOT NULL,
  CHECK (lordosis IN (1, 2, 3, 4, 5)),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_heat_idx ON ev_heat (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_service
-- ****************
CREATE TABLE ev_service (
  male_id INTEGER NOT NULL,
  matings SMALLINT NOT NULL,
  lordosis SMALLINT NOT NULL,
  quality SMALLINT NOT NULL,
  repeat BOOLEAN NOT NULL,
  CHECK (matings > 0 AND matings < 10),
  CHECK (lordosis IN (1, 2, 3, 4, 5)),
  CHECK (quality IN (1, 2, 3, 4, 5)),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id),
  FOREIGN KEY(male_id) REFERENCES animals(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_service_idx ON ev_service (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_preg_checks+
-- ****************
CREATE TABLE ev_check_pos (
  test CHAR(2) NOT NULL,
  note VARCHAR(100) DEFAULT NULL,
  CHECK (test IN ('us', 'px', 'vs')),
  CHECK (TRIM(note) <> '' AND note !~* '[^a-z0-9 :.,/()#-]+'),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_check_pos_idx ON ev_check_pos (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_preg_checks-
-- ****************
CREATE TABLE ev_check_neg (
  test CHAR(2) NOT NULL,
  note VARCHAR(100) DEFAULT NULL,
  CHECK (test IN ('us', 'px', 'vs', 'ht')),
  CHECK (TRIM(note) <> '' AND note !~* '[^a-z0-9 :.,/()#-]+'),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_check_neg_idx ON ev_check_neg (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_abortion
-- ****************
CREATE TABLE ev_abortion (
  inducted BOOLEAN NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_abortion_idx ON ev_abortion (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_farrow
-- ****************
CREATE TABLE ev_farrow (
  litter VARCHAR(10) UNIQUE NOT NULL,
  males INTEGER NOT NULL,
  females INTEGER NOT NULL,
  weight NUMERIC(5,2) NOT NULL,
  deaths INTEGER NOT NULL,
  mummies INTEGER NOT NULL,
  hernias INTEGER NOT NULL,
  cryptorchids INTEGER NOT NULL,
  dystocia BOOLEAN NOT NULL,
  retention BOOLEAN NOT NULL,
  inducted BOOLEAN NOT NULL,
  asisted BOOLEAN NOT NULL,
  CHECK (litter ~* '^\d+[a-z]$'),
  CHECK (males >= 0),
  CHECK (females >= 0),
  CHECK (males > 0 OR females > 0),
  CHECK (weight > 0),
  CHECK (deaths >= 0),
  CHECK (mummies >= 0),
  CHECK (hernias >= 0),
  CHECK (cryptorchids >= 0),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_farrow_idx ON ev_farrow (
  animal_id ASC,
  id ASC
);

-- for genelogy_function
CREATE UNIQUE INDEX ev_farrow_litterx ON ev_farrow (
  litter ASC
);


-- ****************
-- ev_death
-- ****************
CREATE TABLE ev_death (
  death_id INTEGER NOT NULL,
  animals INTEGER NOT NULL,
  CHECK (animals > 0),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id),
  FOREIGN KEY(death_id) REFERENCES deaths(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_death_idx ON ev_death (
  animal_id ASC,
  id ASC
);

-- for litters_resumen
--CREATE UNIQUE INDEX ev_death_parityx ON ev_death (
--  animal_id ASC,
--  parity ASC
--);


-- ****************
-- ev_foster
-- ****************
CREATE TABLE ev_foster (
  animals INTEGER NOT NULL,
  weight NUMERIC(5,2) NOT NULL,
  CHECK (animals > 0),
  CHECK (weight >= 0),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_foster_idx ON ev_foster (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_adoption
-- ****************
CREATE TABLE ev_adoption (
  animals INTEGER NOT NULL,
  weight NUMERIC(5,2) NOT NULL,
  CHECK (animals > 0),
  CHECK (weight >= 0),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_adoption_idx ON ev_adoption (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_wean
-- ****************
CREATE TABLE ev_wean (
  animals INTEGER NOT NULL,
  weight NUMERIC(5,2) NOT NULL,
  CHECK (animals >= 0),
  CHECK (weight >= 0),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_wean_idx ON ev_wean (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_partial_wean
-- ****************
CREATE TABLE ev_partial_wean (
  animals INTEGER NOT NULL,
  weight NUMERIC(5,2) NOT NULL,
  CHECK (animals > 0),
  CHECK (weight >= 0),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_partial_wean_idx ON ev_partial_wean (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_semen
-- ****************
CREATE TABLE ev_semen (
  volumen NUMERIC(5,2) NOT NULL,
  concentration NUMERIC(6,2) NOT NULL,
  motility SMALLINT NOT NULL,
  -- morfology INTEGER NOT NULL,
  -- grumos SMALLINT NOT NULL,
  dosis INTEGER NOT NULL,
  CHECK (volumen > 0),
  CHECK (concentration >= 0),
  CHECK (motility IN (0, 1, 2, 3, 4, 5)),
  CHECK (dosis >= 0),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_semen_idx ON ev_semen (
  animal_id ASC,
  id ASC
);


/****************************************************/
-- ****************
-- ev_ubication
-- ****************
CREATE TABLE ev_ubication (
  ubication VARCHAR(15) NOT NULL,
  CHECK (TRIM(ubication) <> '' AND ubication !~* '[^a-z0-9 :.,/()#-]+'),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_ubication_idx ON ev_ubication (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_feed
-- ****************
CREATE TABLE ev_feed (
  weight NUMERIC(4,2) NOT NULL,
  CHECK (weight >= 0),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_feed_idx ON ev_feed (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_condition
-- ****************
CREATE TABLE ev_condition (
  condition NUMERIC(2,1) NOT NULL,
  weight NUMERIC(5,2) NOT NULL,
  backfat NUMERIC(5,2) NULL,
  CHECK (condition IN(1, 1.5, 2, 2.5, 3, 3.5, 4, 4.5, 5)),
  CHECK (weight >= 0),
  CHECK (backfat >= 0),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_condition_idx ON ev_condition (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_milk
-- ****************
CREATE TABLE ev_milk (
  weight NUMERIC(4,2) NOT NULL,
  quality SMALLINT NOT NULL,
  CHECK (weight >= 0),  -- 0 = end
  CHECK (quality IN(1, 2, 3, 4, 5)),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_milk_idx ON ev_milk (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_dry
-- ****************
CREATE TABLE ev_dry (
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_dry_idx ON ev_dry (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_temperature
-- ****************
CREATE TABLE ev_temperature (
  temperature NUMERIC(4,2) NOT NULL,
  CHECK (temperature > 0 AND temperature < 50),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_temperature_idx ON ev_temperature (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_treatment
-- ****************
CREATE TABLE ev_treatment (
  treatment VARCHAR(10) NOT NULL,
  dose VARCHAR(15) NOT NULL,
  frequency INTEGER NOT NULL,
  days INTEGER NOT NULL,
  route CHAR(2) NOT NULL,
  note VARCHAR(100) NOT NULL,
  CHECK (TRIM(treatment) <> '' AND treatment !~* '[^a-z0-9 :.,/()#-]+'),
  CHECK (dose ~* '[0-9]?(\.)?[0-9]ui/kg|ml/kg|mg/kg|g/kg'),
  CHECK (frequency IN (6, 8, 12, 24, 48, 72)),
  CHECK (days > 0),
  CHECK (route IN ('im', 'iv', 'ip', 'sc', 'vd', 'vr', 'vt')),
  CHECK (TRIM(note) <> '' AND note !~* '[^a-z0-9 :.,/()#-]+'),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_treatment_idx ON ev_treatment (
  animal_id ASC,
  id ASC
);


-- ****************
-- ev_note
-- ****************
CREATE TABLE ev_note (
  note VARCHAR(100) NOT NULL,
  CHECK (TRIM(note) <> '' AND note !~* '[^a-z0-9 :.,/()#-]+'),
  PRIMARY KEY (id),
  FOREIGN KEY(animal_id) REFERENCES animals(id),
  FOREIGN KEY(race_id) REFERENCES races(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
) INHERITS (events);

CREATE UNIQUE INDEX ev_note_idx ON ev_note (
  animal_id ASC,
  id ASC
);


/******************************************************************/


CREATE TABLE events_querys (
  id SERIAL PRIMARY KEY NOT NULL,
  lft INTEGER NOT NULL,
  rgt INTEGER NOT NULL,
  title VARCHAR(100) UNIQUE NOT NULL,
  tbl TEXT DEFAULT NULL,
  var TEXT DEFAULT NULL,
  rnd INTEGER DEFAULT NULL,
  agg BOOLEAN DEFAULT NULL,
  CHECK (lft > 0),
  CHECK (rgt > 0),
  CHECK (TRIM(title) <> '' AND title !~* '[^a-z0-9 @#&()"'';:,.%<>=/*+-]+'),
  CHECK (TRIM(tbl) <> '' AND tbl !~* '[^a-z0-9 _\]\[{}()"'';:,.&|?!~$%<>=/*+-\\^]+'),
  CHECK (TRIM(var) <> '' AND var !~* '[^a-z0-9 _\]\[{}()"'';:,.&|?!~$%<>=/*+-\\^]+'),
  CHECK (rnd >= 0 AND rnd <=4)
);
