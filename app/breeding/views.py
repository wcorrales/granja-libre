from json import dumps
from quart import make_response, request, render_template, session, url_for
from quart_auth import current_user, login_required
from re import sub

from app import db
from . import breeding
from ..notebooks.views import get_notebooks_menu
from ..pgvega.views import get_reports_menu
from ..utils.admin import check_user
from ..utils.utils import sql_ts, JsonEncoder  # escape, unescape,


def get_breeding_menu(u_admin, u_access):
  admin = [{"url": "breeding.breeding_parameters", "name": "Parametros"}] if u_admin else []
  forms = filter(lambda x: u_admin or url_for(x['url']) in u_access,
                 [{"url": "breeding.breeding_handler", "name": "Eventos"}]) 
  reports = filter(lambda x: url_for('pgvega.get_report', code=x['code']) in u_access,
                   get_reports_menu('^breeding_')) 
  notebooks = filter(lambda x: url_for('notebooks.get_notebook', notebook_id=x['id']) in u_access,
                     get_notebooks_menu('^breeding_'))
  return {"code": "reproduction", "title": "Reproduccion",
          "forms": list(forms)+admin, "reports": list(reports), "notebooks": list(notebooks)}


@breeding.route('/breeding/breeding_handler')
@login_required
@check_user
@db.wrapper
async def breeding_handler():
  return await render_template('templates/breeding_history.html', title='Reproduccion',
                               animal=session['animal'] if 'animal' in session else '',
                               races=dumps(db.all("SELECT * FROM races ORDER BY race;"), cls=JsonEncoder),
                               deaths=dumps(db.all("SELECT * FROM deaths ORDER BY death;"), cls=JsonEncoder))


###################### ANIMALS #######################
@breeding.route('/breeding/get_activity', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_activity():
  rq = await request.form
  return dumps(db.all("""
    SELECT CONCAT_WS(' -> ', farm, id) AS name
    FROM (SELECT id, public.farm_name(a.id) AS farm
          FROM public.activities a
          WHERE spiece=(SELECT spiece FROM public.activities
                        WHERE id=REGEXP_REPLACE(CURRENT_SCHEMA(), '\D+','')::INTEGER)) t
    WHERE farm ILIKE %s ORDER BY farm LIMIT 5;""",
    ('%%%s%%' % rq['val'], ), as_dict=True), cls=JsonEncoder)


@breeding.route('/breeding/get_animal', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_animal():
  rq = await request.form
  return dumps(db.all("""
    SELECT CASE WHEN death > 0 THEN '*' ELSE '' END ||
           CONCAT_WS(' -> ', animal, ubication) AS name,
           (SELECT eartag FROM animals_eartags WHERE id=t.id) AS eartag
    FROM (SELECT id, animal,
                 (SELECT ubication FROM ev_ubication WHERE animal_id=a.id
                  ORDER BY id DESC LIMIT 1) AS ubication,
                 COALESCE((SELECT ts FROM ev_sale WHERE animal_id=a.id),
                          (SELECT ts FROM ev_sale_semen WHERE animal_id=a.id)) AS death
          FROM animals a) t
    WHERE animal ILIKE %s OR ubication ILIKE %s
    ORDER BY death DESC LIMIT 10;""",
    ('%%%s%%' % rq['val'], '%%%s%%' % rq['val']), as_dict=True), cls=JsonEncoder)


@breeding.route('/breeding/get_animal_male', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_animal_male():
  rq = await request.form
  return dumps(db.all("""
    SELECT animal AS name
    FROM animals a
    WHERE EXISTS(SELECT id FROM ev_entry_male WHERE animal_id=a.id) AND
          NOT EXISTS(SELECT id FROM ev_sale WHERE animal_id=a.id) AND
          animal ILIKE %s OR
          EXISTS(SELECT id FROM ev_entry_semen WHERE animal_id=a.id) AND
          NOT EXISTS(SELECT id FROM ev_sale_semen WHERE animal_id=a.id) AND
          animal ILIKE %s
    ORDER BY animal LIMIT 5;""",
    ('%%%s%%' % rq['val'], '%%%s%%' % rq['val']), as_dict=True), cls=JsonEncoder)


@breeding.route('/breeding/get_animal_adoptive', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_animal_adoptive():
  rq = await request.form
  return dumps(db.all("""
    SELECT a.animal AS name
    FROM animals a JOIN ev_farrow f ON a.id=f.animal_id
    WHERE NOT EXISTS(SELECT id FROM ev_wean WHERE animal_id=a.id AND id>f.id) AND
          a.animal ILIKE %s ORDER BY a.animal LIMIT 5;""",
    ('%%%s%%' % rq['val'], ), as_dict=True), cls=JsonEncoder)


@breeding.route('/breeding/get_animal_litter', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_animal_litter():
  rq = await request.form
  return dumps(db.one("SELECT * FROM litter_info_function(%s, %s);",
                      (sub(r'.+(?=\d+$)', r'', rq["farm_search"]), rq['litter'])), cls=JsonEncoder)


@breeding.route('/breeding/get_animal_olds', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_animal_olds():
  return dumps(db.all("""
    SELECT t.id, i.animal, public.farm_name(i.prev_a_id)
    FROM (SELECT DISTINCT(animal_id) AS id FROM public.animals_activities a
          WHERE NOT EXISTS(SELECT id FROM animals WHERE id=a.animal_id)) t,
         public.animal_information(t.id) i
    WHERE i.last_a_id=REGEXP_REPLACE(CURRENT_SCHEMA(),'\D+','')::INTEGER;"""), cls=JsonEncoder)


@breeding.route('/breeding/set_animal', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_animal():
  rq = await request.form
  u_worker = await current_user.worker
  if rq['action'] == 'insert_old':
    db.execute("SELECT animal_old_insert_function(%s, %s);", (rq['id'], u_worker))
  else:
    db.execute("""
      SELECT animal_new_insert_function(%s,%s,%s,%s,%s,%s,%s,%s,%s);""",
      (sql_ts(rq['a_entry']), rq['a_name'], rq['a_pedigree'] or None,
       sub(r'.+(?=\d+$)', r'', rq["farm_search"]), rq['a_litter'],
       sql_ts(rq['a_birth']), rq['a_race'], rq['a_sex'], u_worker))
  db.commit()
  return '[]'


@breeding.route('/breeding/set_animal_eartag', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_animal_eartag():
  rq = await request.form
  if rq['animal'] == '':
    raise Exception("error: animal not selected!")
  if rq['a_eartag'] != '':
    db.execute("INSERT INTO animals_eartags VALUES((SELECT id FROM animals WHERE animal=%s), %s);",
               (rq['animal'], rq['a_eartag']))
  else:
    db.execute("DELETE FROM animals_eartags WHERE id=(SELECT id FROM animals WHERE animal=%s);",
               (rq['animal'], ))
  db.commit()
  return '[]'


###################### SEMEN #######################
@breeding.route('/breeding/get_semen', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_semen():
  rq = await request.form
  if rq['action'] == 'animals':
    return dumps(db.all("""
      SELECT t.id, i.animal, public.farm_name(i.last_a_id)
      FROM (SELECT id FROM public.animals_semen a
            WHERE NOT EXISTS(SELECT id FROM animals WHERE id=a.id)) t,
           public.animal_information(t.id) i
      WHERE (SELECT spiece FROM public.activities WHERE id=i.last_a_id) =
            (SELECT spiece FROM public.activities
             WHERE id=REGEXP_REPLACE(CURRENT_SCHEMA(), '\D+', '')::INTEGER);"""), cls=JsonEncoder)
  else:
    if rq['animal'] == '':
      raise Exception("error: animal not selected!")
    return dumps(db.all("""
      SELECT CONCAT_WS(' -> ', public.farm_name(a.activity_id), a.activity_id)
      FROM public.animals_semen_activities a
      WHERE animal_id=(SELECT id FROM animals WHERE animal=%s);""", (rq['animal'], )), cls=JsonEncoder)


@breeding.route('/breeding/set_semen', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_semen():
  rq = await request.form
  u_worker = await current_user.worker
  if rq['action'] == 'insert_semen':
    db.execute("SELECT animal_semen_insert_function(%s, %s, %s);",
               (rq['id'], sql_ts(rq['ts'], u_worker)))
  else:
    db.execute("""SELECT public.toggle_animals_semen_function(
                    (SELECT id FROM animals WHERE animal=%s));""", (rq['animal'], ))
  db.commit()
  return '[]'


####################### EVENTS ########################
def getConstantHistory(animal):
  return db.all("""
    SELECT id, tableoid::regclass::name, ts, parity,
           ev_cols_function(tableoid::regclass::name, id)
    FROM events
    WHERE animal_id=(SELECT id FROM animals WHERE animal=%s) AND
          tableoid IN ('ev_feed'::regclass::oid,
                       'ev_condition'::regclass::oid,
                       'ev_milk'::regclass::oid,
                       'ev_temperature'::regclass::oid)
    ORDER BY ts, id;""", (animal, ))


def getVariableHistory(animal):
  return db.all("""
    SELECT id, tableoid::regclass::name, ts, parity,
           ev_cols_function(tableoid::regclass::name, id)
    FROM events
    WHERE animal_id=(SELECT id FROM animals WHERE animal=%s) AND
          tableoid NOT IN ('ev_feed'::regclass::oid,
                           'ev_condition'::regclass::oid,
                           'ev_milk'::regclass::oid,
                           'ev_temperature'::regclass::oid)
    ORDER BY ts, id;""", (animal, ))


def getRules(animal):
  return db.one("""
    SELECT animal_rules_function(
           (SELECT id FROM animals WHERE animal=%s));""", (animal, ))


@breeding.route('/breeding/get_animal_history', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_animal_history():
  rq = await request.form
  session['animal'] = rq['animal']
  return dumps({"ev_constant": getConstantHistory(rq['animal']) if rq['all'] == 'true' else [],
                "ev_variable": getVariableHistory(rq['animal']),
                "rules": getRules(rq['animal'])[0][1]}, cls=JsonEncoder)


@breeding.route('/breeding/set_animal_event', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_animal_event():
  rq = await request.form
  u_id = await current_user.id
  try:
    if rq['action'] == 'ev_sale_semen':
      ids = db.one("""
        INSERT INTO ev_sale_semen
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"])))
    elif rq['action'] == 'ev_sale':
      ids = db.one("""
        INSERT INTO ev_sale
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, (SELECT id FROM deaths WHERE death=%s))
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["death"]))
      db.execute("INSERT INTO public.animals_activities VALUES(DEFAULT, %s, %s);",
                    (ids[1], sub(r'.+(?=\d+$)', r'', rq["farm_search"]) or None))
    elif rq['action'] == 'ev_heat':
      ids = db.one("""
        INSERT INTO ev_heat
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["lordosis"]))
    elif rq['action'] == 'ev_service':
      ids = db.one("""
        INSERT INTO ev_service
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, (SELECT id FROM animals WHERE animal=%s), %s, %s,%s, FALSE)
        RETURNING id, animal_id, parity;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]),
         rq["male"], rq["matings"], rq["lordosis"], rq["quality"]))
      db.execute("""
        UPDATE ev_service
        SET repeat=EXISTS(SELECT id FROM ev_service WHERE id<%s AND animal_id=%s AND parity=%s)
        WHERE id=%s""", (ids[0], ids[1], ids[2], ids[0]))
    elif rq['action'] == 'ev_check_pos':
      ids = db.one("""
        INSERT INTO ev_check_pos
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["test"],
         None if rq["note"] == '' else rq["note"]))
    elif rq['action'] == 'ev_check_neg':
      ids = db.one("""
        INSERT INTO ev_check_neg
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["test"],
         None if rq["note"] == '' else rq["note"]))
    elif rq['action'] == 'ev_abortion':
      ids = db.one("""
        INSERT INTO ev_abortion
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), "inducted" in rq))
    elif rq['action'] == 'ev_farrow':
      ids = db.one("""
        INSERT INTO ev_farrow
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["litter"], rq["males"],
         rq["females"], rq["weight"], rq["deaths"], rq["mummies"], rq["hernias"],rq["cryptorchids"],
         "dystocia" in rq, "retention" in rq, "inducted" in rq, "asisted" in rq))
    elif rq['action'] == 'ev_death':
      ids = db.one("""
        INSERT INTO ev_death
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, (SELECT id FROM deaths WHERE death=%s), %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["death"], rq["animals"]))
    elif rq['action'] == 'ev_foster':
      ids = db.one("""
        INSERT INTO ev_foster
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["animals"], rq["weight"]))
      db.execute("""
        INSERT INTO ev_adoption
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s);""",
        (rq["mother"], rq["mother"], u_id, sql_ts(rq["ts"]), rq["animals"], rq["weight"]))
    elif rq['action'] == 'ev_adoption':
      db.execute("""
        INSERT INTO ev_foster
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s);""",
        (rq["mother"], rq["mother"], u_id, sql_ts(rq["ts"]), rq["animals"], rq["weight"]))
      ids = db.one("""
        INSERT INTO ev_adoption
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["animals"], rq["weight"]))
    elif rq['action'] == 'ev_partial_wean':
      ids = db.one("""
        INSERT INTO ev_partial_wean
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["animals"], rq["weight"]))
    elif rq['action'] == 'ev_wean':
      ids = db.one("""
        INSERT INTO ev_wean
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["animals"], rq["weight"]))
    elif rq['action'] == 'ev_semen':
      ids = db.one("""
        INSERT INTO ev_semen
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]),
         rq["volumen"], rq["concentration"], rq["motility"], rq["dosis"]))
    elif rq['action'] == 'ev_ubication':
      ids = db.one("""
        INSERT INTO ev_ubication
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["ubication"]))
    elif rq['action'] == 'ev_feed':
      ids = db.one("""
        INSERT INTO ev_feed
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["weight"]))
    elif rq['action'] == 'ev_condition':
      ids = db.one("""
        INSERT INTO ev_condition
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]),
         rq["condition"], rq["weight"], rq["backfat"]))
    elif rq['action'] == 'ev_milk':
      ids = db.one("""
        INSERT INTO ev_milk
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["weight"], rq["quality"]))
    elif rq['action'] == 'ev_dry':
      ids = db.one("""
        INSERT INTO ev_dry
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"])))
    elif rq['action'] == 'ev_temperature':
      ids = db.one("""
        INSERT INTO ev_temperature
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["temperature"]))
    elif rq['action'] == 'ev_treatment':
      ids = db.one("""
        INSERT INTO ev_treatment
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s, %s, %s, %s, %s, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]),
         rq["treatment"], rq["dose"], rq["frecuency"], rq["days"], rq["route"], rq["note"]))
    elif rq['action'] == 'ev_note':
      ids = db.one("""
        INSERT INTO ev_note
        VALUES(DEFAULT,
               (SELECT id FROM animals WHERE animal=%s),
               (SELECT race_id FROM animals WHERE animal=%s),
               %s, %s, 0, %s)
        RETURNING id, animal_id;""",
        (rq["animal"], rq["animal"], u_id, sql_ts(rq["ts"]), rq["note"]))
    else:
      raise Exception("event not defined!")
    db.commit()
  except Exception as e:
    return await make_response("breeding error -> " + str(e), 500)
  return dumps({"ev_variable": db.one("SELECT * FROM ev_data_function(%s);", (ids[0], )),
                "rules": db.one("SELECT animal_rules_function(%s);", (ids[1], ))[0][1]}, cls=JsonEncoder)


@breeding.route('/breeding/set_animal_event_delete', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_animal_event_delete():
  rq = await request.form
  db.execute("DELETE FROM events WHERE id=%s;", (rq["id"], ))
  db.commit()
  return dumps({"rules": db.one("SELECT animal_rules_function((SELECT id FROM animals WHERE animal=%s));",
                                (rq['animal'], ))[0][1]}, cls=JsonEncoder)


'''
def sync_temperatures():
  ids, rows = [], loads(rq['temperatures'])
  for row in rows:  # id, eartag, ts, temperature
    _id = db.one("""
      INSERT INTO ev_temperature
      VALUES(%s, (SELECT id FROM animals_eartags WHERE eartag=%s), %s, 0, %s)
      RETURNING id;""", tuple(row))
    ids.append(_id[0])
  return ids


def get_temperatures():
  return db.all("""
    SELECT ts::BIGINT * 1000,
      (SELECT temperature FROM ev_temperature WHERE id=events.id),
      CASE WHEN tableoid::regclass::name = 'ev_dry'
        THEN 0 ELSE (SELECT weight FROM ev_milk WHERE id=events.id) END,
      CASE WHEN tableoid::regclass::name NOT IN ('ev_temperature', 'ev_milk')
        THEN tableoid::regclass::name END
    FROM events
    WHERE animal_id=(SELECT id FROM animals WHERE animal=%s) AND
          tableoid IN ('ev_heat'::regclass::oid,
                       'ev_service'::regclass::oid,
                       'ev_abortion'::regclass::oid,
                       'ev_farrow'::regclass::oid,
                       'ev_death'::regclass::oid,
                       'ev_semen'::regclass::oid,
                       'ev_ubication'::regclass::oid,
                       'ev_milk'::regclass::oid,
                       'ev_dry'::regclass::oid,
                       'ev_temperature'::regclass::oid,
                       'ev_treatment'::regclass::oid)
    ORDER BY ts, id;""", (rq['animal'], ))
'''


###################### PARAMETERS #######################
@breeding.route('/breeding/breeding_parameters')
@login_required
@check_user
@db.wrapper
async def breeding_parameters():
  return await render_template('templates/breeding_parameters.html', title='Parametros Reproduccion')


@breeding.route('/breeding/get_breeding_parameters', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_breeding_parameters():
  rq = await request.form
  if 'variable' in rq['action']:
    rows = db.all("SELECT var, val FROM variables;")
  if 'race' in rq['action']:
    rows = db.all("SELECT * FROM races ORDER BY race;")
  if 'death' in rq['action']:
    rows = db.all("SELECT * FROM deaths ORDER BY death;")
  return dumps(rows, cls=JsonEncoder)


@breeding.route('/breeding/set_breeding_parameters', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_breeding_parameters():
  rq = await request.form
  if rq['action'] == 'update_variable':
    db.execute("UPDATE variables SET val=%s WHERE var=%s;", (rq["val"], rq["var"]))
  if rq['action'] == 'insert_race':
    db.execute("INSERT INTO races VALUES (DEFAULT, %s, 1);", (rq["race"], ))
  if rq['action'] == 'update_race':
    db.execute("UPDATE races SET active=ABS(active-1) WHERE id=%s;", (rq["id"], ))
  if rq['action'] == 'insert_death':
    db.execute("INSERT INTO deaths VALUES (DEFAULT, %s, %s, 1);", (rq["death"], rq["type"]))
  if rq['action'] == 'update_death':
    db.execute("UPDATE deaths SET active=ABS(active-1) WHERE id=%s;", (rq["id"], ))
  db.commit()
  return get_breeding_parameters()
