from quart import Blueprint

workers = Blueprint('workers', __name__, template_folder='')

from . import views
from .views import get_settings_menu
