from json import dumps
from quart import current_app, flash, redirect, request, render_template, session, url_for  # abort,
from quart_auth import current_user, login_required, login_user, logout_user
from random import choice
from re import sub
from string import ascii_letters, digits
from werkzeug.security import generate_password_hash, check_password_hash

from app import db, xc
from . import workers
from .forms import LoginForm, ResetForm, WorkerForm, set_worker_data, check_worker, check_email
# from ..pgvega.views import get_reports_menu
# from ..notebooks.views import get_notebooks_menu
from ..utils.admin import User, check_admin, check_user
from ..utils.utils import escape, unescape, JsonEncoder, send_email, paginate


def get_settings_menu(u_admin, u_access):
  admin = [{"url": "workers.workers_handler", "name": "Usuarios"},
           {"url": "pgvega.reports", "name": "Reportes"},
           {"url": "pgvega.alerts", "name": "Alertas"},
           {"url": "notebooks.notebooks_handler", "name": "Notebooks"}] if u_admin else []
  forms = filter(lambda x: u_admin or url_for(x['url']) in u_access,
                 [{"url": "workers.calendar", "name": "Calendario"},
                  {"url": "workers_chats.get_chats", "name": "Chat"},
                  {"url": "workers.get_account", "name": "Usuario"}]) 
  return {"code": "settings", "title": "Ajustes",
          "forms": list(forms)+admin, "reports": [], "notebooks": []}


######################## START ########################
def set_session(activity_url):
  if activity_url != '' and activity_url != 'favicon.ico':
    activity = db.one("""SELECT CURRENT_SCHEMA(), 'activity_' || id, activity, logo
                         FROM public.activities WHERE url=%s;""", (activity_url, ))
    if activity and activity[0] != activity[1]:
      db.execute("SET search_path TO %s;", (activity[1], ))
      session['activity_url'] = activity_url
      session['schema'] = activity[1]
      session['activity'] = activity[2]
      session['logo'] = activity[3]


@workers.route('/<activity_url>')
@db.wrapper
async def home(activity_url='granja'):
  set_session(activity_url)
  return await render_template('templates/index.html', title="Inicio", activity_url=activity_url)


@workers.route('/<activity_url>/login', methods=['GET', 'POST'])
@db.wrapper
async def login(activity_url):
  set_session(activity_url)
  form = LoginForm(await request.form)
  if request.method == 'POST' and form.validate():
    email = form.email.data.strip()
    user = db.one("SELECT id, worker, password FROM workers WHERE email=%s", (email, ), as_dict=True)
    if not user:
      await flash('Email mal escrito o usuario no ha creado cuenta!')
      # return redirect(url_for('workers.register'))
    elif not check_password_hash(user['password'], form.password.data):
      await flash('Contraseña invalida.')
    else:
      login_user(User(user['id']))
      if not await current_user.is_authenticated:
        db.execute("UPDATE workers SET logged=NOW() WHERE id=%s;", (user['id'], ))
        db.commit()
      u_admin = await current_user.admin
      u_access = await current_user.access
      session['menu'] = [fn(u_admin, u_access) for fn in current_app.menu]
      await flash('Hola, %s!' % unescape(user['worker']))
      return redirect(url_for('workers.calendar'))

  return await render_template('templates/login.html', title='Ingreso', form=form, activity_url=activity_url)


@workers.route('/<activity_url>/reset_password', methods=['GET', 'POST'])
@db.wrapper
async def reset_password(activity_url):
  set_session(activity_url)
  rq = await request.form 
  form = ResetForm(rq)
  if request.method == 'POST' and form.validate() and xc.verify(rq, request.remote_addr):
    email = form.email.data.strip()
    user = db.one("SELECT id FROM workers WHERE email=%s", (email, ), as_dict=True)
    if user:
      password = ''.join(choice(ascii_letters + digits) for n in range(6))
      send_email(email, 'Granja Libre - nueva contraseña',
                 '\n\n' + \
                 'Contraseña: ' + \
                 password + \
                 '\n\n' + \
                 request.url_root[:-1] + url_for('workers.login', activity_url=activity_url))
      db.execute("UPDATE workers SET password=%s WHERE id=%s;", (generate_password_hash(password), user['id']))
      db.commit()
      await flash('Nueva contraseña enviada a: %s!' % email)
      return redirect(url_for('workers.login', activity_url=activity_url))
    else:
      await flash('No existe un usuario con este email: %s!' % email)
      return redirect(url_for('workers.login', activity_url=activity_url))  # redirect(url_for('workers.register', activity_url=activity_url))

  return await render_template('templates/reset_password.html', title='Nueva Contraseña',
                               form=form, activity_url=activity_url)


# @workers.route('/<activity_url>/register', methods=['GET', 'POST'])
# async def register(activity_url):
#   set_session(activity_url)
#   rq = await request.form 
#   form = WorkerForm(rq)
#   if request.method == 'POST' and form.validate() and xc.verify(rq, request.remote_addr):
#     data = set_worker_data(form)
#     await check_worker(data['worker'])
#     await check_email(data['email'])
#     db.execute("""INSERT INTO workers
#                   VALUES (DEFAULT, %(worker)s, %(email)s, %(phone)s, %(address)s,
#                           %(lat)s, %(lon)s, %(password)s, FALSE, NULL, NULL, NULL, NOW());""", data)
#     db.commit()
#     await flash('Se ha registrado correctamente! Ahora puede iniciar sesión.')
#
#     return redirect(url_for('workers.login', activity_url=activity_url))
#
#   return await render_template('templates/register.html', form=form, title='Registro', activity_url=activity_url)


######################## ACCOUNT ########################
@workers.route('/granja/worker/account', methods=['GET', 'POST'])
@login_required
@check_user
@db.wrapper
async def get_account():
  u_id = await current_user.id
  user = db.one("SELECT id, worker, email, phone, address, lat, lon FROM workers WHERE id=%s;",
                (u_id, ), as_dict=True)
  form = WorkerForm(await request.form)
  if request.method == 'POST' and form.validate():
    data = set_worker_data(form)
    data['id'] = u_id
    await check_worker(data['worker'])
    await check_email(data['email'])
    db.execute("""UPDATE workers
                  SET worker=%(worker)s, email=%(email)s, phone=%(phone)s, address=%(address)s, lat=%(lat)s,
                      lon=%(lon)s, password=%(password)s WHERE id=%(id)s;""", data)
    db.commit()
    await flash('Ha actualizado su información correctamente!')
  else:
    user['worker'] = unescape(user['worker'])
    user['address'] = unescape(user['address'])
    form = WorkerForm(**user)
  # if delete worker
  #   save_path = "{root}/{folder}".format(root=current_app.root_path + current_app.config['PRODUCT_IMAGES_PATH'],
  #                                        folder="items")
  #   imgs = db.all("SELECT img FROM products_images WHERE product_id=%s", (rq['id'], ))
  #   for img in imgs:
  #     remove("{path}/{file}.JPG".format(path=save_path, file=img[0]))
  #     remove("{path}/{file}_thumb.JPG".format(path=save_path, file=img[0]))

  # workermap = Map(
  #   identifier="view-side",
  #   lat=user['lat'],
  #   lng=user['lon'],
  #   markers=[(user['lat'], user['lon'])],
  #   style="width:100%;height:700px;",
  #   zoom=9
  # )

  # reports=get_reports_menu('^account_'), notebooks=get_notebooks_menu('^account_'), workermap=workermap
  return await render_template('templates/account.html', title='Cuenta', form=form)


@workers.route('/granja/worker/logout')
@login_required
@check_user
@db.wrapper
async def logout():
  logout_user()
  await flash('Ha cerrado la sesión correctamente.')

  return redirect(url_for('workers.login', activity_url=session['activity_url']))


######################## CALENDAR ########################
@workers.route('/granja/worker/calendar')
@login_required
@check_user
@db.wrapper
async def calendar():
  return await render_template('templates/calendar.html', title="Calendario")


@workers.route('/granja/worker/calendar_get', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_calendar():
  rq = await request.form
  u_worker = await current_user.worker
  rows = db.all("""
    WITH events AS (SELECT * FROM calendar
                    WHERE start_date>=%s AND start_date<=%s OR
                          end_date>=%s AND end_date<=%s)
    SELECT id, color, title AS name, start_date AS startdate, end_date AS enddate
    FROM events WHERE worker=%s OR color IS NULL;""",
    (rq['start_date'], rq['end_date'], rq['start_date'], rq['end_date'], u_worker), as_dict=True)
  return dumps(rows, cls=JsonEncoder)


@workers.route('/granja/worker/calendar_set', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_calendar():
  rq = await request.form
  u_worker = await current_user.worker
  rows = db.all("""
    INSERT INTO calendar VALUES(DEFAULT, %s, %s, %s, %s, %s)
    RETURNING id, color, title AS name, start_date AS startdate, end_date AS enddate;""",
    (u_worker, rq['color'], rq['title'], rq['start_date'], rq['end_date']), as_dict=True)
  db.commit()
  return dumps(rows, cls=JsonEncoder)


######################## WORKERS ########################
def get_workers_data(rq):
  params = {'filter':'{}', 'sort_col':'1', 'sort_dir':'ASC', 'lines':30,
            'page':int(rq['page']) if request.method == 'POST' else 1}
  return paginate(db, params, """
    SELECT id, unescape(worker) AS worker, email, phone, unescape(address) AS address,
           lat, lon, admin, created, logged, blocked, readonly, access
    FROM workers""")


@workers.route('/granja/workers/list')
@login_required
@check_admin
@db.wrapper
async def workers_handler():
  rq = await request.form
  # reports=get_reports_menu('^workers_'), notebooks=get_notebooks_menu('^workers_')
  return await render_template('templates/workers.html', title='Miembros',
                               data=dumps(get_workers_data(rq), cls=JsonEncoder))


@workers.route('/granja/workers/paginate', methods=['POST'])
@login_required
@check_admin
@db.wrapper
async def paginate_workers():
  rq = await request.form
  return dumps(get_workers_data(rq), cls=JsonEncoder)


@workers.route('/granja/workers/worker_access', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_access():
  access = []
  urls = [str(rule) for rule in current_app.url_map.iter_rules()]
  for u in urls:
    if u != '/' and '<' not in u and 'admin-' not in u:
      x = u.split('/')
      access.append([u, '^%s^%s' % (x[-2], x[-1])])
  reports = db.all("SELECT code, title FROM querys;", as_dict=True)
  for r in reports:
    access.append([url_for('pgvega.get_report', code=r['code']),
                   '^%s^%s' % (sub('^([a-z]+)_.+', '\\1', r['code']), r['title'])])
  notebooks = db.all("SELECT id, code, title FROM notebooks;", as_dict=True)
  for n in notebooks:
    access.append([url_for('notebooks.get_notebook', notebook_id=n['id']),
                   '^%s^%s' % (sub('^([a-z]+)_.+', '\\1', n['code']), n['title'])])
  return dumps(sorted(access, key=lambda x: x[1]), cls=JsonEncoder)  # .lower()


@workers.route('/granja/workers/worker_set', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_worker():
  rq = await request.form
  worker = sub('[^a-zA-Z0-9 \]\[{}()"'';:,.&|?!~#$%<>=/*+-]+', '-', escape(rq['worker']))
  phone = sub('[^0-9+-]+', '-', rq['phone'])
  password = generate_password_hash(rq['password'])
  access = rq['access'].split(',')
  access.extend([sub('<activity_url>', session['activity_url'], str(u)) for u in current_app.url_map.iter_rules() if str(u) == '/' or '<activity_url>' in str(u)])
  if rq['id'] == '':
    db.execute("""INSERT INTO workers VALUES (DEFAULT, %s, %s, %s, NULL, NULL, NULL, %s,
                                              FALSE, FALSE, %s, %s, NULL, NOW(), NULL, NULL);""",
               (rq['worker'], rq['email'], rq['phone'], password, rq['readonly'] == 'on', access))
  else:
    db.execute("UPDATE workers SET worker=%s, email=%s, phone=%s, readonly=%s, access=%s WHERE id=%s;",
               (rq['worker'], rq['email'], rq['phone'], rq['readonly'] == 'on', access, rq['id']))
    if rq['password'] != '':
      db.execute("UPDATE workers SET password=%s WHERE id=%s;", (password, rq['id']))
  db.commit()

  return dumps(get_workers_data(rq), cls=JsonEncoder)


# @workers.route('/granja/workers/set', methods=['POST'])
# @login_required
# @check_admin
# @db.wrapper
# async def set_workers():
#   rq = await request.form
#   ids = tuple(rq['ids'].split(','))
#   if rq['action'] == 'block':
#     db.execute("UPDATE workers SET blocked=NOW() WHERE id IN %s;", (ids, ))
#   elif rq['action'] == 'unblock':
#     db.execute("UPDATE workers SET blocked=NULL WHERE id IN %s;", (ids, ))
#   db.commit()
#
#   return dumps(get_workers_data(rq), cls=JsonEncoder)
