from quart_auth import current_user
from re import sub
from werkzeug.security import generate_password_hash
from wtforms import Form, PasswordField, SelectField, StringField, SubmitField
from wtforms.validators import DataRequired, Email, EqualTo, Regexp

from app import db
from ..utils.utils import escape
# from ..utils.form import Form


def set_worker_data(form):
  return {'worker': sub('[^a-zA-Z0-9 \]\[{}()"'';:,.&|?!~#$%<>=/*+-]+', '-', escape(form.worker.data)),
          'email': form.email.data.strip(),
          'phone': sub('[^0-9+-]+', '-', form.phone.data),
          'address': sub('[^a-zA-Z0-9 \]\[{}()"'';:,.&|?!~#$%<>=/*+-]+', '-', escape(form.address.data)),
          'lat': form.lat.data,
          'lon': form.lon.data,
          'password': generate_password_hash(form.password.data)}


class LoginForm(Form):
  email = StringField('Email', validators=[DataRequired(), Email(message='Email inválido.')])
  password = PasswordField('Contraseña', validators=[DataRequired(message='Por favor llene este campo.')])
  submit = SubmitField('Ingreso')


class ResetForm(Form):
  email = StringField('Email', validators=[DataRequired(), Email(message='Email inválido.')])
  submit = SubmitField('Enviar')


class WorkerForm(Form):
  worker = StringField('Usuario', validators=[DataRequired(message='Por favor llene este campo.')])
  email = StringField('Email', validators=[DataRequired(), Email(message='Email inválido.')])
  phone = StringField('Telefono', validators=[DataRequired(message='Por favor llene este campo.')])
  address = StringField('Direccion', validators=[DataRequired(message='Por favor llene este campo.')])
  lat = StringField('Latitud', validators=[DataRequired(), Regexp('^[-+]?[0-9]*\.?[0-9]+$', message='Número inválido.')])
  lon = StringField('Longitud', validators=[DataRequired(), Regexp('^[-+]?[0-9]*\.?[0-9]+$', message='Número inválido.')])
  password = PasswordField('Contraseña', validators=[DataRequired(message='Por favor llene este campo.'),
                                          EqualTo('confirm_password', message='Clave debe ser igual a su confirmación.')])
  confirm_password = PasswordField('Confirmar Clave', validators=[DataRequired()])
  submit = SubmitField('Registrarse', render_kw={'class':'btn btn-primary'})


async def check_worker(worker):
  row = db.one("SELECT id FROM workers WHERE worker=%s", (worker, ))
  if row and (not await current_user.is_authenticated or row[0] != int(await current_user.id)):
    raise Exception('Usuario ya esta en uso!')


async def check_email(email):
  row = db.one("SELECT id FROM workers WHERE email=%s", (email, ))
  if row and (not await current_user.is_authenticated or row[0] != int(await current_user.id)):
    raise Exception('Email ya esta en uso!')
