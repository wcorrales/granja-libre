SET search_path TO :app_schema;

-- ************************************** --
------------------ TABLES ------------------
-- ************************************** --
CREATE TABLE workers (
  id SERIAL PRIMARY KEY NOT NULL,
  -- zone_id INTEGER NOT NULL,
  worker VARCHAR(150) UNIQUE NOT NULL,
  email VARCHAR(60) UNIQUE NOT NULL,
  phone VARCHAR(20) UNIQUE NOT NULL,
  address VARCHAR(250) DEFAULT NULL,
  lat DOUBLE PRECISION DEFAULT NULL,
  lon DOUBLE PRECISION DEFAULT NULL,
  password VARCHAR(128) NOT NULL,
  notifications BOOLEAN NOT NULL,
  admin BOOLEAN NOT NULL,  -- SELECT * FROM pg_get_keywords(); admin: unreserved
  readonly BOOLEAN NOT NULL,
  access TEXT[] NOT NULL,
  bot INTEGER DEFAULT NULL,
  created TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  logged TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  blocked TIMESTAMP WITHOUT TIME ZONE DEFAULT NULL,
  CHECK (TRIM(worker) <> '' AND worker !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+'),
  CHECK (TRIM(email) <> '' AND email ~ '\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*'),
  CHECK (TRIM(phone) <> '' AND phone !~ '[^0-9+-]+'),
  CHECK (TRIM(address) <> '' AND address !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+')
  -- , FOREIGN KEY(zone_id) REFERENCES zones(id)
);

CREATE TABLE calendar (
  id SERIAL PRIMARY KEY NOT NULL,
  worker VARCHAR(50) NOT NULL,
  color VARCHAR(10) DEFAULT NULL,
  title VARCHAR(100) NOT NULL,
  start_date DATE NOT NULL,
  end_date DATE NOT NULL,
  CHECK (TRIM(color) <> '' AND color !~ '[^a-z0-9#]+'),
  CHECK (TRIM(title) <> '' AND title !~ '[^a-zA-Z0-9 ()#$:,./-]+')
);

-- ************************************* --
------------------ VIEWS ------------------
-- ************************************* --


-- ************************************** --
------------------ QUERYS ------------------
-- ************************************** --


-- **************************************** --
------------------ FUNCTIONS -----------------
-- **************************************** --


-- **************************************** --
------------------ TRIGGERS ------------------
-- **************************************** --
