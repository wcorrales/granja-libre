Date.prototype.toSql = function() {
  return this.getFullYear() + '-' +
         new String(this.getMonth() + 1).padStart(2, '0') + '-' +
         new String(this.getDate()).padStart(2, '0');
};

/*
Date.prototype.getWeek = function (dowOffset) {
  dowOffset = typeof(dowOffset) == 'int' ? dowOffset : 0;
  var newYear = new Date(this.getFullYear(),0,1);
  var day = newYear.getDay() - dowOffset;
  day = (day >= 0 ? day : day + 7);
  var daynum = Math.floor((this.getTime() - newYear.getTime() - (this.getTimezoneOffset()-newYear.getTimezoneOffset())*60000)/86400000) + 1;
  var weeknum;
  if(day < 4) {
    weeknum = Math.floor((daynum+day-1)/7) + 1;
    if(weeknum > 52) {
      nYear = new Date(this.getFullYear() + 1,0,1);
      nday = nYear.getDay() - dowOffset;
      nday = nday >= 0 ? nday : nday + 7;
      weeknum = nday < 4 ? 1 : 53;
    }
  } else {
    weeknum = Math.floor((daynum+day-1)/7);
  }
  return weeknum;
};
*/

function addDays(date, days) {
  date.setDate(date.getDate() + parseInt(days, 10));
  return date;
}

function getDays(diff) { return Math.floor(diff/86400000); }

function getMinutes(diff) { return Math.floor(diff/60000); }

// diff = d1.getTime() - d2.getTime()
// age = today.getFullYear() - birth.getFullYear()

function datetime_local(datetime){
  return datetime?new Date(datetime).toLocaleString(TIMEZONE, {hour12: false, day: '2-digit', month: '2-digit', year: '2-digit', minute: '2-digit', hour: '2-digit'}):'';
}

/*
function strToDate(str) {
  str = sqlToLocal(str)
  var a=str.split('/');
  return new Date(a[2], a[1]-1, a[0]);
}
*/

function stripHtml(str) { return str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/g, ''); }

function formatCurrency(num, dec) {
  if(!num || isNaN(num)) return;
  sign = (num == (num = Math.abs(num)));
  num = num.toFixed(dec).toString();
  for (var i=num.length-(4+(dec?dec:-1)); i>0; i=i-3)
    num = num.substring(0,i)+','+num.substring(i);
  return ((sign?'':'-') + num);
}

// function parseFix(v) { return parseFloat(v.toFixed(2)); }

/////////////////////// blockUI //////////////////////////
$.extend($.blockUI.defaults, {
  baseZ: 10000,
  message: " ",
  css: {top:"50%", left:"50%"},
  onBlock: function () {
    var $blockOverlay = $(".blockUI.blockOverlay").not(".has-spinner");
    var $blockMsg = $blockOverlay.next(".blockMsg");
    $blockOverlay.addClass("has-spinner");
    new Spinner({color:"#fff"}).spin($blockMsg.get(0));
  }
});


/////////////////////// request //////////////////////////
var csrftoken = $('meta[name=csrf-token]').attr('content');

function request(url, pd, response) {
//  if(navigator.onLine) {
  $.blockUI();
  $.ajax({beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken)
            }
          },
          type: "POST",
          url: url,
          data: pd,
          contentType: false,
          processData: false,
          dataType: "json"})
    .done(function(rs){ response(rs); })
    .fail(function(jqXHR, textStatus){
      alert(jqXHR.responseText);
    })
    .always(function(){ $.unblockUI(); });
}

function request_pdf(url, pd, response) {
//  if(navigator.onLine) {
  $.blockUI();
  $.ajax({beforeSend: function(xhr, settings) {
            if (!/^(GET|HEAD|OPTIONS|TRACE)$/i.test(settings.type) && !this.crossDomain) {
              xhr.setRequestHeader("X-CSRFToken", csrftoken)
            }
          },
          type: "POST",
          url: url,
          data: pd,
          contentType: false,
          processData: false,
          xhrFields: {responseType: 'blob'}})
    .done(function(blob, textStatus, jqXHR){
      var cd = jqXHR.getResponseHeader('content-disposition');
      response(blob, cd.replace(/filename=/, ''));
    })
    .fail(function(jqXHR, textStatus){
      alert(jqXHR.responseText);
    })
    .always(function(){ $.unblockUI(); });
}


/////////////////////////////////////////////////////////////
function rating(stars){
  return $.map(Array.from(Array(5).keys()), function(star){
           return '<span class="fa fa-star '+ ((star < stars)?'rating_checked':'')+'"></span>';
         }).join('');
}

function paginate($nav, rs){
  if (!rs['rows'] || rs['rows'].length < 1 || rs['rows'].length == rs['count']) {
    $nav.empty();
  } else {
    var page = parseInt(rs['page'], 10);
    $nav.html(
      '<ul class="pagination justify-content-center">' +
      '  <li class="page-item '+ ((page == 1)?'disabled':'') + '">' +
      '    <a class="page-link" href="#'+ (page-1) +'">' +
      '      <span aria-hidden="true">&laquo;</span>' +
      '      <span class="sr-only">Anterior</span>' +
      '    </a>' +
      '  </li>' +
      $.map(Array.from(Array(rs['pages']).keys()), function(p){
        return '<li class="page-item '+ ((page == (p+1))?'active':'') + '">' +
               '  <a href="#'+ (p+1) +'" class="page-link" aria-label="'+ (p+1) +'">'+ (p+1) +'</a>' +
               '</li>';
      }).join('') +
      '  <li class="page-item '+ ((page == rs['pages'])?'disabled':'') + '">' +
      '    <a class="page-link" href="#'+ (page+1) +'">' +
      '      <span aria-hidden="true">&raquo;</span>' +
      '      <span class="sr-only">Posterior</span>' +
      '    </a>' +
      '  </li>' +
      '</ul>'
    );
  }
}


/////////////////////////////////////////////////////////////
function newTab(pd) {
  txt = '<html><head>';
  txt += '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />';
  txt += '<!--link rel="stylesheet" href="css/monthly.css"-->';
  txt += '<style>input {display:none}</style>';
  txt += '<style>.table {border-collapse: collapse; border-style: hidden;}</style>';
  txt += '<style>.table th {border-top:1px solid #ccc; border-bottom:1px solid #ccc;}</style>';
  txt += '<style>.table th, .table td {text-align:right; vertical-align:top; padding:.3em;}</style>';
  txt += '<style>.table th:first-child, .table td:first-child {text-align:left}</style>';
  txt += '<style>.right th, .right td {text-align:right; vertical-align:top; padding:.3em;}</style>';
  txt += '<style>.right th:first-child, .right td:first-child {text-align:left}</style>';
  txt += '</head><body style="max-width:800px;">';
  txt += '<table><tr><td style="width:550px; vertical-align:middle;">';
  txt += print_header;
  txt += '</td><td style="width:150px;">';
  txt += '<img src="/static/logos/' + logo + '" />';
  txt += '</td></tr></table>';
  txt += pd['title'];
  txt += pd['content'];
  txt += '</body></html>';
  OpenWindow=window.open('', '');
  OpenWindow.document.write(txt);
  OpenWindow.document.close();
}

function print_qrcode(data){
  var txt = '<html><head>';
  txt += '<meta http-equiv="Content-Type" content="text/html;" />';
  txt += '</head><body>';
  txt += '<h1>'+data+'</h1>'
  txt += '</body></html>';
  OpenWindow=window.open('', '');
  OpenWindow.document.write(txt);
  OpenWindow.document.body.appendChild(kjua({"text":data}));
  OpenWindow.document.close();
}

function download(content, filename) {
  var a = document.createElement('a'),
      blob = new Blob([content], {'type':'application/octet-stream'});
  a.href = window.URL.createObjectURL(blob);
//  a.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(content);
  a.download = filename;
  a.click();
//  saveAs(blob, filename);
}

function downloadPDF(content, filename) {
  var a = document.createElement('a'),
      blob = new Blob([content], {'type':'application/pdf'});
  a.href = window.URL.createObjectURL(blob);
  a.download = filename;
  a.click();
//  saveAs(blob, filename);
}


function exportXLS(head, body) {
  var rows = [];  // [head.join(';')];
  for (var k in body){
    var row = $.map(body[k], function(n){
      n = n?stripHtml(String(n).replace(/,/g, "")):"";
      return n.length > 11 && n.replace(/\d+/, '') == ''?"'"+n:n;
    });
//    rows.push(row.join(';'));
    rows.push('<td>'+row.join('</td><td>')+'</td>');
  }

//  var a = document.createElement('a');
//  a.href = 'data:text/csv;charset=UTF-8,' + encodeURIComponent(rows.join('\n'));
//  a.download = "file.csv";
//  a.click();

  var MSDocType   = 'excel';  // 'word'
  var MSDocExt    = (MSDocType == 'excel') ? 'xls' : 'doc';
  var MSDocSchema = 'xmlns:x="urn:schemas-microsoft-com:office:' + MSDocType + '"';
  var docData = '<table>' +
                '<thead><th>' + head.join('</th><th>') + '</th></thead>' +
                '<tbody><tr>' + rows.join('</tr><tr>') + '</tr></tbody>' +
                '</table>';
  var docFile = '<html xmlns:o="urn:schemas-microsoft-com:office:office" ' + MSDocSchema + ' xmlns="http://www.w3.org/TR/REC-html40">';
  docFile += '<meta http-equiv="content-type" content="application/vnd.ms-' + MSDocType + '; charset=UTF-8">';
  docFile += "<head>";
  if ( MSDocType === 'excel' ) {
    docFile += "<!--[if gte mso 9]>";
    docFile += "<xml>";
    docFile += "<x:ExcelWorkbook>";
    docFile += "<x:ExcelWorksheets>";
    docFile += "<x:ExcelWorksheet>";
    docFile += "<x:Name>";
    docFile += "page1";
    docFile += "</x:Name>";
    docFile += "<x:WorksheetOptions>";
    docFile += "<x:DisplayGridlines/>";
    docFile += "</x:WorksheetOptions>";
    docFile += "</x:ExcelWorksheet>";
    docFile += "</x:ExcelWorksheets>";
    docFile += "</x:ExcelWorkbook>";
    docFile += "</xml>";
    docFile += "<![endif]-->";
  }
  docFile += "<style>br {mso-data-placement:same-cell;}</style>";
  docFile += "</head>";
  docFile += "<body>";
  docFile += docData;
  docFile += "</body>";
  docFile += "</html>";

  // console.log(docFile);

  blob = new Blob([docFile], {type: 'application/vnd.ms-' + MSDocType});
  saveAs(blob, 'file.' + MSDocExt);
}
