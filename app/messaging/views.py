from json import dumps, loads
from io import BytesIO, BufferedReader
from pickle import dump, load
from quart import request
from telebot import TeleBot, types
from re import sub

from app import db
from config import AppConfig
from . import messaging
from ..pgvega.report_utils import set_pdf, set_header_html, set_report_html
from ..pgvega.views import get_report_data
from ..notebooks.views import get_notebook_data


######################## TELEGRAM ########################
COMMANDS = AppConfig.BOT_COMMANDS
TOKEN = AppConfig.BOT_TOKEN
USERS_SCHEMAS_PATH = AppConfig.BOT_USERS_SCHEMAS_PATH
WEBHOOK_HOST = AppConfig.BOT_WEBHOOK_HOST
WEBHOOK_PATH = AppConfig.BOT_WEBHOOK_PATH

bot = TeleBot(TOKEN)


def get_commands(commands):
  help_text = ["%s: %s" % (key, commands[key]['description']) for key in commands]
  return 'Comandos disponibles:\n' + '\n'.join(help_text)


def get_users_schemas():
  try:
    with open(USERS_SCHEMAS_PATH, 'rb') as f:
      return load(f)
  except EOFError:
    return {}


@bot.message_handler(commands=['add_user'])  # /add_user id schema
def set_users_schemas(msg):
  cid = msg.chat.id
  users_schemas = get_users_schemas()
  if cid in users_schemas or len(users_schemas) < 1:
    args = msg.text.split(' ')[1::]
    schema = args[1] if len(args) > 1 else users_schemas[cid]
    users_schemas[int(args[0])] = schema
    with open(USERS_SCHEMAS_PATH, 'wb') as f:
      dump(users_schemas, f)
    bot.send_message(cid, 'add_user id(%s) schema(%s)' % (args[0], schema))


@bot.message_handler(commands=['start', 'help'] + list(map(lambda x: sub('^/', '', x), COMMANDS.keys())))
def get_information(msg):
  cid = msg.chat.id
  users_schemas = get_users_schemas()
  bot.send_chat_action(cid, 'typing')
  if cid not in users_schemas:
    bot.send_message(cid, '/add_user %s' % cid)
  else:
    db.connect(users_schemas[cid])
    user = db.one('SELECT id FROM workers WHERE bot=%s;', (cid, ))  # msg.from_user.id
    user_id = -1 if not user else user[0]
    db.close()
    if user_id < 0:
      markup = types.ReplyKeyboardMarkup(one_time_keyboard=True)
      markup.add(types.KeyboardButton('enviar contacto', request_contact=True))
      bot.send_message(cid, "Acceso no permitido!", reply_markup=markup)
    else:
      command = sub('(\w+)\s.+', '\\1', msg.text)
      if command in COMMANDS.keys():
        try:
          args = []
          if len(COMMANDS[command]['params']) > 0:
            args = msg.text.split(' ')[1::]
            if len(args) < 1:
              raise Exception('Mantener presionado el comando y luego escribir fechas.\n' + \
                              command + ' yyyy-mm-dd yyyy-mm-dd')
            elif len(args) < 2 or len(args) > 2:
              raise Exception('Escribir fecha inicial y fecha final.')
          rq = dict(zip(COMMANDS[command]['params'], args))
          params = ', '.join([k + ': '+v for k, v in rq.items()])
          rq['current_user'] = user_id
          db.connect(users_schemas[cid])
          activity = db.one("""SELECT unescape(activity) FROM public.activities
                               WHERE id=REPLACE((SELECT CURRENT_SCHEMA), 'activity_', '')::INT;""")[0]
          if COMMANDS[command]['notebook']:
            data = get_notebook_data(COMMANDS[command]['code'], rq, {})
            content = f'<span style="font-size:200%;"><h2 style="display:inline;">{data["title"]}</h2>' + \
                      f'&nbsp;{params}</span>{data["body"]}'
          else:
            content = set_report_html(get_report_data(COMMANDS[command]['code'], rq, pdf=True), params)
          db.close()
          pdf = BytesIO(set_pdf(set_header_html('Granja Libre - ' + activity), content))
          pdf.name = 'doc.pdf'
          bot.send_document(cid, BufferedReader(pdf))
        except Exception as e:
          bot.send_message(cid, "Error: " + str(e))
      else:
        bot.send_message(cid, get_commands(COMMANDS))


@bot.message_handler(content_types=['contact'])
def set_contact(msg):
  cid = msg.chat.id
  users_schemas = get_users_schemas()
  bot.send_chat_action(cid, 'typing')
  if cid not in users_schemas:
    bot.send_message(cid, '/add_user %s' % cid)
  else:
    db.connect(users_schemas[cid])
    user = db.one('UPDATE workers SET bot=%s WHERE phone=%s RETURNING id;',
                  (cid,  '+' + sub('\D', '', msg.contact.phone_number)))  # msg.from_user.id
    user_id = -1 if not user else user[0]
    db.close()
    if user_id < 0:
      bot.send_message(cid, "Error en verificacion contacto, telefono no autorizado!")
    else:
      bot.send_message(cid, "Verificacion contacto completa!")
      bot.send_message(cid, get_commands(COMMANDS), reply_markup=types.ReplyKeyboardRemove())


@bot.message_handler(func=lambda msg: msg.content_type == 'text')
def msg_handler(msg):
  cid = msg.chat.id
  users_schemas = get_users_schemas()
  bot.send_chat_action(cid, 'typing')
  if cid not in users_schemas:
    bot.send_message(cid, '/add_user %s' % cid)
  else:
    bot.send_message(cid, get_commands(COMMANDS))


@messaging.route(WEBHOOK_PATH + TOKEN, methods=['POST'])
async def getMessage():
  bot.process_new_updates([types.Update.de_json(await request.get_json())])
  return "!", 200


@messaging.route(WEBHOOK_PATH)
async def webhook():
  # https://api.telegram.org/bot[TOKEN]/getWebhookInfo
  # https://api.telegram.org/bot[TOKEN]/setWebhook?url=https://granjalibre.com/granja_bot/[TOKEN]
  # https://api.telegram.org/bot[TOKEN]/deleteWebhook
  bot.remove_webhook()
  bot.set_webhook(url=WEBHOOK_HOST + WEBHOOK_PATH + TOKEN)
  return "!", 200


######################## WHATSAPP ########################
'''
from json import dumps
from quart import request, render_template
from quart_auth import login_required
from requests import post

from app import db
from . import messaging
from ..utils.admin import check_admin
from ..utils.utils import JsonEncoder, paginate  # escape, unescape,


def get_whatsapp_data(rq):
  params = {'filter':'{}', 'sort_col':'1', 'sort_dir':'DESC', 'lines':20,
            'page':int(rq['page']) if request.method == 'POST' else 1}
  return paginate(db, params, """
    SELECT id, status, document, unescape(worker) AS worker, phone, unescape(message) AS message
    FROM whatsapp w, LATERAL (SELECT worker, phone FROM workers WHERE id=w.worker_id) t""")


@messaging.route('/admin-messaging/whatsapp')
@login_required
@check_admin
@db.wrapper
async def whatsapp_handler():
  rq = await request.form
  return await render_template('templates/whatsapp.html', title="Whatsapp",
                               data=dumps(get_whatsapp_data(rq), cls=JsonEncoder))


@messaging.route('/admin-messaging/whatsapp_get_msgs', methods=['POST'])
@login_required
@check_admin
@db.wrapper
async def whatsapp_get_msgs():
  rq = await request.form
  return dumps(get_whatsapp_data(rq), cls=JsonEncoder)


@messaging.route('/admin-messaging/whatsapp_get_status', methods=['POST'])
@login_required
@check_admin
@db.wrapper
async def whatsapp_get_status():
  rs = post('http://127.0.0.1:5005/get_status')
  return rs.text


@messaging.route('/admin-messaging/whatsapp_set_status', methods=['POST'])
@login_required
@check_admin
@db.wrapper
async def whatsapp_set_status():
  rq = await request.form
  rs = post('http://127.0.0.1:5005/set_status', data={'action':rq['action']})
  return rs.text


@messaging.route('/admin-messaging/whatsapp_set_msg', methods=['POST'])
@login_required
@check_admin
@db.wrapper
async def whatsapp_set_msg():
  rq = await request.form
  rs = post('http://127.0.0.1:5005/set_msg', data={'phone':rq['phone'], 'msg':rq['msg']})
  return rs.text
'''


######################## SMS ########################
'''
@messaging.route('/<activity>/sms', methods=['POST'])
@db.wrapper
async def sms_handler(activity):
  rq = await request.form.to_dict()
  try:
    schema_id, name, logo, temp = wrapper(get_activity_id_name, {"url":activity})
    rq['privilege'] = 'select'
    rq['schema'] = 'activity_%s' % schema_id
    rs = await make_response(wrapper(response_sms, rq, True))
  except Exception as e:
    rs = await make_response("sms error -> " + str(e), 500)
  rs.headers["Access-Control-Allow-Origin"] = "*"
  return rs
'''
