SET search_path TO :app_schema;

-- ************************************** --
------------------ TABLES ------------------
-- ************************************** --
CREATE TABLE whatsapp (
  id SERIAL PRIMARY KEY NOT NULL,
  worker_id INTEGER NOT NULL,
  document INTEGER NOT NULL,
  status VARCHAR(10) NOT NULL,
  message VARCHAR(250) NOT NULL,
  CHECK (TRIM(message) <> '' AND message !~ '[^a-zA-Z0-9 \]\[{}()'';:,.&|?!~#$%<>=/*+-]+'),
  FOREIGN KEY(worker_id) REFERENCES workers(id) ON DELETE CASCADE
);

/*** BOTS ***/
/*
CREATE TABLE bots (
  id SERIAL PRIMARY KEY NOT NULL,
  url VARCHAR(20) NOT NULL,
  person VARCHAR(50) UNIQUE NOT NULL,
  phone VARCHAR(20) UNIQUE NOT NULL,
  bot INTEGER DEFAULT NULL,
  CHECK (TRIM(url) <> '' AND url !~ '[^a-z0-9]+'),
  CHECK (TRIM(person) <> '' AND person !~ '[^a-zA-Z0-9 ().-]+'),
  CHECK (TRIM(phone) <> '' AND phone !~ '[^0-9+-]+')
);
*/

-- ************************************* --
------------------ VIEWS ------------------
-- ************************************* --


-- ************************************** --
------------------ QUERYS ------------------
-- ************************************** --


-- **************************************** --
------------------ FUNCTIONS -----------------
-- **************************************** --


-- **************************************** --
------------------ TRIGGERS ------------------
-- **************************************** --

