from quart import Blueprint

messaging = Blueprint('messaging', __name__, template_folder='')

from . import views
