from json import dumps
from lxml.html import fromstring, tostring
from quart import make_response, request, render_template, session, url_for
from quart_auth import current_user, login_required
from nbconvert.preprocessors import ExecutePreprocessor
from nbconvert import HTMLExporter
from nbformat.v4 import new_code_cell
from nbformat import reads

from app import db
from . import notebooks
from ..utils.admin import check_admin, check_user
from ..pgvega.report_utils import set_pdf, set_header_html
from ..utils.utils import JsonEncoder, paginate  # escape, unescape,


ep = ExecutePreprocessor(timeout=600, kernel_name='python3')
html_exporter = HTMLExporter()  # template_file='basic'


def get_notebooks_menu(code):
  return db.all("SELECT id, title FROM notebooks WHERE code ~ %s ORDER BY code;", (code, ), as_dict=True)


def get_notebooks_data(rq):
  params = {'filter':'{}', 'sort_col':'2', 'sort_dir':'ASC', 'lines':20,
            'page':int(rq['page']) if request.method == 'POST' else 1}
  return paginate(db, params, "SELECT id, code, title, no_input, params FROM notebooks")


def get_notebook_data(notebook_id, values, values_2):
  ttl, ntb, n_i = tuple(db.one("SELECT title, notebook, no_input FROM notebooks WHERE id=%s;",
                               (notebook_id, )))
  nb = reads(dumps(ntb), as_version=4)
  if len(values) > 0:
    code_cell =  '\n'.join(('# Cell inserted during automated execution.',
                            '\n'.join(("{} = {}".format(k, repr(v)) for k, v in values.items())),
                            '\n'.join(("{} = {}".format(k, repr(v)) for k, v in values_2.items()))))
    nb['cells'].insert(1, new_code_cell(code_cell))
  ep.preprocess(nb)
  html_exporter.exclude_input = n_i
  html_exporter.exclude_output_prompt = n_i
  (body, resources) = html_exporter.from_notebook_node(nb)
  divs = fromstring(body).xpath("//div[contains(@class, 'jp-RenderedHTML')]")
  body = ''.join(tostring(div.getchildren()[0], encoding='unicode') for div in divs)
  return {'title': ttl, 'body': body}


@notebooks.route('/admin-notebooks/list')
@login_required
@check_admin
@db.wrapper
async def notebooks_handler():
  return await render_template('templates/notebooks.html', title="Notebooks",
                               data=dumps(get_notebooks_data({}), cls=JsonEncoder))


@notebooks.route('/admin-notebooks/paginate', methods=['POST'])
@login_required
@check_admin
@db.wrapper
async def paginate_notebooks():
  rq = await request.form
  return dumps(get_notebooks_data(rq), cls=JsonEncoder)


@notebooks.route('/get_notebook/<notebook_id>', methods=['GET', 'POST'])
@login_required
@check_user
@db.wrapper
async def get_notebook(notebook_id, pdf=False):
  u_id = await current_user.id
  title, params = tuple(db.one("SELECT title, params FROM notebooks WHERE id=%s;", (notebook_id, )))
  values = {}
  values_2 = {'current_user_id': u_id}
  body = None
  if request.method == 'POST':
    if len(params) > 0:
      rq = await request.form  # {**request.form, **request.args}
      for k, v in params.items():
        if v == 'file':
          f = await request.files[k]
          values_2[k] = f.read()
          values_2[k+'_name'] = f.filename
        elif v == 'check':
          values[k] = rq[k] if k in rq else ''
        else:
          values[k] = rq[k]
      # params.update(request.args)
    body = get_notebook_data(notebook_id, values, values_2)['body']
  if pdf:
    return (title, values, body)
  return await render_template('templates/notebook.html', title=title, params=params,
                               values=values, body=body or '', n_id=notebook_id)


'''
@notebooks.route('/get_notebook_pdf/<notebook_id>', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_notebook_pdf(notebook_id):
  try:
    activity = db.one("""SELECT unescape(activity) FROM public.activities
                         WHERE id=REPLACE(CURRENT_SCHEMA(), 'activity_', '')::INT;""")[0]
    title, params = tuple(db.one("SELECT title, params FROM notebooks WHERE id=%s;", (notebook_id, )))
    body = get_notebook(notebook_id, values, values_2, pdf=True)
    pdf = set_pdf(set_header_html('Granja Libre - ' + activity + '<br>' + \
                                  title + [k + ': '+v for k, v in params.items()].join(', ')),
                  body)
    rs = await make_response(pdf)
    rs.content_type = 'application/pdf'
    rs.headers["Content-Disposition"] = "filename=notebook_%s.pdf" % notebook_id
  except Exception as e:
    rs = await make_response("db error -> " + str(e), 500)
  return rs
'''


@notebooks.route('/admin-notebooks/set', methods=['POST'])
@login_required
@check_admin
@db.wrapper
async def set_notebook():
  rq = await request.form
  if rq['id'] == '':
    db.execute("INSERT INTO notebooks VALUES (DEFAULT, %s, %s, %s, %s, %s);",
               (rq['notebook'], rq['params'], rq['code'], rq['title'], rq['no_input'] == 'true'))
  else:
    db.execute("DELETE FROM notebooks WHERE id=%s;", (rq['id'], ))
  db.commit()
  return dumps(get_notebooks_data(rq), cls=JsonEncoder)
