from quart import Blueprint

notebooks = Blueprint('notebooks', __name__, template_folder='')

from . import views
