from datetime import datetime
from json import dumps, loads
from math import ceil
from quart import current_app, flash, request, render_template, session
from quart_auth import current_user, login_required
from re import sub

from app import db, ws
from . import workers_chats
from ..utils.admin import check_user
from ..utils.utils import escape, unescape  # , JsonEncoder


@workers_chats.route('/workers_chats/chats')
@login_required
@check_user
@db.wrapper
async def get_chats():
  rows = db.all("""SELECT thread_id AS thread, TO_CHAR(inactived, 'YY-MM-DD, HH:MIAM') AS inactived,
                          CASE WHEN tw.friend_id IS NULL THEN 'public' ELSE 'private' END AS type,
                          COALESCE((SELECT worker FROM workers WHERE id=tw.friend_id),
                                   (SELECT title FROM threads WHERE id=tw.thread_id)) AS title,
                          (SELECT COUNT(*) FROM threads_messages
                           WHERE thread_id=tw.thread_id AND created>tw.inactived) AS unread,
                          TO_CHAR(COALESCE((SELECT MAX(created) FROM threads_messages
                            WHERE thread_id=tw.thread_id), created), 'YY-MM-DD, HH:MIAM') AS last
                   FROM threads_workers tw
                   WHERE worker_id=%s AND joined IS TRUE
                   ORDER BY unread DESC, last DESC;""", (await current_user.id, ), as_dict=True)

  return await render_template('templates/chat.html', title='Chats', chats=rows)


@workers_chats.route('/workers_chats/get_chat', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_chat():
  rq = await request.form
  rows = db.all("""SELECT 'public' AS type, id, title AS name FROM threads WHERE title ILIKE %s
                   UNION ALL
                   SELECT 'private', id, worker FROM workers WHERE id<>%s AND worker ILIKE %s
                   ORDER BY name ASC LIMIT 20;""",
                ('%%%s%%' % rq['val'], await current_user.id, '%%%s%%' % rq['val']), as_dict=True)
  return dumps(rows)


@workers_chats.route('/workers_chats/set_chat', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_chat():
  rq = await request.form
  u_id = await current_user.id
  row = db.one("""SELECT thread_id, joined FROM threads_workers
                  WHERE 'public' = %s AND worker_id=%s AND thread_id=%s OR
                        'private' = %s AND worker_id=%s AND friend_id=%s;""",
               (rq['type'], u_id, rq['id'], rq['type'], u_id, rq['id']))
  if row:
    thread = row[0]
    if rq['type'] == 'block':
      db.execute("UPDATE threads_workers SET blocked=TRUE WHERE thread_id=%s AND friend_id=%s;",
                 (thread, u_id))
    elif rq['type'] == 'unjoin':
      db.execute("UPDATE threads_workers SET joined=FALSE WHERE thread_id=%s AND worker_id=%s;",
                 (thread, u_id))
    elif row[1] is False:
      db.execute("UPDATE threads_workers SET joined=TRUE WHERE thread_id=%s AND worker_id=%s;",
                 (thread, u_id))
  elif rq['type'] == 'public':
    thread = rq['id']
    db.execute("INSERT INTO threads_workers VALUES(DEFAULT, %s, %s, NULL, TRUE, FALSE, NOW(), NOW());",
               (thread, u_id))
  elif rq['type'] == 'private':
    thread = db.one("INSERT INTO threads VALUES(DEFAULT, %s, NULL, NOW()) RETURNING id;",
                    (u_id, ))[0]
    db.execute("INSERT INTO threads_workers VALUES(DEFAULT, %s, %s, %s, TRUE, FALSE, NOW(), NOW());",
               (thread, u_id, rq['id']))
    db.execute("INSERT INTO threads_workers VALUES(DEFAULT, %s, %s, %s, TRUE, FALSE, NOW(), NOW());",
               (thread, rq['id'], u_id))
  db.commit()
  return dumps({"thread": thread})


def check_joined_blocked(thread, user_id):
  rows = db.all("""SELECT id, joined, blocked, worker_id FROM threads_workers
                   WHERE thread_id=%s AND (worker_id=%s OR friend_id=%s);""",
                (thread, user_id, user_id))
  for row in rows:
    if row[1] is False:
      db.execute("UPDATE threads_workers SET joined=TRUE WHERE id=%s;", (row[0], ))
    if row[2] is True:
      if row[3] != user_id:
        db.execute("UPDATE threads_workers SET blocked=FALSE WHERE friend_id=%s;", (row[0], ))
      else:
        raise Exception('Usuario bloqueado!')
  db.commit()


@workers_chats.route('/workers_chats/set_chat_msg', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_chat_msg():
  rq = await request.form
  u_id = await current_user.id
  thread = loads(await set_chat())["thread"]  # type = private, public
  check_joined_blocked(thread, u_id)
  db.execute("INSERT INTO threads_messages VALUES (DEFAULT, %s, %s, %s, NOW());",
             (thread, u_id, rq['msg']))
  db.commit()
  return '[]'


@workers_chats.route('/workers_chats/get_chat_data', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_chat_data():
  rq = await request.form
  lines = 20
  page = int(rq['page'])
  rows = db.all("""SELECT COUNT(*) OVER(), worker_id,
                          (SELECT worker FROM workers WHERE id=tm.worker_id),
                          message, created
                   FROM threads_messages tm WHERE thread_id=%s
                   ORDER BY created DESC LIMIT %s OFFSET %s;""", (rq['thread'], lines, (page-1)*lines))
  count = rows[0][0] if rows else 0
  if rows:
    rows = [{"action":"active", "thread":rq['thread'], "msg":row[3],
             "dt":row[4].strftime("%Y-%m-%d, %-I:%M%p"),
             "worker":row[2] if row[1] != int(await current_user.id) else ""} for row in rows]
  else:
    rows = []
  return dumps({"rows":rows, "count":count, "page":page, "pages":ceil(count/lines)})  # , cls=JsonEncoder


@workers_chats.websocket('/workers_chats/chat_ws')
@ws.wrapper
async def chat_ws(key, disconnect):
  id = ws.get_id(key)
  thread = sub('_\d+$', '', id)
  user_id = sub('^.*_', '', id)
  schema = session['schema'] if 'schema' in session else 'public'
  if disconnect:
    db.connect(schema)
    db.execute("""UPDATE threads_workers SET inactived=NOW()
                  WHERE thread_id=%s AND worker_id=%s;""", (thread, user_id))
    db.commit()
    db.close()
  else:
    db.connect(schema)
    worker = db.one("SELECT worker FROM workers WHERE id=%s", (user_id, ))[0]
    db.close()
    socket = ws.get_socket(key)
    tz = current_app.config['TIMEZONE']
    # await socket.send(dumps({"action":"joined", "thread":thread, "msg":"", "worker":worker,
    #                          "dt":datetime.now(tz).strftime("%Y-%m-%d, %-I:%M%p")}))
    while True:
      data = await socket.receive()
      msg = sub('[^a-zA-Z0-9 \]\[{}()"'';:,.&|?!~#$%<>=/*+-]+', '-', escape(data))
      db.connect(schema)
      if msg != '**typing**':
        check_joined_blocked(thread, user_id)
        db.execute("INSERT INTO threads_messages VALUES (DEFAULT, %s, %s, %s, NOW());",
                   (thread, user_id, msg))
        db.commit()
      rows = db.all("""SELECT worker_id, (SELECT ARRAY_AGG(thread_id || '_' || worker_id)
                                          FROM threads_workers WHERE worker_id=tw.worker_id) AS others
                       FROM threads_workers tw WHERE thread_id=%s;""", (thread, ), as_dict=True)
      db.close()
      ids = ws.get_ids()
      for row in rows:
        for k, v in ids.items():
          a_id = '%s_%s' % (thread, row['worker_id'])
          if v == a_id and (row['worker_id'] != int(user_id) or msg != '**typing**'):
            a_socket = ws.get_socket(k)
            await a_socket.send(dumps({"action":"active", "thread":thread, "msg":unescape(msg),
                                       "dt":datetime.now(tz).strftime("%Y-%m-%d, %-I:%M%p"),
                                       "worker":worker if row['worker_id'] != int(user_id) else ""}))
          elif v in row['others'] and row['worker_id'] != int(user_id) and msg != '**typing**':
            o_socket = ws.get_socket(k)
            await o_socket.send(dumps({"action":"others", "thread":thread, "msg":"", "dt":"", "worker":""}))
