SET search_path TO :app_schema;

-- ************************************** --
------------------ TABLES ------------------
-- ************************************** --
CREATE TABLE threads (
  id SERIAL PRIMARY KEY NOT NULL,
  worker_id INTEGER NOT NULL,
  title VARCHAR(50) DEFAULT NULL,  -- NULL = private thread
  created TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  CHECK (TRIM(title) <> '' AND title !~ '[^a-zA-Z0-9 \]\[{}()"'';:,.&|?!~#$%<>=/*+-]+'),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
);
-- INSERT INTO threads VALUES(DEFAULT, 1, 'Grupo PZ', NOW());

CREATE TABLE threads_workers (
  id SERIAL PRIMARY KEY NOT NULL,
  thread_id INTEGER NOT NULL,
  worker_id INTEGER NOT NULL,
  friend_id INTEGER DEFAULT NULL,  -- NULL = public thread
  joined BOOLEAN NOT NULL,
  blocked BOOLEAN NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  inactived TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  FOREIGN KEY(thread_id) REFERENCES threads(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id),
  FOREIGN KEY(friend_id) REFERENCES workers(id)
);
-- INSERT INTO threads_workers VALUES(DEFAULT, 1, 1, NULL, TRUE, FALSE, NOW(), NOW());
-- INSERT INTO threads_workers VALUES(DEFAULT, 1, 5, NULL, TRUE, FALSE, NOW(), NOW());

CREATE TABLE threads_messages (
  id SERIAL PRIMARY KEY NOT NULL,
  thread_id INTEGER NOT NULL,
  worker_id INTEGER NOT NULL,
  message VARCHAR(150) NOT NULL,
  created TIMESTAMP WITHOUT TIME ZONE NOT NULL,
  CHECK (TRIM(message) <> '' AND message !~ '[^a-zA-Z0-9 \]\[{}()"'';:,.&|?!~#$%<>=/*+-]+'),
  FOREIGN KEY(thread_id) REFERENCES threads(id),
  FOREIGN KEY(worker_id) REFERENCES workers(id)
);

CREATE INDEX threads_messages_unread_idx ON threads_messages(
  thread_id DESC,
  created DESC
);


-- ************************************* --
------------------ VIEWS ------------------
-- ************************************* --


-- ************************************** --
------------------ QUERYS ------------------
-- ************************************** --


-- **************************************** --
------------------ FUNCTIONS -----------------
-- **************************************** --


-- **************************************** --
------------------ TRIGGERS ------------------
-- **************************************** --
