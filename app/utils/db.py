from functools import wraps
from pg_db import DB
from quart import flash, make_response, redirect, request, session, url_for
from quart_auth import current_user


class db(DB):
  def wrapper(self, func):
    @wraps(func)
    async def wrapped(*args, **kwargs):
      try:
        self.connect(session['schema'] if 'schema' in session else 'public',
                     'QUART_AUTH' in request.cookies and await current_user.readonly)
        return await func(*args, **kwargs)
      except Exception as e:
        self.rollback()
        if self.debug:
          print("db error -> " + str(e))
          # raise e
        if request.method == 'POST':
          return await make_response("db error -> " + str(e), 500)
        else:
          await flash("db error -> " + str(e))
          return redirect(url_for(request.endpoint))
      finally:
        try:
          self.close()
        except:
          pass
    return wrapped
