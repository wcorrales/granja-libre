-- db.execute(u"SELECT escape('Martín Muñoz');")
CREATE OR REPLACE FUNCTION escape(VARCHAR) RETURNS VARCHAR AS $$
DECLARE
  s VARCHAR;
BEGIN
  EXECUTE FORMAT('SELECT ''%s''',
    REGEXP_REPLACE($1, '([áéíóúüñÁÉÍÓÚÜÑ])', '&#'' || ASCII(''\1'') || '';', 'g')) INTO s;
  RETURN s;
END;
$$ LANGUAGE plpgsql;


-- db.execute(u"SELECT unescape('Mart&#237;n Mu&#241;oz');")
CREATE OR REPLACE FUNCTION unescape(VARCHAR) RETURNS VARCHAR AS $$
DECLARE
  s VARCHAR;
BEGIN
  EXECUTE FORMAT('SELECT ''%s''',
    REGEXP_REPLACE($1, '(&#\d{3};)', ''' || CHR(SUBSTR(''\1'', 3, 3)::INT) || ''', 'g')) INTO s;
  RETURN s;
END;
$$ LANGUAGE plpgsql;


-- SELECT eval_frac('1/6/33');
CREATE FUNCTION eval_frac(TEXT) RETURNS FLOAT AS $$
  WITH RECURSIVE rec(a, i, n) AS (
    SELECT x, 1, x[1]::FLOAT
    FROM STRING_TO_ARRAY($1, '/') x
  UNION ALL
    SELECT a, i+1, n::FLOAT/REGEXP_REPLACE(a[i+1], '^0$', '1')::FLOAT
    FROM rec WHERE i<ARRAY_LENGTH(a, 1)
  )
  SELECT n FROM rec ORDER BY i DESC LIMIT 1;
$$ LANGUAGE SQL;
