-- psql granjalibre -v activity='1' -f db_update.sql


SET search_path TO activity_:activity;



DROP TRIGGER g_ev_feeds_stock_delete_trigger ON g_ev_feeds_stock;
DELETE FROM g_ev_feeds_stock;
DELETE FROM pens_feeds_move;
DELETE FROM g_ev_feeds;
DELETE FROM pens_feeds;


CREATE TRIGGER g_ev_feeds_stock_delete_trigger
  AFTER DELETE ON g_ev_feeds_stock
  FOR EACH ROW EXECUTE PROCEDURE g_ev_feeds_stock_delete_function(); 



/*
UPDATE querys q SET query=t.query, defs=t.defs, title=t.title
FROM (SELECT code, query, defs, title FROM activity_1.querys) t
WHERE q.code=t.code AND
      q.code IN ('resumens_1_repro_stock_all',
                 'resumens_1_repro_stock_litters',
                 'resumens_1_repro_stock_productives',
                 'resumens_1_repro_stock_services',
                 'resumens_1_repro_stock_unproductives',
                 'resumens_2_repro_actives',
                 'resumens_2_repro_analysis',
                 'resumens_2_repro_litters',
                 'resumens_2_repro_males',
                 'resumens_2_repro_males_usage',
                 'resumens_3_produ_stock',
                 'resumens_3_produ_stock_semanas',
                 'resumens_4_produ_deaths_day',
                 'resumens_4_produ_deaths_week',
                 'resumens_4_produ_salidas',
                 'resumens_4_produ_semanal_feed',
                 'resumens_7_repeticiones',
                 'resumens_7_serv_analysis',
                 'resumens_9_partos_ubicaciones',
                 'resumens_inv_mensual',
                 'resumens_partos_nacidos',
                 'resumens_produ_movimientos',
                 'resumens_produ_salidas');


INSERT INTO querys SELECT * FROM activity_1.querys t 
WHERE t.code NOT IN(SELECT code FROM querys) AND
      t.code IN ('resumens_1_repro_stock_all',
                 'resumens_1_repro_stock_litters',
                 'resumens_1_repro_stock_productives',
                 'resumens_1_repro_stock_services',
                 'resumens_1_repro_stock_unproductives',
                 'resumens_2_repro_actives',
                 'resumens_2_repro_analysis',
                 'resumens_2_repro_litters',
                 'resumens_2_repro_males',
                 'resumens_2_repro_males_usage',
                 'resumens_3_produ_stock',
                 'resumens_3_produ_stock_semanas',
                 'resumens_4_produ_deaths_day',
                 'resumens_4_produ_deaths_week',
                 'resumens_4_produ_salidas',
                 'resumens_4_produ_semanal_feed',
                 'resumens_7_repeticiones',
                 'resumens_7_serv_analysis',
                 'resumens_9_partos_ubicaciones',
                 'resumens_inv_mensual',
                 'resumens_partos_nacidos',
                 'resumens_produ_movimientos',
                 'resumens_produ_salidas');

*/
