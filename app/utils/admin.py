from functools import wraps
from pg_db import DB
from quart import request, session  # jsonify
from quart_auth import AuthUser, Unauthorized, current_user
from re import sub

from config import AppConfig


db = DB(AppConfig.DB_NAME, AppConfig.DB_USER, AppConfig.DEBUG)


class User(AuthUser):
  _data = None

  def set_data(self):
    if self._data is None: 
      schema = session['schema'] if 'schema' in session else 'public'
      db.connect(schema)
      row = db.one("SELECT worker, admin, readonly, access FROM workers WHERE id=%s",
                   (self.auth_id, ), as_dict=True)
      db.close()
      self._data = row if row else {"worker":'', "admin":False, "readonly":False, "access":[]}

  @property
  async def id(self):
    return self.auth_id

  @property
  async def worker(self):
    self.set_data()
    return self._data['worker']

  @property
  async def admin(self):
    self.set_data()
    return self._data['admin']

  @property
  async def readonly(self):
    self.set_data()
    return self._data['readonly']

  @property
  async def access(self):
    self.set_data()
    return self._data['access']


def check_admin(func):
  @wraps(func)
  async def wrapper(*args, **kwargs):
    if not await current_user.admin:
      # return jsonify(msg='Admins only!'), 403
      raise Unauthorized()
    return await func(*args, **kwargs)
  return wrapper


def check_user(func):
  @wraps(func)
  async def wrapper(*args, **kwargs):
    # schema = session['schema'] if 'schema' in session else 'public'
    # if schema != 'public' and not await current_user.admin and \
    if not await current_user.admin and sub('_pdf', '', request.path) not in await current_user.access:  # '_pdf|_subquery'
      # return jsonify(msg='Not authorized!'), 403
      raise Unauthorized()
    return await func(*args, **kwargs)
  return wrapper
