# coding=utf-8
from datetime import date, datetime
from decimal import Decimal, ROUND_HALF_UP
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from json import JSONEncoder, loads
from math import ceil
from re import sub
from smtplib import SMTP

from config import AppConfig


def round_half_up(n, decimals=0):
  return Decimal(str(n)).quantize(Decimal(10) ** -decimals, rounding=ROUND_HALF_UP)


def escape(s):
  return sub(r'([áéíóúüñÁÉÍÓÚÜÑ])', lambda x: '&#%s;' % ord(x.group()), s or '')


def unescape(s):
  return sub(r'(&#\d{3};)', lambda x: chr(int(x.group()[2:-1])), s or '')


def sql_ts(x):
  return datetime.strptime(x, "%Y-%m-%d").strftime("%s")


class JsonEncoder(JSONEncoder):
  def default(self, obj):
    if isinstance(obj, Decimal): return float(obj)
    elif isinstance(obj, date): return str(obj)
    elif isinstance(obj, datetime): return obj.isoformat()
    return JSONEncoder.default(self, obj)


def paginate(db, rq, sql, as_dict=False):
  parameters = ()
  sql = sub(r'(?!\w|\))\s(?!\w|[\(\*])', r'', sql).strip()
  db.execute(sql + " LIMIT 1;")
  cols = db.description()
  for k, v in loads(rq['filter']).items():
    if v != '':
      sql = sql + (' AND ' if len(parameters) else ' WHERE ') + cols[int(k)] + '::TEXT ILIKE %s'
      parameters = parameters + ('%%%s%%' % v, )
  if rq['sort_dir'].lower() not in ('asc', 'desc'):
    raise Exception('bad sort direction!')
  sql = sql + (" ORDER BY %s %s" % (cols[int(rq['sort_col'])], rq['sort_dir']))
  sql = sql + (" LIMIT %s OFFSET %s;" % (int(rq['lines']), (int(rq['page'])-1)*int(rq['lines'])))
  rs = {"count":0, "lines": rq['lines'], "page": rq['page'], "pages": 1, "rows":[]}
  try:
    rows = db.all(sub('^SELECT', 'SELECT COUNT(*) OVER(),', sql), parameters, as_dict=as_dict)
    rs['count'] = rows[0]['count' if as_dict else 0]
    rs['pages'] = ceil(rs['count'] / rq['lines'])
    rs['rows'] = rows if as_dict else [row[1:] for row in rows]
  except Exception as e:
    pass
  return rs


def send_email(to, title, txt):
  msg = MIMEMultipart()
  msg['Subject'] = title
  msg['From'] = AppConfig.EMAIL
  msg['To'] = to
  msg['Date'] = datetime.now(AppConfig.TIMEZONE).strftime("%a, %d %b %Y %H:%M:%S %z")
  msg.attach(MIMEText(txt, 'plain'))
  # s = SMTP('localhost')
  s = SMTP(AppConfig.EMAIL_SERVER, int(AppConfig.EMAIL_SERVER_PORT))
  s.starttls()
  s.login(AppConfig.EMAIL_LOGIN, AppConfig.EMAIL_PASSWORD)
  s.send_message(msg)
  s.quit()
