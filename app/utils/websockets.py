from functools import wraps
from hashlib import sha512
from itsdangerous import BadSignature, URLSafeSerializer
from quart import current_app, websocket


class _AuthSerializer(URLSafeSerializer):
  def __init__(self, secret: str, salt: str) -> None:
    super().__init__(secret, salt, signer_kwargs={"digest_method": sha512})


class websockets:
  ids = {}
  sockets = {}

  def get_id(self, key):
    return self.ids[key] if key in self.ids else None

  def get_ids(self):
    return self.ids

  def get_key(self):
    for k in sorted(self.ids.keys(), reverse=True):
      if self.ids[k] is None: del self.ids[k]
      else: break
    return len(self.ids)

  def get_socket(self, key):
    return self.sockets[key] if key in self.sockets else None

  def get_cookie(self):
    try:
      token = websocket.cookies[current_app.config['QUART_AUTH_COOKIE_NAME']]
    except KeyError:
      return None
    else:
      serializer = _AuthSerializer(current_app.config['SECRET_KEY'], current_app.config['QUART_AUTH_SALT'])
      try:
        return serializer.loads(token)
      except BadSignature:
        return None

  def wrapper(self, func):
    @wraps(func)
    async def wrapped(*args, **kwargs):
      key = self.get_key()
      self.ids[key] = '%s_%s' % (websocket.args.get('thread'), self.get_cookie())
      self.sockets[key] = websocket._get_current_object()
      try:
        return await func(key, False, *args, **kwargs)
      except Exception as e:
        pass
      finally:
        await func(key, True, *args, **kwargs)
        self.ids[key] = None
        self.sockets[key] = None
    return wrapped
