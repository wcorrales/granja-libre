from jinja2 import Markup
from json import loads
from urllib import request as http
from werkzeug.urls import url_encode


class DEFAULTS(object):
    IS_ENABLED = True
    THEME = "light"
    TYPE = "image"
    SIZE = "normal"
    TABINDEX = 0
    VERIFY_URL = "https://www.google.com/recaptcha/api/siteverify"
    API_URL = "//www.google.com/recaptcha/api.js"
    DIV_CLASS = "g-recaptcha"


class XCaptcha(object):
    def __init__(self,
        app=None,
        site_key=None,
        secret_key=None,
        is_enabled=DEFAULTS.IS_ENABLED,
        theme=DEFAULTS.THEME,
        xtype=DEFAULTS.TYPE,
        size=DEFAULTS.SIZE,
        tabindex=DEFAULTS.TABINDEX,
        verify_url=DEFAULTS.VERIFY_URL,
        api_url=DEFAULTS.API_URL,
        div_class=DEFAULTS.DIV_CLASS,
        **kwargs
    ):
        self.site_key = site_key
        self.secret_key = secret_key
        self.is_enabled = is_enabled
        self.theme = theme
        self.type = xtype
        self.size = size
        self.tabindex = tabindex
        self.verify_url = verify_url
        self.api_url = api_url
        self.div_class = div_class

    def init_app(self, app):
        self.site_key = app.config['RECAPTCHA_PUBLIC_KEY']
        self.secret_key = app.config['RECAPTCHA_PRIVATE_KEY']

        @app.context_processor
        def get_code():
            return dict(xcaptcha=Markup(self.get_code()))

    def get_code(self):
        """
        Returns the new XCaptcha code
        :return:
        """
        return "" if not self.is_enabled else ("""
        <script src='{API_URL}'></script>
        <div class="{DIV_CLASS}" data-sitekey="{SITE_KEY}" data-theme="{THEME}" data-type="{TYPE}" data-size="{SIZE}"\
         data-tabindex="{TABINDEX}"></div>
        """.format(
                DIV_CLASS=self.div_class,
                API_URL=self.api_url,
                SITE_KEY=self.site_key,
                THEME=self.theme,
                TYPE=self.type,
                SIZE=self.size,
                TABINDEX=self.tabindex
            )
        )

    def verify(self, form, remote_ip):
        if self.is_enabled:
            data = url_encode({
                'secret':     self.secret_key,
                'remoteip':   remote_ip,
                'response':   form.get('{}-response'.format(self.div_class))
            })
            http_response = http.urlopen(self.verify_url, data.encode())
            if http_response.code == 200:
                json_resp = loads(http_response.read().decode())
                return json_resp["success"]
            else:
                return False

        return True
