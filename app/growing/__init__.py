from quart import Blueprint

growing = Blueprint('growing', __name__, template_folder='')

from . import views
from .views import get_growing_menu
