-- psql granjalibre -v activity='1' -f db_ajustes.sql


SET search_path TO activity_:activity;


SELECT COUNT(*) FROM groups
WHERE NOT EXISTS(SELECT 1 FROM groups_events WHERE group_id=groups.id);


-- 1 pen con mas de un grupo
SELECT pen_id, ARRAY_AGG(id), ARRAY_AGG(animals)
FROM (SELECT g.id,
             (SELECT pen_id FROM g_ev_stock WHERE group_id=g.id ORDER BY id DESC LIMIT 1),
             (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock WHERE group_id=g.id) AS animals
      FROM groups g WHERE date_final IS NULL) t
GROUP BY pen_id HAVING COUNT(*)>1;


-- UPDATE groups SET date_final='2019-11-18' WHERE id=832;




-- 1 grupo con mas de 2 pens

  SELECT group_id,
         (SELECT MIN(date) FROM g_ev_stock WHERE group_id=e.group_id AND pen_id=e.pen_id)
  FROM g_ev_stock e
  WHERE (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock
         WHERE group_id=e.group_id AND pen_id=e.pen_id) > 0
--        AND pen_id=102
  ORDER BY id DESC LIMIT 1;


  SELECT * FROM g_ev_stock WHERE group_id=927;

  SELECT SUM(ingress-egress-deaths) FROM g_ev_stock WHERE group_id=927 and pen_id=187;




-- UPDATE egress_weight
SELECT date, galeron, SUM(egress), AVG(egress_weight)
FROM g_ev_stock s,
LATERAL(SELECT pen AS corral,
               (SELECT pen FROM pens
                WHERE rgt>(lft+1) AND lft<p.lft AND rgt>p.rgt
                ORDER BY lft DESC LIMIT 1) AS galeron
        FROM pens p WHERE id=s.pen_id) p
WHERE date>='2021-01-01' AND egress>0 AND death_id IS NULL AND
      NOT EXISTS(SELECT id FROM g_ev_stock_move WHERE id_of=s.id)
GROUP BY date, galeron;

UPDATE g_ev_stock s SET egress_weight=103.3
WHERE date='2021-02-04' AND egress>0 AND death_id IS NULL AND
      NOT EXISTS(SELECT id FROM g_ev_stock_move WHERE id_of=s.id);
