from datetime import datetime
from json import dumps
from quart import current_app, make_response, request, render_template, url_for
from quart_auth import current_user, login_required

from app import db
from . import growing
from ..notebooks.views import get_notebooks_menu
from ..pgvega.views import get_reports_menu
from ..pgvega.report_utils import set_table
from ..utils.admin import check_user
from ..utils.utils import sql_ts, JsonEncoder  # escape, unescape,


def get_growing_menu(u_admin, u_access):
  admin = [{"url": "growing.growing_parameters", "name": "Parametros"}] if u_admin else []
  forms = filter(lambda x: u_admin or url_for(x['url']) in u_access,
                 [{"url": "growing.growing_events", "name": "Eventos"},
                  {"url": "growing.growing_inventory", "name": "Inventarios"}]) 
  reports = filter(lambda x: url_for('pgvega.get_report', code=x['code']) in u_access,
                   get_reports_menu('^growing_')) 
  notebooks = filter(lambda x: url_for('notebooks.get_notebook', notebook_id=x['id']) in u_access,
                     get_notebooks_menu('^growing_')) 
  return {"code": "production", "title": "Produccion",
          "forms": list(forms)+admin, "reports": list(reports), "notebooks": list(notebooks)}


#################################### EVENTS #########################################
@growing.route('/growing/growing_events')
@login_required
@check_user
@db.wrapper
async def growing_events():
  return await render_template('templates/growing_events.html', title="Produccion Eventos",
                               feeds=dumps(db.all("SELECT feed FROM feeds;"), cls=JsonEncoder),
                               deaths=dumps(db.all("SELECT death FROM deaths WHERE type='young' AND active=1;"),
                                            cls=JsonEncoder))


@growing.route('/growing/get_pens_events', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_pens_events():
  return dumps(db.all("""
    WITH
      pens_paths AS (
        SELECT n.id, n.pen, STRING_AGG(p.pen, '_') AS path,
               n.pen='0' OR n.lft<>(n.rgt-1) AS parent
        FROM pens n, (SELECT * FROM pens ORDER BY lft) p
        WHERE n.lft BETWEEN p.lft AND p.rgt GROUP BY n.id ORDER BY n.lft),
      groups_inventory AS (
        SELECT (SELECT pen_id FROM g_ev_stock WHERE group_id=g.id ORDER BY id DESC LIMIT 1),
               (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock WHERE group_id=g.id) AS animals
        FROM groups g WHERE date_final IS NULL),
      feed_inventory AS (
        SELECT pen_id,
               (SELECT feed FROM feeds WHERE id=pf.feed_id) AS feed,
               SUM(ingress-egress) AS count
        FROM pens_feeds pf WHERE EXISTS(SELECT id FROM pens WHERE pen='0' AND id=pf.pen_id)
        GROUP BY pen_id, feed_id HAVING SUM(ingress-egress)<>0)
    SELECT id, pen, path, parent, pen='0' AS feed,
           COALESCE((SELECT animals FROM groups_inventory WHERE pen_id=pp.id), 0)
    FROM pens_paths pp
    UNION ALL
    SELECT 0, fi.feed, CONCAT_WS('_', pp.path, fi.feed), FALSE, TRUE, fi.count
    FROM feed_inventory fi JOIN pens_paths pp ON fi.pen_id=pp.id;"""), cls=JsonEncoder)


############################### PENS INVENTORY EVENTS ###############################
def insert_pen_feed(rq):
  db.execute("""INSERT INTO pens_feeds
                VALUES(DEFAULT, %s, (SELECT id FROM feeds WHERE feed=%s), %s, %s, 0);""",
             (rq['target_pen'], rq['feed'], rq['date'], rq['units']))


def insert_feed_move(rq):
  if rq['date'] > datetime.now(current_app.config['TIMEZONE']).strftime("%Y-%m-%d"):
    raise Exception("error: fecha mayor a fecha actual!")
  if int(rq['units']) <= 0:
    raise Exception("error: traslado debe ser mayor a 0!")
  inventory = db.one("""
    SELECT SUM(ingress-egress), MAX(date) FROM pens_feeds
    WHERE pen_id=%s AND feed_id=(SELECT id FROM feeds WHERE feed=%s);""", 
    (rq['source_pen'], rq['feed']))
  if int(rq['units']) > inventory[0]:
    raise Exception("error: traslado mayor a inventario disponible!")
  if rq['date'] < str(inventory[1]):
    raise Exception("error: fecha anterior a inventario disponible!")
  source_feed_id = db.one("""
    INSERT INTO pens_feeds
    VALUES(DEFAULT, %s, (SELECT id FROM feeds WHERE feed=%s), %s, 0, %s) RETURNING id;""",
    (rq['source_pen'], rq['feed'], rq['date'], rq['units']))[0]
  target_feed_id = db.one("""
    INSERT INTO pens_feeds
    VALUES(DEFAULT, %s, (SELECT id FROM feeds WHERE feed=%s), %s, %s, 0) RETURNING id;""",
    (rq['target_pen'], rq['feed'], rq['date'], rq['units']))[0]
  db.execute("INSERT INTO pens_feeds_move VALUES(DEFAULT, %s, %s, %s, %s);",
             (source_feed_id, rq['source_pen'], target_feed_id, rq['target_pen']))


def insert_group_feed(rq):
  if rq['date'] > datetime.now(current_app.config['TIMEZONE']).strftime("%Y-%m-%d"):
    raise Exception("error: fecha mayor a fecha actual!")
  if int(rq['units']) <= 0:
    raise Exception("error: traslado debe ser mayor a 0!")
  inventory = db.one("""
    SELECT SUM(ingress-egress), MAX(date) FROM pens_feeds
    WHERE pen_id=%s AND feed_id=(SELECT id FROM feeds WHERE feed=%s);""", 
    (rq['source_pen'], rq['feed']))
  if int(rq['units']) > inventory[0]:
    raise Exception("error: traslado mayor a inventario disponible!")
  if rq['date'] < str(inventory[1]):
    raise Exception("error: fecha anterior a inventario disponible!")
  target_group = db.one("""
    SELECT group_id FROM g_ev_stock e
    WHERE pen_id=%s AND
          (SELECT MIN(date) FROM g_ev_stock
           WHERE group_id=e.group_id AND pen_id=e.pen_id) <= %s AND
          (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock
           WHERE group_id=e.group_id AND pen_id=e.pen_id) > 0
    ORDER BY id DESC LIMIT 1""", (rq['target_pen'], rq['date']))
  if not target_group:
    raise Exception("error: fecha inicial en corral o corral sin animales!")
  source_feed_id = db.one("""
    INSERT INTO pens_feeds
    VALUES(DEFAULT, %s, (SELECT id FROM feeds WHERE feed=%s), %s, 0, %s) RETURNING id;""",
    (rq['source_pen'], rq['feed'], rq['date'], rq['units']))[0]
  target_feed_id = db.one("""
    INSERT INTO pens_feeds
    VALUES(DEFAULT, %s, (SELECT id FROM feeds WHERE feed=%s), %s, %s, 0) RETURNING id;""",
    (rq['target_pen'], rq['feed'], rq['date'], rq['units']))[0]
  db.execute("INSERT INTO g_ev_feeds VALUES(%s, %s, %s, %s);",
             (target_feed_id, target_group[0], rq['target_pen'], rq['date']))
  db.execute("INSERT INTO pens_feeds_move VALUES(DEFAULT, %s, %s, %s, %s);",
             (source_feed_id, rq['source_pen'], target_feed_id, rq['target_pen']))


def insert_pen_wean(rq):
  if rq['date'] > datetime.now(current_app.config['TIMEZONE']).strftime("%Y-%m-%d"):
    raise Exception("error: fecha mayor a fecha actual!")
  if rq['birth'] == '' or rq['birth'] > rq['date']:
    raise Exception("error: fecha parto!")
  target_group = db.one("""
    SELECT group_id,
           (SELECT MIN(date) FROM g_ev_stock WHERE group_id=e.group_id AND pen_id=e.pen_id)
    FROM g_ev_stock e
    WHERE pen_id=%s AND
          (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock
           WHERE group_id=e.group_id AND pen_id=e.pen_id) > 0
    ORDER BY id DESC LIMIT 1""", (rq['target_pen'], ))
  if target_group and str(target_group[1]) > rq['date']:
    raise Exception("error: fecha inicial en corral mayor a fecha evento!")
  if not target_group:
    target_group = db.one("INSERT INTO groups VALUES(DEFAULT, %s, NULL) RETURNING id;",
                          (rq['date'], ))
  db.execute("INSERT INTO g_ev_stock VALUES(DEFAULT, %s, %s, %s, %s, 0, 0, NULL, %s, %s, NULL);",
    (target_group[0], rq['target_pen'], rq['date'], rq['units'], rq['birth'], rq['date']))


def insert_group_move(rq):
  if rq['date'] > datetime.now(current_app.config['TIMEZONE']).strftime("%Y-%m-%d"):
    raise Exception("error: fecha mayor a fecha actual!")
  count = int(rq['units'])
  if count <= 0:
    raise Exception("error: traslado debe ser mayor a 0!")
  source_data = db.one("""
    SELECT group_id,
           (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock WHERE group_id=e.group_id),
           (SELECT MAX(date) FROM g_ev_stock WHERE group_id=e.group_id),
           (SELECT CURRENT_DATE - ROUND(SUM((ingress - egress - deaths) *
                                            (CURRENT_DATE - date_birth))::NUMERIC /
                                        SUM(ingress - egress - deaths)::NUMERIC)::INTEGER
            FROM g_ev_stock WHERE group_id=e.group_id) AS balanced_birth,
           (SELECT CURRENT_DATE - ROUND(SUM((ingress - egress - deaths) *
                                            (CURRENT_DATE - date_wean))::NUMERIC /
                                        SUM(ingress - egress - deaths)::NUMERIC)::INTEGER
            FROM g_ev_stock WHERE group_id=e.group_id) AS balanced_wean
    FROM g_ev_stock e WHERE pen_id=%s ORDER BY id DESC LIMIT 1""", (rq['source_pen'], ))
  if count > source_data[1]:
    raise Exception("error: traslado mayor a inventario disponible!")
  if rq['date'] < str(source_data[2]):
    raise Exception("error: fecha anterior a inventario disponible!")
  target_group = db.one("""
    SELECT group_id,
           (SELECT MIN(date) FROM g_ev_stock WHERE group_id=e.group_id AND pen_id=e.pen_id)
    FROM g_ev_stock e
    WHERE pen_id=%s AND
          (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock
           WHERE group_id=e.group_id AND pen_id=e.pen_id) > 0
          -- AND EXISTS(SELECT id FROM groups WHERE date_final IS NULL AND id=e.group_id)
    ORDER BY id DESC LIMIT 1""", (rq['target_pen'], ))
  if target_group and str(target_group[1]) > rq['date']:
    raise Exception("error: fecha inicial en corral mayor a fecha evento!")
  source_id = db.one("""
    INSERT INTO g_ev_stock VALUES(DEFAULT, %s, %s, %s, 0, %s, 0, NULL, %s, %s, NULL) RETURNING id;""",
    (source_data[0], rq['source_pen'], rq['date'], count, source_data[3], source_data[4]))[0]
  if source_data[1] == count and not target_group:
    target_id = db.one("""
      INSERT INTO g_ev_stock VALUES(DEFAULT, %s, %s, %s, %s, 0, 0, NULL, %s, %s, NULL) RETURNING id;""",
      (source_data[0], rq['target_pen'], rq['date'], count, source_data[3], source_data[4]))[0]
  else:
    if source_data[1] == count:
      db.execute("UPDATE groups SET date_final=%s WHERE id=%s;", (rq['date'], source_data[0]))
    if not target_group:
      target_group = db.one("INSERT INTO groups VALUES(DEFAULT, %s, NULL) RETURNING id;",
                            (rq['date'], ))
    target_id = db.one("""
      INSERT INTO g_ev_stock VALUES(DEFAULT, %s, %s, %s, %s, 0, 0, NULL, %s, %s, NULL) RETURNING id;""",
      (target_group[0], rq['target_pen'], rq['date'], count, source_data[3], source_data[4]))[0]
    # feed_data = db.all("""
    #   SELECT feed_id, SUM(ingress-egress) FROM pens_feeds pf
    #   WHERE EXISTS(SELECT id FROM g_ev_feeds WHERE id=pf.id AND group_id=%s)
    #   GROUP BY feed_id;""", (source_data[0], ))
    # for row in feed_data:
    #   total = row[1] if source_data[1] == count else row[1]*count/source_data[1]
    #   source_feed_id = db.one("""
    #     INSERT INTO pens_feeds VALUES(DEFAULT, %s, %s, %s, 0, %s) RETURNING id;""",
    #     (rq['source_pen'], row[0], rq['date'], total))[0]
    #   db.execute("INSERT INTO g_ev_feeds VALUES(%s, %s, %s, %s);",
    #              (source_feed_id, source_data[0], rq['source_pen'], rq['date']))
    #   db.execute("INSERT INTO g_ev_feeds_stock VALUES(%s, %s);",
    #              (source_feed_id, source_id))
    #   target_feed_id = db.one("""
    #     INSERT INTO pens_feeds VALUES(DEFAULT, %s, %s, %s, %s, 0) RETURNING id;""",
    #     (rq['target_pen'], row[0], rq['date'], total))[0]
    #   db.execute("INSERT INTO g_ev_feeds VALUES(%s, %s, %s, %s);",
    #              (target_feed_id, target_group[0], rq['target_pen'], rq['date']))
    #   db.execute("INSERT INTO g_ev_feeds_stock VALUES(%s, %s);",
    #              (target_feed_id, target_id))
    #   db.execute("INSERT INTO pens_feeds_move VALUES(DEFAULT, %s, %s, %s, %s);",
    #              (source_feed_id, rq['source_pen'], target_feed_id, rq['target_pen']))
  db.execute("INSERT INTO g_ev_stock_move VALUES(DEFAULT, %s, %s);", (source_id, target_id))


def insert_pen_sale(rq):
  if rq['date'] > datetime.now(current_app.config['TIMEZONE']).strftime("%Y-%m-%d"):
    raise Exception("error: fecha mayor a fecha actual!")
  count = int(rq['units'])
  if count <= 0:
    raise Exception("error: salida debe ser mayor a 0!")
  source_data = db.one("""
    SELECT group_id,
           (SELECT MAX(date) FROM groups_events WHERE group_id=e.group_id AND pen_id=e.pen_id),
           (SELECT MIN(date) FROM g_ev_stock WHERE group_id=e.group_id AND pen_id=e.pen_id),
           (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock WHERE group_id=e.group_id),
           (SELECT CURRENT_DATE - ROUND(SUM((ingress - egress - deaths) *
                                            (CURRENT_DATE - date_birth))::NUMERIC /
                                        SUM(ingress - egress - deaths)::NUMERIC)::INTEGER
            FROM g_ev_stock WHERE group_id=e.group_id) AS balanced_birth,
           (SELECT CURRENT_DATE - ROUND(SUM((ingress - egress - deaths) *
                                            (CURRENT_DATE - date_wean))::NUMERIC /
                                        SUM(ingress - egress - deaths)::NUMERIC)::INTEGER
            FROM g_ev_stock WHERE group_id=e.group_id) AS balanced_wean
    FROM g_ev_stock e WHERE pen_id=%s ORDER BY id DESC LIMIT 1""", (rq['source_pen'], ))
  if str(source_data[2]) > rq['date']:
    raise Exception("error: fecha inicial en corral mayor a fecha evento!")
  if source_data[3] < count:
    raise Exception("error: salida mayor a inventario disponible!")
  if source_data[3] == count and str(source_data[1]) > rq['date']:
    raise Exception("error: salida anterior a ultimo evento!")
  source_id = db.one("""
    INSERT INTO g_ev_stock VALUES(DEFAULT, %s, %s, %s, 0, %s, 0, NULL, %s, %s, %s) RETURNING id;""",
    (source_data[0], rq['source_pen'], rq['date'], count, source_data[4], source_data[5],
     None if rq['weight'] == '' else rq['weight']))[0]
  # feed_data = db.all("""
  #   SELECT feed_id, SUM(ingress-egress) FROM pens_feeds pf
  #   WHERE EXISTS(SELECT id FROM g_ev_feeds WHERE id=pf.id AND group_id=%s)
  #   GROUP BY feed_id;""", (source_data[0], ))
  # for row in feed_data:
  #   total = row[1] if source_data[3] == count else row[1]*count/source_data[3]
  #   source_feed_id = db.one("""
  #     INSERT INTO pens_feeds VALUES(DEFAULT, %s, %s, %s, 0, %s) RETURNING id;""",
  #     (rq['source_pen'], row[0], rq['date'], total))[0]
  #   db.execute("INSERT INTO g_ev_feeds VALUES(%s, %s, %s, %s);",
  #              (source_feed_id, source_data[0], rq['source_pen'], rq['date']))
  #   db.execute("INSERT INTO g_ev_feeds_stock VALUES(%s, %s);",
  #              (source_feed_id, source_id))
  if source_data[3] == count:
    db.execute("UPDATE groups SET date_final=%s WHERE id=%s;", (rq['date'], source_data[0]))


def insert_pen_death(rq):
  if rq['date'] > datetime.now(current_app.config['TIMEZONE']).strftime("%Y-%m-%d"):
    raise Exception("error: fecha mayor a fecha actual!")
  count = int(rq['units'])
  if count <= 0:
    raise Exception("error: muertos debe ser mayor a 0!")
  death = db.one("SELECT id FROM deaths WHERE death=%s", (rq['death'], ))
  if not death:
    raise Exception("error: ingrese una muerte valida!")
  source_data = db.one("""
    SELECT group_id,
           (SELECT MAX(date) FROM groups_events WHERE group_id=e.group_id AND pen_id=e.pen_id),
           (SELECT MIN(date) FROM g_ev_stock WHERE group_id=e.group_id AND pen_id=e.pen_id),
           (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock WHERE group_id=e.group_id),
           (SELECT CURRENT_DATE - ROUND(SUM((ingress - egress - deaths) *
                                            (CURRENT_DATE - date_birth))::NUMERIC /
                                        SUM(ingress - egress - deaths)::NUMERIC)::INTEGER
            FROM g_ev_stock WHERE group_id=e.group_id) AS balanced_birth,
           (SELECT CURRENT_DATE - ROUND(SUM((ingress - egress - deaths) *
                                            (CURRENT_DATE - date_wean))::NUMERIC /
                                        SUM(ingress - egress - deaths)::NUMERIC)::INTEGER
            FROM g_ev_stock WHERE group_id=e.group_id) AS balanced_wean
    FROM g_ev_stock e WHERE pen_id=%s ORDER BY id DESC LIMIT 1""", (rq['source_pen'], ))
  if str(source_data[2]) > rq['date']:
    raise Exception("error: fecha inicial en corral mayor a fecha evento!")
  if source_data[3] < count:
    raise Exception("error: muertos mayor a inventario disponible!")
  if source_data[3] == count and str(source_data[1]) > rq['date']:
    raise Exception("error: salida anterior a ultimo evento!")
  source_id = db.one("""
    INSERT INTO g_ev_stock VALUES(DEFAULT, %s, %s, %s, 0, 0, %s, %s, %s, %s, NULL) RETURNING id;""",
    (source_data[0],rq['source_pen'],rq['date'], count, death[0], source_data[4], source_data[5]))[0]
  # feed_data = db.all("""
  #   SELECT feed_id, SUM(ingress-egress) FROM pens_feeds pf
  #   WHERE EXISTS(SELECT id FROM g_ev_feeds WHERE id=pf.id AND group_id=%s)
  #   GROUP BY feed_id;""", (source_data[0], ))
  # for row in feed_data:
  #   total = row[1] if source_data[3] == count else row[1]*count/source_data[3]
  #   source_feed_id = db.one("""
  #     INSERT INTO pens_feeds VALUES(DEFAULT, %s, %s, %s, 0, %s) RETURNING id;""",
  #     (rq['source_pen'], row[0], rq['date'], total))[0]
  #   db.execute("INSERT INTO g_ev_feeds VALUES(%s, %s, %s, %s);",
  #              (source_feed_id, source_data[0], rq['source_pen'], rq['date']))
  #   db.execute("INSERT INTO g_ev_feeds_stock VALUES(%s, %s);",
  #              (source_feed_id, source_id))
  if source_data[3] == count:
    db.execute("UPDATE groups SET date_final=%s WHERE id=%s;", (rq['date'], source_data[0]))


def insert_pen_disease(rq):
  if rq['date'] > datetime.now(current_app.config['TIMEZONE']).strftime("%Y-%m-%d"):
    raise Exception("error: fecha mayor a fecha actual!")
  count = int(rq['units'])
  if count <= 0:
    raise Exception("error: enfermos debe ser mayor a 0!")
  source_data = db.one("""
    SELECT group_id,
           (SELECT MIN(date) FROM g_ev_stock WHERE group_id=e.group_id AND pen_id=e.pen_id),
           (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock WHERE group_id=e.group_id)
    FROM g_ev_stock e WHERE pen_id=%s ORDER BY id DESC LIMIT 1""", (rq['source_pen'], ))
  if str(source_data[1]) > rq['date']:
    raise Exception("error: fecha inicial en corral mayor a fecha evento!")
  if source_data[2] < count:
    raise Exception("error: enfermos mayor a inventario disponible!")
  db.execute("INSERT INTO g_ev_diseases VALUES(DEFAULT, %s, %s, %s, %s, %s, %s);",
    (source_data[0], rq['source_pen'], rq['date'], count, rq['disease'], rq['medication']))


def insert_pen_weight(rq):
  if rq['date'] > datetime.now(current_app.config['TIMEZONE']).strftime("%Y-%m-%d"):
    raise Exception("error: fecha mayor a fecha actual!")
  count = int(rq['units'])
  if count <= 0:
    raise Exception("error: enfermos debe ser mayor a 0!")
  source_data = db.one("""
    SELECT group_id,
           (SELECT MIN(date) FROM g_ev_stock WHERE group_id=e.group_id AND pen_id=e.pen_id),
           (SELECT SUM(ingress-egress-deaths) FROM g_ev_stock WHERE group_id=e.group_id)
    FROM g_ev_stock e WHERE pen_id=%s ORDER BY id DESC LIMIT 1""", (rq['source_pen'], ))
  if str(source_data[1]) > rq['date']:
    raise Exception("error: fecha inicial en corral mayor a fecha evento!")
  if source_data[2] < count:
    raise Exception("error: pesados mayor a inventario disponible!")
  db.execute("INSERT INTO g_ev_weights VALUES(DEFAULT, %s, %s, %s, %s, %s);",
             (source_data[0], rq['source_pen'], rq['date'], count, rq['weight']))


def insert_pen_note(rq):
  if rq['date'] > datetime.now(current_app.config['TIMEZONE']).strftime("%Y-%m-%d"):
    raise Exception("error: fecha mayor a fecha actual!")
  source_data = db.one("""
    SELECT group_id,
           (SELECT MIN(date) FROM g_ev_stock WHERE group_id=e.group_id AND pen_id=e.pen_id)
    FROM g_ev_stock e WHERE pen_id=%s ORDER BY id DESC LIMIT 1""", (rq['source_pen'], ))
  if str(source_data[1]) > rq['date']:
    raise Exception("error: fecha inicial en corral mayor a fecha evento!")
  db.execute("INSERT INTO g_ev_notes VALUES(DEFAULT, %s, %s, %s, %s);",
             (source_data[0], rq['source_pen'], rq['date'], rq['note']))


@growing.route('/growing/set_pens_events', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_pens_events():
  rq = await request.form
  try:
    {"insert_pen_feed": lambda x: insert_pen_feed(x),
     "insert_feed_move": lambda x: insert_feed_move(x),
     "insert_group_feed": lambda x: insert_group_feed(x),
     "insert_pen_wean": lambda x: insert_pen_wean(x),
     "insert_group_move": lambda x: insert_group_move(x),
     "insert_pen_sale": lambda x: insert_pen_sale(x),
     "insert_pen_death": lambda x: insert_pen_death(x),
     "insert_pen_disease": lambda x: insert_pen_disease(x),
     "insert_pen_weight": lambda x: insert_pen_weight(x),
     "insert_pen_note": lambda x: insert_pen_note(x)}[rq['action']](rq)
    db.commit()
  except Exception as e:
    return await make_response("growing error -> " + str(e), 500)
  return await get_pens_events()



################################ INVENTORY FEED #####################################
def get_feeds(rq):
  defs = [{"head":"Id", "foot":"count", "class":"strong"},
          {"head":"Bodega", "class":"feed"},
          {"head":"Alimento"},
          {"head":"Inicial", "foot":"sum"},
          {"head":"Ingresos", "foot":"sum"},
          {"head":"Salidas", "foot":"sum"},
          {"head":"Final", "foot":"sum"}]
  rows = db.all("""
    WITH
      feed_inventory AS (
        SELECT pen_id,
               (SELECT feed FROM feeds WHERE id=pf.feed_id) AS feed,
               SUM(CASE WHEN date<%s THEN ingress-egress ELSE 0 END) AS initial,
               SUM(CASE WHEN date>=%s THEN ingress ELSE 0 END) AS ingress,
               SUM(CASE WHEN date>=%s THEN egress ELSE 0 END) AS egress
        FROM pens_feeds pf
        WHERE date<=%s AND EXISTS(SELECT id FROM pens WHERE pen='0' AND id=pf.pen_id)
        GROUP BY pen_id, feed_id)
    SELECT p.id,
           (SELECT pen FROM pens WHERE rgt>(lft+1) AND lft<p.lft AND rgt>p.rgt
            ORDER BY lft DESC LIMIT 1) AS house,
           f.feed, f.initial, f.ingress, f.egress, f.initial + f.ingress - f.egress
    FROM pens p JOIN feed_inventory f ON p.id=f.pen_id
    ORDER BY house, feed;""", (rq['d1'], rq['d1'], rq['d1'], rq['d2']))
  return dumps(set_table(defs, rows), cls=JsonEncoder)


def get_feed_history(rq):
  return dumps(db.all("""
    SELECT id, date, ingress, egress,
           CASE WHEN ingress > 0 THEN
             COALESCE(
               (SELECT (SELECT pen FROM pens WHERE rgt>(lft+1) AND lft<p.lft AND rgt>p.rgt
                        ORDER BY lft DESC LIMIT 1)
                FROM pens_feeds_move m JOIN pens p ON m.pen_of=p.id
                WHERE id_to=pf.id),
                'Fabrica'
             )
           ELSE
             COALESCE(
               (SELECT (SELECT pen FROM pens WHERE rgt>(lft+1) AND lft<p.lft AND rgt>p.rgt
                        ORDER BY lft DESC LIMIT 1)
                FROM pens_feeds_move m JOIN pens p ON m.pen_to=p.id
                WHERE id_of=pf.id),
               (SELECT CONCAT_WS('-', (SELECT pen FROM pens WHERE rgt>(lft+1) AND lft<p.lft AND
                                         rgt>p.rgt ORDER BY lft DESC LIMIT 1), p.pen)
                FROM g_ev_feeds e JOIN pens p ON e.pen_id=p.id
                WHERE id=pf.id)
             )
           END 
    FROM pens_feeds pf
    WHERE date>=%s AND date<=%s AND pen_id=%s AND feed_id=(SELECT id FROM feeds WHERE feed=%s)
    ORDER BY date, id;""", (rq['d1'], rq['d2'], rq['pen_id'], rq['feed'])), cls=JsonEncoder)


def set_feed(rq):
  move = db.one("SELECT id_to, id_of FROM pens_feeds_move WHERE id_to=%s OR id_of=%s;",
                (rq['id'], rq['id']))
  if move:
    other = move[0] if int(rq['id']) == move[1] else move[1]
    group = db.one("SELECT group_id FROM g_ev_feeds WHERE id=%s OR id=%s;", (rq['id'], other))
    if group:
      raise Exception("error: alimentacion asociada a grupo: %s!" % group[0])
    db.execute("DELETE FROM pens_feeds WHERE id=%s", (other, ))
  db.execute("DELETE FROM pens_feeds WHERE id=%s", (rq['id'], ))


################################ INVENTORY STOCK #####################################
def get_groups(rq):
  defs = [{"head":"Grupo", "foot":"count", "class":"strong"},
          {"head":"Corral Actual", "class":"group"},
          {"head":"Edad", "foot":"avg"},
          {"head":"Consumo", "foot":"sum"},
          {"head":"Inicial", "foot":"sum"},
          {"head":"Ingresos", "foot":"sum"},
          {"head":"Salidas", "foot":"sum"},
          {"head":"Muertes", "foot":"sum"},
          {"head":"Final", "foot":"sum", "value":"r[4] + r[5] - r[6] - r[7]"}]
  rows = db.all("""
  SELECT g.id,
         (SELECT (SELECT CONCAT_WS('-',
                           (SELECT pen FROM pens WHERE rgt>(lft+1) AND lft<p.lft AND rgt>p.rgt
                            ORDER BY lft DESC LIMIT 1), pen)
                  FROM pens p WHERE id=ges.pen_id)
          FROM g_ev_stock ges WHERE group_id=g.id AND date<=%(d2)s
          ORDER BY id DESC LIMIT 1) AS house_pen,
         (SELECT SUM(total * days) / SUM(total)::NUMERIC
          FROM (SELECT (ingress -
                        CASE WHEN SUM(ingress-egress-deaths) OVER()=0 AND MAX(id) OVER()=id
                             THEN 0 ELSE egress + deaths END) AS total,
                       COALESCE(g.date_final, %(d2)s) - date_birth AS days
                FROM g_ev_stock WHERE group_id=g.id AND date<=%(d2)s) t),
         (SELECT SUM((SELECT SUM(ingress-egress) FROM pens_feeds WHERE id=ef.id))
          FROM g_ev_feeds ef WHERE group_id=g.id AND date<=%(d2)s),
         (SELECT CONCAT_WS('_', SUM(CASE WHEN date<%(d1)s THEN ingress-egress-deaths ELSE 0 END),
                                SUM(CASE WHEN date>=%(d1)s AND
                                              NOT EXISTS(SELECT id FROM groups_events
                                                         WHERE group_id=g_ev_stock.group_id AND
                                                               id=(SELECT id_of
                                                                   FROM g_ev_stock_move
                                                                   WHERE id_to=g_ev_stock.id))
                                         THEN ingress ELSE 0 END),
                                SUM(CASE WHEN date>=%(d1)s AND
                                              NOT EXISTS(SELECT id FROM groups_events
                                                         WHERE group_id=g_ev_stock.group_id AND
                                                               id=(SELECT id_to
                                                                   FROM g_ev_stock_move
                                                                   WHERE id_of=g_ev_stock.id))
                                         THEN egress ELSE 0 END),
                                SUM(CASE WHEN date>=%(d1)s THEN deaths ELSE 0 END))
          FROM g_ev_stock WHERE group_id=g.id AND date<=%(d2)s)
    FROM groups g
    WHERE date_initial<=%(d2)s AND (date_final>=%(d1)s OR date_final IS NULL)
    ORDER BY house_pen;""", {"d1":rq['d1'],"d2":rq['d2']}) 
  tmp = [[r[0], r[1], r[2], r[3]] + \
         ([int(x) for x in r[4].split('_')] if r[4] != '' else [0,0,0,0]) for r in rows]
  return dumps(set_table(defs, tmp), cls=JsonEncoder)


def get_group_history(rq):
  return dumps(db.all("""
    SELECT id, date,
           CONCAT_WS('_', tableoid::regclass::name,
                     (SELECT 'move' FROM groups_events WHERE group_id=e.group_id AND
                        id=(SELECT id_of FROM g_ev_stock_move WHERE id_to=e.id)),
                     (SELECT 'move' FROM groups_events WHERE group_id=e.group_id AND
                        id=(SELECT id_to FROM g_ev_stock_move WHERE id_of=e.id))),
           g_ev_cols_function(tableoid::regclass::name, id),
           (SELECT CONCAT_WS('-',
                     (SELECT pen FROM pens WHERE rgt>(lft+1) AND lft<p.lft AND rgt>p.rgt
                      ORDER BY lft DESC LIMIT 1), pen)
            FROM pens p WHERE id=e.pen_id),
           CASE WHEN tableoid::regclass::name = 'g_ev_stock'
                THEN date - (SELECT date_birth FROM g_ev_stock WHERE id=e.id)
                ELSE (SELECT SUM(total * days) / SUM(total)::NUMERIC
                      FROM (SELECT ingress - CASE WHEN SUM(ingress-egress-deaths) OVER()=0 AND
                                                       MAX(id) OVER()=id
                                                  THEN 0 ELSE egress + deaths END AS total,
                                   e.date - date_birth AS days
                            FROM g_ev_stock WHERE group_id=e.group_id AND date<=e.date) t) END
    FROM groups_events e WHERE date>=%s AND date<=%s AND group_id=%s
    ORDER BY date, id;""", (rq['d1'], rq['d2'], rq['group_id'])), cls=JsonEncoder)


def set_group(rq):
  group_data = db.one("""
    SELECT group_id, tableoid::regclass::name,
           (SELECT date_final FROM groups WHERE id=e.group_id),
           (SELECT MAX(id) FROM groups_events WHERE group_id=e.group_id AND
              tableoid::regclass::name IN ('g_ev_stock', 'insert_pen_sale', 'insert_pen_death'))
    FROM groups_events e WHERE id=%s;""", (rq['id'], ))
  if group_data[2]:
    if int(rq['id']) == group_data[3]:
      db.execute("UPDATE groups SET date_final=NULL WHERE id=%s", (group_data[0], ))
    else:
      raise Exception("error: grupo cerrado!")
  if group_data[1] == 'g_ev_feeds':
    db.execute("DELETE FROM g_ev_feeds WHERE id=%s;", (rq['id'], ))
    set_feed(rq)
  elif group_data[1] == 'g_ev_stock':
    move = db.one("SELECT id_to, id_of FROM g_ev_stock_move WHERE id_to=%s OR id_of=%s;",
                  (rq['id'], rq['id']))
    if move:
      other = move[0] if int(rq['id']) == move[1] else move[1]
      other_group_id = db.one("SELECT group_id FROM g_ev_stock WHERE id=%s;", (other, ))[0]
      if group_data[0] == other_group_id:
        prev_pen_count = db.one("""
          WITH groups_inventory AS (SELECT (SELECT pen_id FROM g_ev_stock
                                            WHERE group_id=g.id ORDER BY id DESC LIMIT 1)
                                    FROM groups g WHERE date_final IS NULL)
          SELECT COALESCE((SELECT 1 FROM groups_inventory WHERE pen_id=t.pen_id), 0)
          FROM g_ev_stock t WHERE id=%s;""", (other if other < int(rq['id']) else rq['id'], ))[0]
        if prev_pen_count > 0:
          raise Exception("error: corral anterior esta ocupado por otro grupo!")
      db.execute("DELETE FROM g_ev_stock_move WHERE id_to=%s OR id_of=%s;",
                 (rq['id'], rq['id']))
      group_id = db.one("DELETE FROM groups_events WHERE id=%s RETURNING group_id",
                        (other, ))[0]
      db.execute("UPDATE groups SET date_final=NULL WHERE id=%s", (group_id, ))
    db.execute("DELETE FROM groups_events WHERE id=%s", (rq['id'], ))
  else:
    db.execute("DELETE FROM groups_events WHERE id=%s", (rq['id'], ))



#################################### INVENTORY #########################################
@growing.route('/growing/get_inventory', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_inventory():
  rq = await request.form
  return get_feeds(rq) if rq['inventory'] == 'feed' else get_groups(rq)


@growing.route('/growing/get_inventory_history', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_inventory_history():
  rq = await request.form
  return get_feed_history(rq) if rq['inventory'] == 'feed' else get_group_history(rq)


@growing.route('/growing/set_inventory_delete', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_inventory_delete():
  rq = await request.form
  set_feed(rq) if rq['inventory'] == 'feed' else set_group(rq)
  db.commit()
  return await get_inventory()


@growing.route('/growing/growing_inventory', methods=['GET', 'POST'])
@login_required
@check_user
@db.wrapper
async def growing_inventory():
  rq = await request.form
  return await render_template('templates/growing_inventory.html', title="Produccion Inventarios",
                               data=await get_inventory() if request.method == 'POST' else [],
                               inventory=rq['inventory'] if 'inventory' in rq else '',
                               d1=rq['d1'] if 'd1' in rq else '', d2=rq['d2'] if 'd2' in rq else '')


###################### PARAMETERS #######################
def insert_pens(rq):
  pens = int(rq['pens'])
  if pens < 1:
    raise Exception("error: numero de corrales debe ser mayor a 0!")
  rgt = db.one("SELECT COALESCE((SELECT rgt FROM pens WHERE rgt<>lft+1 AND pen=%s), 0);",
               (rq['place'], ))[0]
  if rgt > 0:
    house = db.one("""SELECT (SELECT MAX(rgt) FROM pens
                              WHERE rgt>(lft+1) AND lft<p.lft AND rgt>p.rgt)
                      FROM pens p WHERE pen=%s;""", (rq['house'], ))
    if house and house[0] == rgt:
      raise Exception("error: el galeron ya existe!")
    db.execute("UPDATE pens SET lft=lft+%s WHERE lft>=%s;", ((pens * 2) + 4, rgt))
    db.execute("UPDATE pens SET rgt=rgt+%s WHERE rgt>=%s;", ((pens * 2) + 4, rgt))
    db.execute("INSERT INTO pens VALUES(DEFAULT, %s, %s, %s);",
               (rq['house'], rgt, (pens * 2) + 3 + rgt))
    for n in range(pens+1):
      db.execute("INSERT INTO pens VALUES(DEFAULT, %s, %s, %s);",
                 (n, (n * 2) + 1 + rgt, (n * 2) + 2 + rgt))
  else:
    rgt = db.one("SELECT COALESCE(MAX(rgt), 0) FROM pens;")[0];
    db.execute("INSERT INTO pens VALUES(DEFAULT, %s, %s, %s);",
               (rq['place'], 1 + rgt, (pens * 2) + 6 + rgt))
    db.execute("INSERT INTO pens VALUES(DEFAULT, %s, %s, %s);",
               (rq['house'], 2 + rgt, (pens * 2) + 5 + rgt))
    for n in range(pens+1):
      db.execute("INSERT INTO pens VALUES(DEFAULT, %s, %s, %s);",
                 (n, (n * 2) + 3 + rgt, (n * 2) + 4 + rgt))


def growing_parameters_data(rq):
  if 'pen' in rq['action']:
    rows = db.all("""
      SELECT (SELECT pen FROM pens WHERE lft=MIN(p.lft)) AS place,
             n.pen AS house,
             (SELECT COUNT(*)-1 FROM pens WHERE lft>n.lft AND rgt<n.rgt) AS pens       
      FROM pens n, pens p WHERE n.lft BETWEEN p.lft AND p.rgt
      GROUP BY n.id HAVING COUNT(*) = 2 ORDER BY n.lft;""")
  if 'price' in rq['action']:
    rows = db.all("SELECT * FROM feeds ORDER BY feed;")
  if 'week_feed' in rq['action']:
    rows = db.all("SELECT * FROM week_feeds ORDER BY id;")
  return dumps(rows, cls=JsonEncoder)


@growing.route('/growing/growing_parameters')
@login_required
@check_user
@db.wrapper
async def growing_parameters():
  return await render_template('templates/growing_parameters.html', title="Parametros Produccion")


@growing.route('/growing/get_growing_parameters', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def get_growing_parameters():
  rq = await request.form
  return growing_parameters_data(rq)


@growing.route('/growing/set_growing_parameters', methods=['POST'])
@login_required
@check_user
@db.wrapper
async def set_growing_parameters():
  rq = await request.form
  if rq['action'] == 'insert_pens':
    insert_pens(rq)
  if rq['action'] == 'insert_price':
    db.execute("INSERT INTO feeds VALUES (DEFAULT, %s, %s, %s, TRUE);",
               (rq["feed"], rq["description"], rq["price"]))
  if rq['action'] == 'update_price':
    db.execute("UPDATE feeds SET feed=%s, description=%s, price=%s, active=%s WHERE id=%s;",
               (rq["feed"], rq["description"], rq["price"], True, rq["id"]))
  if rq['action'] == 'insert_week_feed':
    db.execute("""INSERT INTO week_feeds
                  SELECT COALESCE(MAX(id), 0)+1, %s, %s FROM week_feeds;""",
               (rq["weight"], rq["feed_type"]))
  if rq['action'] == 'update_week_feed':
    db.execute("UPDATE week_feeds SET weight=%s, feed_type=%s WHERE id=%s;",
               (rq["weight"], rq["feed_type"], rq["id"]))
  db.commit()
  return growing_parameters_data(rq)
